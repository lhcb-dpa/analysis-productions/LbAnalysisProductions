#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail
IFS=$'\n\t'

set -x

# OpenShift builds don't keep the .git so we have to manually clone
# Restore the correct version
if [ -f "/source/.cloned-in-openshift" ] && [ -n "${OPENSHIFT_BUILD_COMMIT:-}" ]; then
    (cd "/source/" && git checkout "${OPENSHIFT_BUILD_COMMIT}")
fi
# Install on launch to allow bind mounts to be used during development
pip install /source -vv

# Enable coverage if requested
if [ -n "${COVERAGE_PROCESS_START:-}" ]; then
    echo "Enabling coverage"
    cp /source/docker/base/enable_coverage.pth "$(python -c "import site; print(site.getsitepackages()[0])")/enable_coverage.pth"
else
    echo "Not enabling coverage"
fi


if dotenv --file "$LBAP_DOTENV" list | grep --count 'LBAP_DIRAC_HOST_CERT'; then
    DIRAC_HOST_CERT_FN=$(mktemp)
    DIRAC_HOST_KEY_FN=$(mktemp)
    dotenv --file "$LBAP_DOTENV" get "LBAP_DIRAC_HOST_CERT" > "${DIRAC_HOST_CERT_FN}"
    dotenv --file "$LBAP_DOTENV" get "LBAP_DIRAC_HOST_KEY" > "${DIRAC_HOST_KEY_FN}"

    mkdir -p /miniconda/envs/analysis-productions/etc
    cat <<EOT > /miniconda/envs/analysis-productions/etc/dirac.cfg
DIRAC
{
Security
{
    CertFile = /PATH/TO/HOST_CERT_FILE
    KeyFile = /PATH/TO/HOST_KEY_FILE
}
}
LocalInstallation
{
UseServerCertificate = True
}
EOT
    sed -i "s@/PATH/TO/HOST_CERT_FILE@${DIRAC_HOST_CERT_FN}@g" /miniconda/envs/analysis-productions/etc/dirac.cfg
    sed -i "s@/PATH/TO/HOST_KEY_FILE@${DIRAC_HOST_KEY_FN}@g" /miniconda/envs/analysis-productions/etc/dirac.cfg
fi

if [ -n "${DIRAC_HOST_CERT_FN:-}" ] || [ -n "${X509_USER_PROXY:-}" ]; then
    if [ "${DIRAC_SETUP}" = "LHCb-Certification" ]; then
        dirac-configure -F -S LHCb-Certification -C dips://lhcb-cert-conf-dirac.cern.ch:9135/Configuration/Server -ddd --UseServerCertificate
    elif [ "${DIRAC_SETUP}" = "LHCb-Production" ]; then
        dirac-configure -F -S LHCb-Production -C dips://lhcb-conf-dirac.cern.ch:9135/Configuration/Server -ddd --UseServerCertificate
    else
        echo "Unknown setup $DIRAC_SETUP"
        exit 47
    fi
fi

exec "$@"
