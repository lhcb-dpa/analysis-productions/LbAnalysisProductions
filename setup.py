###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from os.path import abspath, dirname, join

from setuptools import find_packages, setup

here = abspath(dirname(__file__))

# Get the long description from the README file
with open(join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()


package_data = []
for root, dirs, files in os.walk(join(here, "src/LbAnalysisProductions/data")):
    package_data += [join(root, fn) for fn in files]


setup(
    name="LbAnalysisProductions",
    use_scm_version=True,
    description="Scripts for testing and running analysis productions in LHCb",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.cern.ch/cburr/LbAnalysisProductions",
    author="LHCb",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    keywords="LHCb CERN",
    packages=find_packages("src"),
    package_dir={"": "src"},
    python_requires=">=3.8",
    setup_requires=["setuptools_scm"],
    install_requires=[
        "dirac_prod",
        "setuptools",
        "LHCbDIRAC",
        "cachetools",
        "graphviz",
        "lxml",
        "boto3",
        "jinja2",
        "pyyaml",
        "celery",
        "colorlog",
        "gitpython",
        "lxml",
        "requests",
        "sqlalchemy",
        "gitlab_runner_api",
        "lbapcommon>=0.10.6",
        "python-gitlab",
        "python-logging-rabbitmq",
        "pydantic[dotenv]",
        "celery",
        "psutil",
        "dateparser",
        "typer",
        "lbdiracwrappers",
        "opensearch-py",
        "sentry-sdk==0.10.2",
        "pydantic_settings",
    ],
    package_data={
        "LbAnalysisProductions": package_data,
    },
    zip_safe=False,
    entry_points={
        "console_scripts": [
            "lbap-admin=LbAnalysisProductions.cli:app",
            "lbap-celery-monitoring=LbAnalysisProductions.monitoring:monitor_celery",
        ],
    },
    project_urls={
        "Bug Reports": "https://gitlab.cern.ch/cburr/LbAnalysisProductions/issues",
        "Source": "https://gitlab.cern.ch/cburr/LbAnalysisProductions",
    },
)
