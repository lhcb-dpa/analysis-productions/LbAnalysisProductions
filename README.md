# LHCb Analysis Productions

This is the main source code repository for the infrastructure behind LHCb Analysis productions.
If you are an analyst looking to submit productions you should refer to the [`AnalysisProductions` data package](https://gitlab.cern.ch/lhcb-datapkg/AnalysisProductions).
This page mostly contains technical details about the system is implemented and how developers can contribute.
To get a more general overview you can take a look at:

* [LHCb-INT-2020-003: LHCb Offline Analysis in Run 3 OATF recommendations](https://cds.cern.ch/record/2711207?)
* [LHCb-INT-2020-009: Data Processing & Analysis (DPA) Project](https://cds.cern.ch/record/2715382?)
* ["Analysis productions" at the 12th LHCb Computing Workshop](https://indico.cern.ch/event/831054/contributions/3620293/)

## Overview

Analysis productions are centralised around the `LbAnalysisProductions` python package.
This provides several services that run on [CERN OpenShift](http://information-technology.web.cern.ch/services/PaaS-Web-App) instance.

The main component is a [Celery](https://www.celeryproject.org) application which impersonates a GitLab CI runner.
This allows jobs to be dynamically generated and user DIRAC credentials to be injected into the job.
To better utilise resources, tests are ran as user jobs using LHCbDIRAC.
Test results are streamed to [CERN S3](https://cern.service-now.com/service-portal?id=functional_element&name=S3ObjectStorage) with some results being stores in a MySQL database hosted by [CERN DBoD](http://information-technology.web.cern.ch/services/database-on-demand).
The results of tests are displayed using a web application which powers https://lhcb-analysis-productions.web.cern.ch/.

In addition there are several other associated respositories:
* [`LbAPLocal`](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/lbaplocal): Helper scripts which can be used to running tests locally and reproducing the results of failed tests that ran in CI.
* [`LbAP-testing`](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/lbap-testing): A development instance of the system running against the LHCb-Certification instance of LHCbDIRAC.
* [`LbAP-deployment`](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/lbap-deployment): Deployment scripts which can be used for setting up the system either locally or on OpenShift.

## Contributing

If you're interested in contributing please join:
* Mattermost: https://mattermost.web.cern.ch/lhcb/channels/analysis-productions
* Mailing list: `lhcb-dpa-wp2@cern.ch`

### Setting up a basic local environment

The easiest way to get a local environment is to use conda and conda-forge.

```bash
$ git clone ssh://git@gitlab.cern.ch:7999/lhcb-dpa/analysis-productions/LbAnalysisProductions.git
$ cd LbAnalysisProductions
$ conda env create --file environment.yaml --name analysis-productions-dev
$ conda activate analysis-productions-dev
$ pip install -e .
```

### Running basic tests

```bash
$ flake8 src
$ pytest -v
```

### Code style

Code styling is handled with pre-commit.

### Running a full instance locally

See https://gitlab.cern.ch/lhcb-dpa/analysis-productions/lbap-deployment.
