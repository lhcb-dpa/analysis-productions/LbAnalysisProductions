###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os

from DIRAC.WorkloadManagementSystem.Client.DownloadInputData import DownloadInputData

COMPONENT_NAME = "LbAPInputDownloader"


class LbAPInputDownloader(DownloadInputData):
    def __init__(self, *args, **kwargs):
        # TODO: DownloadInputData should be a new style class
        # super(LbAPInputDownloader, self).__init__(*args, **kwargs)
        DownloadInputData.__init__(self, *args, **kwargs)
        self.inputDataDirectory = os.environ["LBAP_DATA_DIR"]
