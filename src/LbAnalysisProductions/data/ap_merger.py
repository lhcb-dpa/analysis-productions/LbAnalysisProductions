###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import subprocess
import sys
import tempfile
import xml.etree.ElementTree as ET
from pathlib import Path

# pylint: disable=import-error
from GaudiConf.LbExec.options import FileFormats
from GaudiConf.LbExec.options import Options as OptionsBase

SUMMARY_XML_TEMPLATE = """<?xml version="1.0" encoding="UTF-8"?>
<summary xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xsi:noNamespaceSchemaLocation="$XMLSUMMARYBASEROOT/xml/XMLSummary.xsd">
    <success>True</success>
    <step>finalize</step>
    <usage><stat unit="KB" useOf="MemoryMaximum">0</stat></usage>
    <input>
{input_files}
    </input>
    <output>
{output_files}
    </output>
</summary>
"""
XML_FILE_TEMPLATE = '     <file GUID="" name="{name}" status="full">{n}</file>'
ALG_TO_CODE = {
    "ZLIB": 1,
    "LZMA": 2,
    "LZ4": 4,
    "ZSTD": 5,
}


class Options(OptionsBase):
    """Conditions"""

    input_type: FileFormats = FileFormats.ROOT
    simulation: None = False
    data_type: None = None


def read_xml_file_catalog(xml_file_catalog):
    if xml_file_catalog is None:
        return {}

    tree = ET.parse(xml_file_catalog)
    pfn_lookup: dict[str, list[str]] = {}
    for file in tree.findall("./File"):
        lfns = [x.attrib.get("name") for x in file.findall("./logical/lfn")]
        if len(lfns) == 0:
            continue
        if len(lfns) > 1:
            raise NotImplementedError(lfns)
        lfn = lfns[0]
        pfn_lookup[f"LFN:{lfn}"] = [
            x.attrib.get("name") for x in file.findall("./physical/pfn")
        ]
    return pfn_lookup


def resolve_input_files(input_files, file_catalog):
    resolved = []
    for input_file in input_files:
        if input_file.startswith("LFN:"):
            print("Resolved", input_file, "to", file_catalog[input_file][0])
            input_file = file_catalog[input_file][0]
        resolved.append(input_file)
    return resolved


def hadd(options: Options, compression: str = "ZSTD:4"):
    file_catalog = read_xml_file_catalog(options.xml_file_catalog)
    input_files = resolve_input_files(options.input_files, file_catalog)

    alg, level = compression.split(":")
    flags = [f"-f{ALG_TO_CODE[alg]}{int(level):02d}"]
    flags += ["-j", f"{options.n_threads}"]
    flags += ["-n", f"{max(10, options.n_threads*2)}"]

    with tempfile.NamedTemporaryFile(mode="wt") as tmpfile:
        tmpfile.write("\n".join(input_files))
        tmpfile.flush()
        cmd = ["hadd"] + flags + [options.ntuple_file, f"@{tmpfile.name}"]
        print("Running", cmd)
        subprocess.run(cmd, check=True)

    summary_xml = SUMMARY_XML_TEMPLATE.format(
        input_files="\n".join(
            XML_FILE_TEMPLATE.format(
                name=name if name.startswith("LFN:") else f"PFN:{name}", n=1
            )
            for name in options.input_files
        ),
        output_files=XML_FILE_TEMPLATE.format(
            name=f"PFN:{options.ntuple_file}", n=len(input_files)
        ),
    )
    if options.xml_summary_file:
        print("Writing XML summary to", options.xml_summary_file)
        Path(options.xml_summary_file).write_text(summary_xml)

    print("All done")
    sys.exit(0)
