/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
document.addEventListener('DOMContentLoaded', function(event) {
  // Toggle which parts of the log are visible
  for (elm of document.getElementsByClassName('log-level')) {
    elm.addEventListener('change', function() {
        for (elm of document.getElementsByClassName('log-level')) {
          if (elm.checked) {
              elm.parentElement.style.color = '#00ff00';
          } else {
              elm.parentElement.style.color = '#ff0000';
          }
        }
    });
  }

  // Hide the log by default if JavaScript is available
  document.querySelectorAll('.view-text-file-fallback').forEach(e => {
    if (!e) return;
    e.style.display = 'none';
  });

  // Configure the show/hide buttons
  document.querySelectorAll('.display-button').forEach(e => {
    if (!e) return;
    e.addEventListener('click', function() {
      var log_elm = document.getElementById(this.id.replace('toggle-', ''));
      log_elm.style.display = log_elm.style.display == 'block' ? 'none' : 'block';
      var textMap = {'block': 'Hide', 'none': 'Show'};
      this.textContent = textMap[log_elm.style.display];
      return false;
    });
  });
});

function lbap_fill_log_explanation(el, items, headingText) {
  var heading = document.createElement('h4');
  heading.appendChild(document.createTextNode(headingText));
  el.appendChild(heading);
  var ul = document.createElement('ul');
  el.appendChild(ul);

  items.forEach(function(element) {
    var li = document.createElement('li');
    var converter = new showdown.Converter();
    markdown = element[0];
    if (element[1]) {
      if (element[1].length === 1)
        markdown = "**Line " + element[1][0] + ":** " + markdown;
      else
        markdown = "**Line " + element[1][0] + " and " + (element[1].length - 1) + " others:** " + markdown;
    }
    li.innerHTML = converter.makeHtml(markdown);
    ul.appendChild(li);
  });
}
