/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
(function(Prism) {
  var string = /(?:# )?(?:\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} \w+ )?[^\s]+\s*/;

  Prism.languages['gaudirunlog'] = {
    // include: {
    //   pattern: /# --> Including file ([^\n]+)\n([^\n]*\n)*?# <-- End of file \1\n/,
    //   alias: 'config'
    // },

    config: {
      pattern: /(?:# |[^\n]+)\/\*{5} ([^\*]+) \*{5}\*+\n([^\n]*\n)*?(?:# )?\\\-{5} \(End of \1\) \-{5}\-+\n/,
    },

    // Regex's are wrapped with (?:REGEX)+ to reduce the number of spans created
    verbose: RegExp('(?:'+string.source+'VERBOSE [^\n]+\n?)+'),
    debug: RegExp('(?:'+string.source+'DEBUG [^\n]+\n?)+'),
    info: RegExp('(?:'+string.source+'INFO [^\n]+\n?)+'),
    warning: RegExp('(?:'+string.source+'WARNING [^\n]+\n?)+'),
    error: RegExp('(?:'+string.source+'ERROR [^\n]+\n?)+'),
    fatal: RegExp('(?:'+string.source+'FATAL [^\n]+\n?)+'),
    always: RegExp('(?:'+string.source+'ALWAYS [^\n]+\n?)+'),

    success: RegExp('(?:'+string.source+/SUCCESS (?:Number of counters : \d+\n(?: \|[^\n]+\|\n)+|[^\n]+\n)/.source+')+'),

    welcome_message: {
      pattern: /={100}=+\n([^\n]+\n){2}={100}=+\n/
    },

    unknown: /[^\n]*\n/
  };
}(Prism));
