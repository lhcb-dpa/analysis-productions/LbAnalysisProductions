/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
(function(Prism) {
  Prism.languages['j2yaml'] = {
    'j2_comment': {
      pattern: /^{#[\s\S]*?#}$/,
      alias: 'comment'
    },

    'key': {
      pattern: /(\s*(?:^|[:\-,[{\r\n?])[ \t]*(?:![^\s]+)?[ \t]*)(?:[^\r\n{[\]},#\s]|{{ .*? }})+?(?=\s*:\s)/,
      lookbehind: true,
      alias: 'atrule',
      inside: {
        'j2_value': {
          pattern: /{{.+?}}/,
          alias: 'comment'
        }
      }
    },

    // Jinja 2
    'j2_value': {
      pattern: /{{.+?}}/,
      alias: 'comment'
    },
    'j2_tag': {
      pattern: /(^{%[+-]?\s*)\w+/,
      lookbehind: true,
      alias: 'keyword',
      inside: {
        'string': {
          pattern: /([:\-,[{]\s*(?:![^\s]+)?[ \t]*)("|')(?:(?!\2)[^\\\r\n]|\\.)*\2(?=[ \t]*(?:$|,|]|}|\s*#))/m,
          lookbehind: true,
          greedy: true
        }
      }
    },
    'j2_delimiter': {
      pattern: /^{[{%][+-]?|[+-]?[}%]}$/,
      alias: 'punctuation'
    },
    'j2_string': {
      pattern: /("|')(?:\\.|(?!\1)[^\\\r\n])*\1/,
      greedy: true,
      alias: 'string'
    },
    'j2_filter': {
      pattern: /(\|)\w+/,
      lookbehind: true,
      alias: 'function'
    },
    'j2_test': {
      pattern: /(\bis\s+(?:not\s+)?)(?!not\b)\w+/,
      lookbehind: true,
      alias: 'function'
    },
    'j2_function': {
      pattern: /\b[a-z_]\w+(?=\s*\()/i,
      alias: 'function'
    },
    'j2_keyword': {
      pattern: /\b(?:set|and|as|by|else|(?:end)?for|(?:end)?if|import|in|is|loop|not|or|recursive|(?:end)?with|without)\b/,
      alias: 'keyword'
    },
    'j2_operator': {
      pattern: /[-+*/%=]=?|!=|\*\*?=?|\/\/?=?|<[<=>]?|>[=>]?|[&|^~]/,
      alias: 'operator'
    },
    'j2_number': {
      pattern: /\b\d+(?:\.\d+)?\b/,
      alias: 'number'
    },
    'j2_boolean': {
      pattern: /[Tt]rue|[Ff]alse|[Nn]one/,
      alias: 'boolean'
    },
    'j2_variable': {
      pattern: /\b\w+?\b/,
      alias: 'variable'
    },
    'j2_punctuation': {
      pattern: /[{}[\](),.:;]/,
      alias: 'punctuation'
    },

    // YAML
    'scalar': {
      pattern: /([\-:]\s*(?:![^\s]+)?[ \t]*[|>])[ \t]*(?:((?:\r?\n|\r)[ \t]+)[^\r\n]+(?:\2[^\r\n]+)*)/,
      lookbehind: true,
      alias: 'string'
    },
    'comment': /#.*/,
    'directive': {
      pattern: /(^[ \t]*)%.+/m,
      lookbehind: true,
      alias: 'important'
    },
    'datetime': {
      pattern: /([:\-,[{]\s*(?:![^\s]+)?[ \t]*)(?:\d{4}-\d\d?-\d\d?(?:[tT]|[ \t]+)\d\d?:\d{2}:\d{2}(?:\.\d*)?[ \t]*(?:Z|[-+]\d\d?(?::\d{2})?)?|\d{4}-\d{2}-\d{2}|\d\d?:\d{2}(?::\d{2}(?:\.\d*)?)?)(?=[ \t]*(?:$|,|]|}))/m,
      lookbehind: true,
      alias: 'number'
    },
    'boolean': {
      pattern: /([:\-,[{]\s*(?:![^\s]+)?[ \t]*)(?:true|false)[ \t]*(?=$|,|]|})/im,
      lookbehind: true,
      alias: 'important'
    },
    'null': {
      pattern: /([:\-,[{]\s*(?:![^\s]+)?[ \t]*)(?:null|~)[ \t]*(?=$|,|]|})/im,
      lookbehind: true,
      alias: 'important'
    },
    'string': {
      pattern: /([:\-,[{]\s*(?:![^\s]+)?[ \t]*)("|')(?:(?!\2)[^\\\r\n]|\\.)*\2(?=[ \t]*(?:$|,|]|}|\s*#))/m,
      lookbehind: true,
      greedy: true
    },
    'number': {
      pattern: /([:\-,[{]\s*(?:![^\s]+)?[ \t]*)[+-]?(?:0x[\da-f]+|0o[0-7]+|(?:\d+\.?\d*|\.?\d+)(?:e[+-]?\d+)?|\.inf|\.nan)[ \t]*(?=$|,|]|})/im,
      lookbehind: true
    },
    'tag': /![^\s]+/,
    'important': /[&*][\w]+/,
    'punctuation': /---|[:[\]{}\-,|>?]|\.\.\./,
  };
}(Prism));
