#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import asyncio
import hashlib
import json
import logging
import os
import re
import shlex
import shutil
import subprocess
import time
from fnmatch import fnmatch
from pathlib import Path
from tempfile import TemporaryDirectory

import requests

log = logging.getLogger("LbAPTestRunner")
handler = logging.StreamHandler()
try:
    import colorlog
except ImportError:
    pass
else:
    handler.setFormatter(colorlog.ColoredFormatter(reset=True, style="%"))
log.addHandler(handler)
log.setLevel(logging.DEBUG)

DIRAC_LOG_FN = "DIRAC.log"
PRMON_SAMPLE_INTERVAL = 30  # seconds
PRMON_SAMPLE_OUTPUT = "prmon.txt"
PRMON_SUMMARY_OUTPUT = "prmon.json"

# The files and patterns to upload to S3
OUTPUT_DIR_FILENAMES = [
    DIRAC_LOG_FN,
    PRMON_SAMPLE_OUTPUT,
    PRMON_SUMMARY_OUTPUT,
]
LOCAL_DIR_PATTERNS = [
    "pool_xml_catalog.xml",
    "jobDescription.xml",
    "prodConf_00012345_00006789_{i}.py",
    "prodConf_{application_name}_00012345_00006789_{i}.json",
    "summary{application_name}_00012345_00006789_{i}.xml",
    "bookkeeping_00012345_00006789_{i}.xml",
    "{application_name}_00012345_00006789_{i}.log",
    "*.tck.json",
]

# Remove DIRAC's JOBID from the environment if set to avoid DIRAC picking it up
JOB_ID = os.environ.pop("JOBID", None)

# Global variable used to avoid repeatedly upload identical files to S3
S3_HASH_CACHE = {}


async def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--pipeline-id", required=True, type=int)
    parser.add_argument("--production-name", required=True)
    parser.add_argument("--job-name", required=True)
    parser.add_argument("--attempt", required=True, type=int)
    parser.add_argument("--host", default="https://lbap.app.cern.ch")
    parser.add_argument("--token", required=True)

    subparsers = parser.add_subparsers(required=True)

    parser_foo = subparsers.add_parser("ci")
    parser_foo.set_defaults(
        func=lambda args: main(
            args.pipeline_id,
            args.production_name,
            args.job_name,
            args.attempt,
            args.token,
            host=args.host,
        )
    )

    args = parser.parse_args()
    await args.func(args)


async def main(pipeline_id, production_name, job_name, attempt, token, *, host):
    _set_job_status("LbAP Initializing")
    assert shutil.which("singularity"), "Singularity is not installed"

    test_info = _get_exec_info(
        pipeline_id, production_name, job_name, attempt, token, host
    )
    tmpdir = TemporaryDirectory()

    _set_job_status("LbAP Initializing", "Checking out sources")
    repository_dir = await _setup_repository_dir(Path(tmpdir.name), test_info)
    output_dir = repository_dir / "output"
    output_dir.mkdir(exist_ok=False, parents=True)

    _set_job_status("LbAP Initializing", "Launching uploader daemon")
    uploader = asyncio.create_task(_uploader_loop(output_dir, test_info["jobs"]))

    _set_job_status("LbAP Initializing", "Downloading input data")
    data_dir = Path(os.getcwd()) / "inputdata"
    data_dir.mkdir(exist_ok=True, parents=True)
    for lfn in test_info["jobs"][0]["input"]["paths"]:
        await _download_input_data(lfn, data_dir)

    _set_job_status("LbAP Initializing", "Preparing for test")
    dynamic_dir = repository_dir / "dynamic"
    dynamic_dir.mkdir(exist_ok=True, parents=True)
    for job in test_info["jobs"]:
        if job["automatically_configure"]:
            fn = dynamic_dir / job["dynamic_options_path"]
            fn.parent.mkdir(exist_ok=True, parents=True)
            fn.write_text(job["autoconf_options"])

    with TemporaryDirectory() as tmp_dir:
        tmp_dir = Path(tmp_dir)
        extra_env = _setup_lbrun_environment(tmp_dir, repository_dir)

        # write_jsroot_compression_options

        dirac_yaml_path = tmp_dir / "dirac_prod.yaml"
        dirac_yaml = _get_dirac_yaml(
            pipeline_id, production_name, job_name, attempt, token, host
        )
        dirac_yaml_path.write_text(dirac_yaml)

        benchmark_cfg = shlex.quote(str(tmp_dir / "tmp.cfg"))
        benchmark_cfg = subprocess.check_output(
            f"dirac-wms-cpu-normalization --Reconfig {benchmark_cfg} --Update >/dev/null 2>&1 && diraccfg as-json {benchmark_cfg}",
            shell=True,
        )
        cpu_norm_factor = json.loads(benchmark_cfg)["LocalSite"][
            "CPUNormalizationFactor"
        ]

        _set_job_status("LbAP Initializing", "Starting test")

        command = [  # start test with prmon
            "prmon",
            "--interval",
            str(PRMON_SAMPLE_INTERVAL),
            "--filename",
            str(repository_dir / "output" / PRMON_SAMPLE_OUTPUT),
            "--json-summary",
            str(repository_dir / "output" / PRMON_SUMMARY_OUTPUT),
            "--",
        ]
        command += ["dirac-production-request-run-local", str(dirac_yaml_path)]
        command += ["-o", f"/LocalSite/InputDataDirectory={data_dir}"]
        command += ["-o", f"/LocalSite/CPUNormalizationFactor={cpu_norm_factor}"]

        command += [f"--input-files={','.join(test_info['jobs'][0]['input']['paths'])}"]
        log.info("Running: %r", command)
        proc = await asyncio.create_subprocess_exec(
            *command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            env={**os.environ, **extra_env},
            cwd=repository_dir / "output",
        )
        with (repository_dir / "output" / DIRAC_LOG_FN).open("wt") as fp:
            await asyncio.gather(
                _handle_output(proc, "stdout", fp),
                _handle_output(proc, "stderr", fp),
                proc.wait(),
            )
        log.info("Test exited with code %r", proc.returncode)

    _set_job_status("LbAP Finalizing", "Killing upload daemon")
    uploader.cancel()
    await uploader
    try:
        await asyncio.wait_for(uploader, 600)
    except asyncio.TimeoutError:
        log.error("Uploader daemon did not terminate in time")

    _set_job_status("LbAP Finalizing", "Running checks")
    run_checks(test_info, output_dir)

    _set_job_status("LbAP Finalizing", "Processing complete")


def run_checks(test_info: dict, output_dir: str):
    tuple_indices = []
    for i, job in enumerate(test_info["jobs"]):
        # if len(job["checks"]) > 0:
        root_output_count = [
            ".root" in file_type.lower()
            for file_type in test_info["jobs"][i]["output"]["filetypes"]
        ].count(True)
        if root_output_count > 0:
            if root_output_count == 1:
                tuple_indices.append(i)
            else:
                raise NotImplementedError(
                    f"Multiple .root outputs for a single job {job['name']} detected!"
                )

    if len(tuple_indices) > 0:
        log.info("Running checks on the relevant job(s)")
        for job_index in tuple_indices:
            checks_returncode = job_checks(test_info, output_dir, job_index)
            log.info(
                f"Checks for job {test_info['jobs'][job_index]['name']}exited with code {checks_returncode}"
            )
    else:
        log.info("No checks requested for the test job(s)")


CHECK_SCRIPT_TEMPLATE = """

import sys
import json
from LbAPCommon.checks import run_job_checks
from LbAPCommon.checks.utils import checks_to_JSON, hist_to_root


exec_info_fp = sys.argv[1]
input_ntuple = sys.argv[2]
output_dir = sys.argv[3]
job_index = int(sys.argv[4])

with open(exec_info_fp, "r") as inf:
    test_info = json.load(inf)

test_info["jobs"][job_index]["input"] = test_info["jobs"][job_index]["input_data"]

job_info = test_info["jobs"][job_index]

job_name = job_info["name"]
checks = job_info["checks"]
checks_data = job_info["checks_data"]

jobs_info = {job_name: job_info}

check_results = run_job_checks(
    jobs_info,
    job_name,
    checks,
    checks_data,
    [input_ntuple],
)

hist_to_root(
    job_name, check_results, f"{output_dir}/checks.root"
)

check_results = {job_name: check_results}
checks_to_JSON(
    checks_data,
    check_results,
    json_output_path=f"{output_dir}/checks.json",
)

"""


def job_checks(test_info: dict, output_dir: str, job_index: int):
    """Run post-production checks defined for the given job."""
    import json
    from glob import glob
    from os.path import basename, isdir, join
    from tempfile import NamedTemporaryFile

    log.info("Starting checks")
    matches = glob(join(output_dir, "Local_*_JobDir/*"))
    if not matches:
        log.warning(
            f"Found no matches in {output_dir} for {test_info['jobs'][job_index]['name']}"
        )

    with NamedTemporaryFile(suffix=".py", mode="w") as check_script_fp:
        check_script_fp.write(CHECK_SCRIPT_TEMPLATE)
        check_script_fp.flush()
        for path in matches:
            if isdir(path):
                continue
            fn = basename(path)
            if any(
                re.match(
                    f"^00012345_00006789_{job_index+1}.*\\.{file_type}$",
                    fn,
                    re.IGNORECASE,
                )
                for file_type in test_info["jobs"][job_index]["output"]["filetypes"]
            ):
                with NamedTemporaryFile(suffix=".json", mode="w") as fp:
                    json.dump(test_info, fp)
                    fp.flush()
                    result = subprocess.run(
                        "source /cvmfs/lhcb.cern.ch/lib/LbEnv && "
                        f"python {check_script_fp.name} {fp.name} {path} {output_dir} {job_index} 2>&1 | tee {output_dir}/checks.log",
                        shell=True,
                        check=False,
                    )
                break
            else:
                log.info(
                    f"No ntuple found in {fn} for job {test_info['jobs'][job_index]['name']}"
                )
        else:
            # This can happen if the tupling doesn't produce any output
            log.info(
                f"No ntuple found in {output_dir} for job {test_info['jobs'][job_index]['name']}, could not run checks"
            )
            return -1

    out_files = [
        Path(output_dir) / f"checks.{file_type}"
        for file_type in ["json", "root", "log"]
    ]
    for out_file in out_files:
        if out_file.exists():
            _upload(out_file, test_info["jobs"][job_index]["s3"])

    return result.returncode


def _set_job_status(minor_status="", application_status=""):
    log.info("Updating job status minor=%r app=%r", minor_status, application_status)
    if JOB_ID is None:
        log.warning("Not setting job status as job_id is None")
        return
    try:
        from DIRAC.Core.Utilities.ReturnValues import convertToReturnValue
        from DIRAC.WorkloadManagementSystem.Client.JobReport import JobReport

        report = JobReport(JOB_ID, source="LbAnalysisProductions")
        result = report.setJobStatus(
            minorStatus=minor_status, applicationStatus=application_status
        )
        convertToReturnValue(result)
    except Exception:
        log.exception("Failed to update job status")


def _get_exec_info(pipeline_id, production_name, job_name, attempt, token, host):
    r = requests.get(
        f"{host}/pipelines/exec-info/",
        headers={
            "X-LBAP-PIPELINE-ID": str(pipeline_id),
            "X-LBAP-PRODUCTION-NAME": production_name,
            "X-LBAP-JOB-NAME": job_name,
            "X-LBAP-ATTEMPT": str(attempt),
            "X-LBAP-TEST-TOKEN": token,
        },
    )
    r.raise_for_status()
    return r.json()


def _get_dirac_yaml(pipeline_id, production_name, job_name, attempt, token, host):
    r = requests.get(
        f"{host}/pipelines/dirac-yaml/",
        headers={
            "X-LBAP-PIPELINE-ID": str(pipeline_id),
            "X-LBAP-PRODUCTION-NAME": production_name,
            "X-LBAP-JOB-NAME": job_name,
            "X-LBAP-ATTEMPT": str(attempt),
            "X-LBAP-DATAPKG-VERSION": "v999999999999",
            "X-LBAP-TEST-TOKEN": token,
        },
        params={"with_merging": False},
    )
    r.raise_for_status()
    return r.text


async def _handle_output(proc, stream_name, fp=None):
    stream = getattr(proc, stream_name)
    lines = []
    while line := await stream.readline():
        line = line.decode(errors="backslashreplace")
        log.debug(f"{stream_name}: %s", line.rstrip())
        lines += [line]
        if fp:
            fp.write(line)
    return "".join(lines)


async def _setup_repository_dir(tmpdir: Path, test_info):
    repository_dir = tmpdir / test_info["project_name"]

    command = ["git", "clone", test_info["repo_url"], repository_dir]
    log.info("Running: %r", command)
    proc = await asyncio.create_subprocess_exec(
        *command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    await asyncio.wait_for(
        asyncio.gather(
            _handle_output(proc, "stdout"), _handle_output(proc, "stderr"), proc.wait()
        ),
        timeout=120,
    )
    if proc.returncode != 0:
        raise RuntimeError(f"Exited with code {proc.returncode}")

    command = ["git", "checkout", test_info["commit_sha"]]
    log.info("Running: %r", command)
    proc = await asyncio.create_subprocess_exec(
        *command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=repository_dir
    )
    await asyncio.wait_for(
        asyncio.gather(
            _handle_output(proc, "stdout"), _handle_output(proc, "stderr"), proc.wait()
        ),
        timeout=30,
    )
    if proc.returncode != 0:
        raise RuntimeError(f"Exited with code {proc.returncode}")

    return repository_dir


async def _download_input_data(lfn, data_dir):
    fn = lfn.split("/")[-1]
    if (data_dir / fn).exists():
        log.info("Skipping download of %s as it already exists", fn)
        return
    log.info("Downloading %s", lfn)

    command = ["dirac-dms-lfn-accessURL", "--DiskOnly", "--Metalink", lfn]
    log.info("Running: %r", command)
    proc = await asyncio.create_subprocess_exec(
        *command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=data_dir
    )
    await asyncio.wait_for(
        asyncio.gather(
            _handle_output(proc, "stdout"), _handle_output(proc, "stderr"), proc.wait()
        ),
        timeout=120,
    )
    # Unfortunately it's normal for this to return an error code

    src = data_dir / f"{lfn.split('/')[-1]}.meta4"
    # HACK
    src.write_text(src.read_text().replace("mdf:root:", "root:"))
    n_sources = str(min(10, src.read_text().count("<url ")))
    command = ["xrdcp", "--nopbar", "--sources", n_sources, "--force", src, data_dir]
    log.info("Running: %r", command)
    proc = await asyncio.create_subprocess_exec(
        *command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=data_dir
    )
    await asyncio.wait_for(
        asyncio.gather(
            _handle_output(proc, "stdout"), _handle_output(proc, "stderr"), proc.wait()
        ),
        timeout=600,
    )
    if proc.returncode != 0:
        raise RuntimeError(f"Exited with code {proc.returncode}")


async def _uploader_loop(*args, **kwargs):
    try:
        while True:
            await _run_uploader(*args, **kwargs)
            await asyncio.sleep(60)
    except asyncio.CancelledError:
        # Run the uploader one last time
        log.info("Running uploader loop one last time")
        await _run_uploader(*args, **kwargs)
    except Exception:
        log.exception("Uploader loop failed")


async def _run_uploader(output_dir: Path, jobs: dict):
    log.debug("Running uploader for %s", output_dir)
    for filename in OUTPUT_DIR_FILENAMES:
        path = output_dir / filename
        if path.exists():
            for job in jobs:
                await asyncio.to_thread(_upload, path, job["s3"])

    matches = list(output_dir.glob("Local_*_JobDir/*"))
    if not matches:
        log.warning("Found no matches in %s", output_dir)
    for path in matches:
        if path.is_dir():
            continue
        for i, job in enumerate(jobs, start=1):
            for pattern in LOCAL_DIR_PATTERNS:
                pattern = pattern.format(application_name=job["application_name"], i=i)
                if "*" not in pattern:
                    if path.name != pattern:
                        continue
                elif not fnmatch(path.name, pattern):
                    continue

                await asyncio.to_thread(_upload, path, job["s3"])
                break
            else:
                if any(
                    re.fullmatch(
                        rf"^(00012345_00006789_.*\.{file_type}|.*\.tck\.json)$",
                        path.name,
                        re.IGNORECASE,
                    )
                    for file_type in job["output"]["filetypes"]
                ):
                    await asyncio.to_thread(_upload, path, job["s3"])
                else:
                    log.debug("Not uploading %s for %r", path.name, job["name"])


def _hash_file(path: Path):
    hasher = hashlib.md5()
    with path.open(mode="rb") as fp:
        while buf := fp.read(1024**2):
            hasher.update(buf)
    return hasher.hexdigest()


def _upload(path: Path, s3_data: dict, retries: int = 3):
    file_hash = _hash_file(path)
    if S3_HASH_CACHE.get(path.name, None) == file_hash:
        log.debug("Skipping as data already uploaded of %s", path)
        return

    log.debug("Uploading %s", path)
    try:
        with path.open(mode="rb") as fp:
            http_response = requests.post(
                s3_data["post_url"],
                data=s3_data["fields"],
                files={"file": (path.name, fp)},
            )
        assert http_response.status_code == 204
    except OverflowError:
        # This is a known issue in requests
        # https://github.com/psf/requests/issues/2717
        log.error("Overflow error uploading file, ignoring")
    except Exception:
        if retries < 1:
            raise
        log.exception("Failed to upload, will retry in 30 seconds")
        time.sleep(30)
        _upload(path, s3_data, retries=retries - 1)

    S3_HASH_CACHE[path.name] = file_hash


def _setup_lbrun_environment(
    siteroot: Path, repository_dir: Path, setup_cmt: bool = True
) -> dict[str, str]:
    """Set up the fake siteroot for lb-run to use when testing.

    Args:
        siteroot: The directory where the lb-run fake siteroot will be created
        repository_dir: Repository directory
        setup_cmt: whether to use fallback hack for CMT-style projects
    """
    extra_env = {"CMAKE_PREFIX_PATH": str(siteroot)}
    fake_dbase = siteroot / "DBASE"
    fake_install_dir = fake_dbase / "AnalysisProductions" / "v999999999999"
    os.makedirs(fake_install_dir.parent)
    os.symlink(repository_dir, fake_install_dir)
    if setup_cmt:
        log.info("Applying fallback hacks for CMT style projects")
        extra_env["User_release_area"] = str(siteroot)
        LHCB_DBASE_ROOT = Path("/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE")
        for dname in os.listdir(LHCB_DBASE_ROOT):
            if dname == "AnalysisProductions":
                continue
            os.symlink(LHCB_DBASE_ROOT / dname, fake_dbase / dname)
    log.info("extra_env = %s", extra_env)
    return extra_env


if __name__ == "__main__":
    asyncio.run(parse_args())
