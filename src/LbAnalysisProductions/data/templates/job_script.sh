#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail
IFS=$'\n\t'
set -x

# Rescedule if we hit an unsuitable node
unset PYTHONOPTIMIZE
lb-host-binary-tag
lb-host-binary-tag | grep -v '^x86_64'
lb-host-binary-tag | grep -v '^core2'

# Fix up the environment in DIRAC
unset REQUESTS_CA_BUNDLE SSL_CERT_DIR
unset DIRAC GFAL_PLUGIN_DIR DIRACLIB DIRACROOT DIRACSCRIPTS DIRACBIN ARC_PLUGIN_PATH GFAL_CONFIG_DIR
unset DIRACSYSCONFIG
unset RRD_DEFAULT_FONT
unset LD_LIBRARY_PATH DYLD_LIBRARY_PATH PYTHONPATH
# TODO: Clean PATH

# Use a fake home directory to prevent conda affecting other jobs
export HOME=${PWD}/home
mkdir -p "${HOME}"

set +u
{{ env_activation_command }}
set -u

# Ensure containers are working
export APPTAINER_BINDPATH=$PWD
python -c 'import LbPlatformUtils; print(LbPlatformUtils.dirac_platform(allow_containers=True))' | grep -E -- '-any$'

# TODO: Use an activation script in conda-forge
export DIRAC=$CONDA_PREFIX

python run_test.py \
    --host 'https://lbap.app.cern.ch' \
    --pipeline-id={{ pipline_id }} \
    --production-name={{ production_name }} \
    --job-name={{ job_name }} \
    --attempt={{ attempt }} \
    --token={{ token }} \
    ci
