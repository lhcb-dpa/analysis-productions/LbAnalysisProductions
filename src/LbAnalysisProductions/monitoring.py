###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
from functools import partial
from textwrap import dedent

import requests

from LbAnalysisProductions.celery import app
from LbAnalysisProductions.logging import log
from LbAnalysisProductions.settings import settings
from LbAnalysisProductions.utils import send_email_alert, suppress_exceptions

# Tasks for which we can should try to mark the GitLab job as failed
KNOWN_TASKS = [
    f"LbAnalysisProductions.celery.{name}"
    for name in ["start_job", "monitor_job", "submit_productions"]
]


def monitor_celery():
    # Pass all tasks to "state.event" to so it can memorise properties which
    # are only sent with some types of message
    state = app.events.State()

    with app.connection() as connection:
        recv = app.events.Receiver(
            connection,
            handlers={
                "task-failed": partial(announce_failed_tasks, state),
                "*": state.event,
            },
        )
        recv.capture(limit=None, timeout=None, wakeup=True)


@suppress_exceptions
def announce_failed_tasks(state, event):
    state.event(event)
    task = state.tasks.get(event["uuid"])
    job_id = None
    if match := re.search(r'"id": (\d+), "token": "([^"]+)"', task.args):
        job_id, token = match.groups()
        job_id = int(job_id)

        if task.name in KNOWN_TASKS and job_id is not None:
            data = {
                "token": token,
                "state": "failed",
                "failure_reason": "runner_system_failure",
            }
            response = requests.put(
                f"{settings.gitlab.instance}/api/v4/jobs/{job_id}",
                json=data,
                timeout=120,
            )
            if not response.ok:
                log.error(f"Failed to mark {job_id} as failed: {response.text}")

    message = f"""
        There was a problem while running a task {event['uuid']} on {event['hostname']}
        for job #{job_id} at {event['timestamp']}.

        {event['exception']}

        {event['traceback']}

        To understand the problem further, please check the logs:
        {settings.gitlab.instance}/{settings.gitlab_data_repository}/-/jobs/{job_id}

        N.B. This is an automated email, and replies are not monitored.
    """
    message = dedent(message)
    log.error(message)

    send_email_alert(job_id, subject="Celery task failed", content=message)


if __name__ == "__main__":
    monitor_celery()
