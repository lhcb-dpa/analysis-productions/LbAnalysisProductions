###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "log",
]

import logging
import os

import colorlog
import psutil
from python_logging_rabbitmq import RabbitMQHandler

from LbAnalysisProductions.settings import settings

VERBOSE = 5
logging.addLevelName(VERBOSE, "VERBOSE")
ALWAYS = 1000
logging.addLevelName(ALWAYS, "ALWAYS")

logging.getLogger("gitlab_runner_api").setLevel(logging.WARNING)


def verbose(msg, *args, **kwargs):
    if log.isEnabledFor(VERBOSE):
        log._log(VERBOSE, msg, args, **kwargs)


def always(msg, *args, **kwargs):
    if log.isEnabledFor(ALWAYS):
        log._log(ALWAYS, msg, args, **kwargs)


class CallbackHandler(logging.StreamHandler):
    def __init__(self):
        super().__init__()
        self.callback = None

    def emit(self, record):
        super().emit(record)
        if self.callback is not None:
            msg = callback_formatter.format(record)
            self.callback(record, msg)  # pylint: disable=not-callable


class CurrentTaskInfoFilter(logging.Filter):
    """Filter to add extra fields before log the record."""

    def __init__(self):
        super().__init__()
        self._proc = {}

    @property
    def proc(self):
        current_pid = os.getpid()
        if current_pid not in self._proc:
            self._proc[current_pid] = psutil.Process(current_pid)
        return self._proc[current_pid]

    def filter(self, record):
        import celery

        if celery.current_task and celery.current_task.request:
            kwargs = celery.current_task.request.kwargs
            if "pipeline_id" in kwargs:
                record.gitlab_pipeline_id = kwargs["pipeline_id"]
            if "job_id" in kwargs:
                record.gitlab_job_id = kwargs["job_id"]

        record.memory_stats = dict(self.proc.memory_info()._asdict())

        return True


log_colors = {
    "ALWAYS": "blue",
    "FATAL": "purple",
    "ERROR": "red",
    "WARN": "yellow",
    "WARNING": "yellow",
    "INFO": "green",
    "DEBUG": "white",
    "VERBOSE": "cyan",
}

callback_formatter = colorlog.ColoredFormatter(
    "%(log_color)s%(levelname)s:%(reset)s%(message)s",
    reset=True,
    log_colors=log_colors,
    secondary_log_colors={},
    style="%",
)


handler = CallbackHandler()
handler.setFormatter(
    colorlog.ColoredFormatter(
        "%(log_color)s%(asctime)s %(levelname)s:%(name)s:%(reset)s%(message)s [%(thread)d]",
        reset=True,
        log_colors=log_colors,
        secondary_log_colors={},
        style="%",
    )
)

log = logging.getLogger("AnaProd")
log.addHandler(handler)
log.setLevel("VERBOSE")
if settings.logging_broker:
    rabbit_handler = RabbitMQHandler(
        host=settings.logging_broker.host,
        port=settings.logging_broker.port,
        username=settings.logging_broker.user,
        password=settings.logging_broker.password,
        declare_exchange=False,
        connection_params={
            "virtual_host": settings.logging_broker.vhost,
            "connection_attempts": 3,
            "socket_timeout": 5000,
        },
    )
    rabbit_handler.addFilter(CurrentTaskInfoFilter())
    log.addHandler(rabbit_handler)
    # Also add the rabbit handler to other loggers
    logging.getLogger("gitlab_runner_api").addHandler(rabbit_handler)
else:
    log.warning("No RabbitMQ credentials found, disabling")

# Add the extra log methods
log.verbose = verbose
log.always = always
log.always("Configured logging handlers")
