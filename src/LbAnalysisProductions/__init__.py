###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "settings",
    "models",
    "utils",
]

from os.path import basename, dirname

import jinja2

templates = jinja2.Environment(
    loader=jinja2.PackageLoader("LbAnalysisProductions", "data/templates"),
    undefined=jinja2.StrictUndefined,
)  # NOQA
templates.globals.update(
    zip=zip,
    basename=basename,
    dirname=dirname,
    in_kb=lambda size: f"{size / 1024 ** 1:.1f} KB",
    in_mb=lambda size: f"{size / 1024 ** 2:.1f} MB",
    in_gb=lambda size: f"{size / 1024 ** 3:.1f} GB",
    in_tb=lambda size: f"{size / 1024 ** 4:.1f} TB",
    in_hours=lambda time, norm: (
        f"{time / 60 / 60:.1f} ({time / 60 / 60 * norm:.1f} HS06)"
        if isinstance(time, float)
        else time
    ),
)  # NOQA


from . import models  # NOQA
from . import utils  # NOQA
from .settings import settings  # NOQA

if settings.sentry_dsn:
    import sentry_sdk

    sentry_sdk.init(settings.sentry_dsn)  # pylint: disable=abstract-class-instantiated
if settings.db_url:
    from sqlalchemy import create_engine

    from LbAnalysisProductions.models import Base

    engine = create_engine(str(settings.db_url), echo=False)
    Base.metadata.create_all(engine)
