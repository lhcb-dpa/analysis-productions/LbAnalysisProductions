###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "TestStatusEnum",
    "contexted",
    "RunAsDIRACUser",
    "DownloadedProxyFile",
    "suppress_exceptions",
    "send_email_alert",
]

import enum
import smtplib
import subprocess
from collections import defaultdict
from contextlib import contextmanager
from email.message import EmailMessage
from functools import wraps
from tempfile import NamedTemporaryFile
from threading import Lock

import sqlalchemy
from cachetools import TTLCache, cached
from sqlalchemy.orm import scoped_session, sessionmaker

from LbAnalysisProductions.exceptions import LbAPJobFailed
from LbAnalysisProductions.logging import log
from LbAnalysisProductions.settings import settings


class TestStatusEnum(enum.Enum):
    """States that a job test can be in

    State machine is enforced by Test.validate_status
    """

    UNKNOWN = 0
    RUNNING = 1
    SUCCESS = 2
    FAILED = 3

    def __len__(self):
        return 4


class ExecutionBackendEnum(enum.Enum):
    LOCAL = 0
    KUBERNETES = 1
    DIRAC = 2

    def __len__(self):
        return 2


@contextmanager
def contexted(Session=None):
    """Provide a transactional scope around a series of operations."""
    if Session is None:
        engine = sqlalchemy.create_engine(str(settings.db_url), echo=False)
        session = scoped_session(sessionmaker(bind=engine))()
    else:
        session = Session()

    try:
        yield session
        session.commit()
    except BaseException:
        session.rollback()
        raise
    finally:
        session.close()


@cached(cache=TTLCache(1, 10 * 60), lock=Lock())
def dirac_username_mapping():
    """Extrace a mapping of CERN usernames to DIRAC nicknames

    The result is cached for up to 10 minutes.
    """
    from DIRAC.ConfigurationSystem.Client.CSAPI import CSAPI
    from dirac_prod.utils import dw

    users = dw(CSAPI().describeUsers())
    mapping = defaultdict(list)
    for nickname, attributes in users.items():
        if "PrimaryCERNAccount" not in attributes:
            log.warning("PrimaryCERNAccount not found for %s", nickname)
            continue
        if attributes.get("CERNAccountType") not in ["Primary", "Secondary", "Service"]:
            continue
        mapping[attributes["PrimaryCERNAccount"]].append(nickname)

    for cern_account, nicknames in list(mapping.items()):
        if len(nicknames) == 1:
            mapping[cern_account] = nicknames.pop()
        else:
            del mapping[cern_account]
            log.warning("Multiple accounts found for %s: %s", cern_account, nicknames)

    return dict(mapping)


class RunAsDIRACUser:
    def __init__(self, cern_user, group=None, check_proxy=True):
        from DIRAC.ConfigurationSystem.Client.Helpers import Registry
        from DIRAC.FrameworkSystem.Client.ProxyManagerClient import gProxyManager
        from dirac_prod.utils import dw

        self._cern_user = cern_user
        nickname = dirac_username_mapping().get(self._cern_user)
        if not nickname:
            raise LbAPJobFailed(
                f"Failed to find a corresponding DIRAC user for {self._cern_user}.\n"
                + "Please make sure you are registered in DIRAC, see the twiki for details:\n"
                "    https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/Certificate"
            )
        self._nickname = nickname
        self._dn = dw(Registry.getDNForUsername(self._nickname))[0]
        # TODO: We can do something clever here with groups in DIRAC v7r1 to improve turnaround time
        self._group = group or dw(Registry.findDefaultGroupForUser(self._nickname))
        if check_proxy:
            # There should be no need for this but userHasProxy has proxy
            # doesn't understand groupless proxies and for the next year
            # we have a mess of both. This makes pandas sad and can be
            # removed in February 2022. (TODO)
            for group in [self._group, ""]:
                has_valid_proxy = gProxyManager.userHasProxy(
                    self._dn, group, validSeconds=6 * 24 * 60 * 60
                )
                if dw(has_valid_proxy):
                    break
            else:
                raise LbAPJobFailed(
                    f"Failed to find a valid proxy with group {self._dn} for {self._group}.\n"
                    + "Try running lhcb-proxy-init on lxplus"
                )

    def __enter__(self):
        from DIRAC.Core.DISET.ThreadConfig import ThreadConfig

        log.verbose(
            "Setting up credentials for CERN=%s as DIRAC=%s DN=%s group=%s",
            self._cern_user,
            self._nickname,
            self._dn,
            self._group,
        )
        self._tc = ThreadConfig()
        if self._tc.getDN():
            raise NotImplementedError(self._tc.dump())
        self._tc.setDN(self._dn)
        self._tc.setGroup(self._group)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._tc.reset()


@cached(cache=TTLCache(maxsize=1024, ttl=60 * 60 * 12))
def get_proxy_data(username, group):
    with NamedTemporaryFile() as proxy_file:
        subprocess.run(
            settings.lb_dirac
            + [
                "dirac-admin-get-proxy",
                "--option=/DIRAC/Security/UseServerCertificate=yes",
                f"--option=/DIRAC/Security/CertFile={settings.dirac_host_cert.path}",
                f"--option=/DIRAC/Security/KeyFile={settings.dirac_host_key.path}",
                f"--out={proxy_file.name}",
                username,
                group,
            ],
            check=True,
        )
        return proxy_file.read()


@contextmanager
def DownloadedProxyFile(username, group):
    with NamedTemporaryFile() as proxy_file:
        proxy_file.write(get_proxy_data(username, group))
        proxy_file.flush()
        proxy_file.seek(0)
        yield proxy_file


def suppress_exceptions(f):
    """Decorator to suppress exceptions and log them, returning None instead"""

    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception:
            log.exception(f"Exception caught while calling {f.__name__}")

    return wrapper


@suppress_exceptions
def send_email_alert(job_id, subject, content):
    """Send an email alert email to the analysis productions managers"""
    msg = EmailMessage()
    msg.set_content(content)
    msg["Subject"] = f"AnalysisProductions Job {job_id} - {subject}"
    msg["From"] = "lhcb-analysis-productions@cern.ch"
    msg["To"] = [settings.alert_email]

    with smtplib.SMTP("cernmx.cern.ch", timeout=2) as smtp:
        smtp.send_message(msg)
