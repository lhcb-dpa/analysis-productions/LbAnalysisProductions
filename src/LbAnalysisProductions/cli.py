###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import typer

app = typer.Typer(
    help="Admin commands for performing common analysis productions tasks"
)
production_app = typer.Typer()
app.add_typer(production_app, name="production")


@production_app.command("submit")
def production_submit(
    pipeline_id: int,
    tag: str,
    dry_run: bool = True,
    create_jira: bool = False,
    clean_repo: bool = False,
):
    """Submit a proaction and create the associated JIRA task"""
    from LbAnalysisProductions.submission import submit_productions

    submit_productions(
        pipeline_id, tag, dry_run=dry_run, create_jira=create_jira, clean_up=clean_repo
    )


if __name__ == "__main__":
    app()
