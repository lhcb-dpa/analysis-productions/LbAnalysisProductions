###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from celery import Celery
from gitlab_runner_api import Job as GitLabJob
from gitlab_runner_api import Runner as GitLabRunner
from gitlab_runner_api import failure_reasons as gitlab_failure_reasons

from LbAnalysisProductions import job_configuration
from LbAnalysisProductions.logging import log
from LbAnalysisProductions.settings import settings

from .gitlab_runner import (  # do_submission,
    kill_running,
    monitor_gitlab_job,
    monitor_steps,
    start_gitlab_job,
    tag_data_pkg,
)

app = Celery(
    "LbAnalysisProductions.celery",
    broker=settings.celery_broker,
    backend="rpc://",
)
app.conf.task_ignore_result = True
app.conf.worker_max_memory_per_child = 200000  # 200 MB
app.conf.worker_send_task_events = True


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    runner_data = settings.gitlab_runner_data
    assert runner_data is not None
    # Check for new pipelines every 30 seconds
    sender.add_periodic_task(
        5.0, pull_job.s(runner_data), expires=10, name="pull-gitlab-jobs"
    )


@app.task
def pull_job(runner_data):
    # log.debug("pull_job(%r)", runner_data)
    runner = GitLabRunner.loads(runner_data)
    gitlab_job = runner.request_job()
    if gitlab_job is None:
        log.debug("No job received, exiting")
        return
    log.debug("Starting job")
    if gitlab_job._job_info["job_info"]["name"] == "test":
        start_job.s(gitlab_job.dumps()).apply_async(countdown=30)
    else:
        gitlab_job.log += "ERROR: Unsupported GitLab CI job name\n"
        gitlab_job.set_failed(gitlab_failure_reasons.RunnerSystemFailure())


@app.task
def start_job(job_data):
    gitlab_job = GitLabJob.loads(job_data)
    result = start_gitlab_job(gitlab_job)
    if result is not None:
        monitor_job.s(
            gitlab_job.dumps(),
            *result,
            pipeline_id=int(gitlab_job.pipeline_id),
            job_id=int(gitlab_job.id)
        ).apply_async(countdown=30)


@app.task
def monitor_job(job_data, production_ids, running_step_ids, pipeline_id=0, job_id=0):
    gitlab_job = GitLabJob.loads(job_data)

    log.debug("Monitoring job")
    try:
        monitor_steps(gitlab_job, running_step_ids)

        done, failed_steps = monitor_gitlab_job(gitlab_job, production_ids)

        # if this is true, only a dry-run submission will be attempted
        submission_dry_run = gitlab_job._job_info["git_info"]["ref"] != "master"

        if not done:
            # The job isn't done, rerun the monitoring after 30 seconds
            task = monitor_job.s(
                gitlab_job.dumps(),
                production_ids,
                running_step_ids,
                pipeline_id=pipeline_id,
                job_id=job_id,
            )
            task.apply_async(countdown=30)
        elif failed_steps:
            gitlab_job.set_failed(gitlab_failure_reasons.ScriptFailure())
        elif not submission_dry_run:
            tag_name = tag_data_pkg(gitlab_job)

            task = submit_productions.s(gitlab_job.dumps(), tag_name)
            task.apply_async(countdown=120)
        elif submission_dry_run:
            task = submit_productions.s(
                gitlab_job.dumps(), gitlab_job.commit_sha[:8], dry_run=True
            )
            task.apply_async(countdown=120)
        else:
            gitlab_job.set_success()

    except Exception:
        log.exception("Fatal error monitoring %s", gitlab_job)
        # Ensure all steps are always marked as not running
        kill_running(production_ids)


@app.task()
def submit_productions(job_data, tag_name, dry_run=False):
    gitlab_job = GitLabJob.loads(job_data)
    # if do_submission(gitlab_job, tag_name, dry_run=dry_run):
    gitlab_job.set_success()
    # else:
    #     task = submit_productions.s(gitlab_job.dumps(), tag_name, dry_run=dry_run)
    #     task.apply_async(countdown=120)


@app.task(ignore_result=False)
def bk_query_info(bk_path, is_turbo, override_filetype=None):
    log.debug("Getting database tags for %s", bk_path)
    return job_configuration.from_bk_query(bk_path, is_turbo, override_filetype)


@app.task(ignore_result=False)
def lookup_lfns(bk_path, n_lfns=1):
    log.debug("Getting %d LFNs for %s", n_lfns, bk_path)
    return job_configuration.lookup_lfns(bk_path, n_lfns)
