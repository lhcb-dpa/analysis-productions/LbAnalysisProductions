###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "changes",
    "changed_productions",
]

import os
import re
from itertools import combinations
from os.path import abspath, join, relpath
from pathlib import Path

import git
from gitdb.exc import BadName

from LbAnalysisProductions.logging import log


def changes(repo_base, prefix=".", depth=0, regex=None, is_merge_request=False):
    repo_base = abspath(repo_base)
    assert not prefix.startswith("/"), prefix
    prefix = relpath(abspath(join(repo_base, prefix)), repo_base)
    log.verbose("Running git.changes(repo_base=%s, prefix=%s,)", repo_base, prefix)

    repo = git.Repo(repo_base)
    if regex:
        regex = re.compile("^%s$", regex)

    if is_merge_request:
        common_ancestor = repo.merge_base("HEAD", "origin/master")
        assert len(common_ancestor) == 1, common_ancestor
        common_ancestor = common_ancestor[0]
        diffs = common_ancestor.diff("HEAD")
    else:
        diffs = repo.commit("HEAD~1").diff("HEAD")

    changes = set()
    for diff in diffs:
        log.verbose(
            "Found change: a_path=%s b_path=%s new_file=%s "
            "renamed_file=%s deleted_file=%s",
            diff.a_path,
            diff.b_path,
            diff.new_file,
            diff.renamed_file,
            diff.deleted_file,
        )

        if diff.deleted_file:
            continue
        full_path = diff.b_path

        if full_path is None:
            continue
        if not (prefix == "." or full_path.startswith(prefix + os.sep)):
            continue
        full_path = relpath(full_path, prefix)
        result = full_path
        if depth:
            result = join(*full_path.split(os.sep)[:depth])
        if regex:
            match = regex.match(full_path)
            log.debug(
                'Pattern "%s" matches %s with %s', regex.pattern, full_path, match
            )
            if not match:
                continue
        changes.add(result)

    return sorted(changes)


def changed_productions(repo_base, is_merge_request):
    if (Path(repo_base) / "dynamic").exists():
        raise RuntimeError('Found object named "dynamic" in the root of the repository')

    # Production directories are those that contain a file named info.yaml
    production_dirs = {
        relpath(root, repo_base)
        for root, dirnames, filenames in os.walk(repo_base)
        if "info.yaml" in filenames
    }
    nested_productions = [
        (a, b)
        for a, b in combinations(production_dirs, 2)
        if not relpath(a, b).startswith("..")
    ]
    if nested_productions:
        raise RuntimeError(f"Found nested productions {nested_productions}")

    # Find production directories which were changed by the last commit
    changed_productions = set()
    for fn in changes(repo_base, is_merge_request=is_merge_request):
        for d in production_dirs:
            if not relpath(fn, d).startswith(".."):
                changed_productions.add(d)
                break
    log.debug("Changed productions are %s", changed_productions)
    return changed_productions


def find_matching_releases(production, repo):
    """
    Basic search for tags matching a previously deployed production
    with name matching 'production'.
    """
    matching_releases = set()
    for tag in repo.tags:
        # see if this tag relates to this production.
        try:
            diffs = repo.commit(f"{tag.name}~1^1").diff(f"{tag.name}~1")
        except BadName:
            # some tags won't play nice when we diff with them like this, so skip them.
            log.debug(
                f"find_matching_releases: could not diff {tag.name}~1^1...{tag.name}~1 (BadName)"
            )
        else:
            for diff in diffs or []:
                if diff.deleted_file:
                    continue
                if diff.b_path.startswith(f"{production}/"):
                    matching_releases.add(tag)
                    break
    return matching_releases
