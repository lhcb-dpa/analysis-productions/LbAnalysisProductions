###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import requests
from LbAPCommon import cern_sso

CERN_JIRA_INSTANCE = "https://its.cern.ch/jira"
REST_API_URL_PREFIX = "/rest/api/2"


class CERNJira:
    """
    This class uses the REST v2 API and the documentation at
    https://developer.atlassian.com/cloud/jira/platform/rest/v2/intro/#about.

    N.B. if CERN upgrades their JIRA instance to the latest version, some aspects of this
    class will need to be changed as well, i.e., using accountId rather than user names, or switching
    to v3 of the REST API.
    """

    def __init__(
        self, instance=CERN_JIRA_INSTANCE, api_prefix=REST_API_URL_PREFIX, cookies=None
    ):
        self.instance = instance
        self.api_prefix = api_prefix
        self.cookies = self._sign_in() if cookies is None else cookies

    def _sign_in(self):
        return cern_sso.sign_in(self.instance + "/loginCern.jsp")

    def _check_ok(self, r):
        if r.status_code not in [200, 201]:
            # log.error(f"Response from JIRA API: {r.status_code}")
            print(f"Response from JIRA API: {r.status_code}")
            for line in r.iter_lines():
                # log.error(line)
                print(line)

        r.raise_for_status()

        return r.ok

    def _make_request(self, func, api_url, params=None, data=None, json=None):
        headers = {"Accept": "application/json", "Content-Type": "application/json"}

        r = func(
            self.instance + self.api_prefix + api_url,
            data=data,
            json=json,
            params=params,
            headers=headers,
            cookies=self.cookies,
        )
        self._check_ok(r)

        return r

    def get_issue(self, issueIdOrKey):
        """
        Return details for an issue
        https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-issues/#api-rest-api-2-issue-issueidorkey-get
        """

        api_url = f"/issue/{issueIdOrKey}"

        r = self._make_request(
            requests.get,
            api_url,
        )

        return r.json()

    def get_create_issue_meta(self, projectKeys=None, projectIds=None):
        """
        Get create issue metadata
        https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-issues/#api-rest-api-2-issue-createmeta-get
        """

        api_url = "/issue/createmeta"

        params = {}

        if projectKeys is not None:
            params["projectKeys"] = ",".join(projectKeys)

        if projectIds is not None:
            params["projectIds"] = ",".join(projectIds)

        r = self._make_request(requests.get, api_url)

        return r.json()

    def create_issue(
        self, project_key, title, description, issuetype="Task", priority="Minor"
    ):
        """
        Create an issue
        https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-issues/#api-rest-api-2-issue-post
        """

        api_url = "/issue/"

        request = {
            "fields": {
                "project": {"key": project_key},
                "summary": title,
                "description": description,
                "issuetype": {"name": issuetype},
                "priority": {"name": priority},
            }
        }

        r = self._make_request(requests.post, api_url, json=request)
        return r.json()

    def get_user_by_query(self, query, require_single_result=True):
        """
        Query JIRA for a single (or multiple) user(s).
        https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-user-search/#api-rest-api-2-user-picker-get

        If require_single_result is True, the default, then don't return anything if
        more than one user is found, else return a list of all users in the query.

        The username can be extracted from response[N]["name"] or
        response["name"] if require_single_response is True
        """

        pars = {"query": query}

        api_url = "/user/picker"

        r = self._make_request(
            requests.get,
            api_url,
            params=pars,
        )
        response = r.json()

        if len(response.get("users", [])) == 0:
            return False

        if require_single_result:
            if len(response["users"]) > 1:
                return False  # query was ambiguous - return nothing
            return response["users"][0]

        return response["users"]

    def add_issue_watcher(self, issueIdOrKey, username):
        """
        Add a watcher identified by 'username' to the specified issue ID or key.
        https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-issue-watchers/#api-rest-api-2-issue-issueidorkey-watchers-post.
        """

        api_url = f"/issue/{issueIdOrKey}/watchers"

        r = self._make_request(requests.post, api_url, json=username)
        return r.ok

    def update_issue(
        self,
        issueIdOrKey,
        title=None,
        description=None,
        issuetype=None,
        priority=None,
        labels=None,
    ):
        """
        Update/Edit an issue on JIRA
        https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-issues/#api-rest-api-2-issue-issueidorkey-put
        """

        api_url = f"/issue/{issueIdOrKey}"

        request = {}

        def add_field(name, value):
            if value is None:
                return
            if "fields" not in request:
                request["fields"] = {}

            request["fields"][name] = value

        add_field("summary", title)
        add_field("description", description)
        add_field("issuetype", issuetype)
        add_field("labels", labels)

        if priority is not None:
            add_field("priority", {"name": priority})

        r = self._make_request(requests.put, api_url, json=request)

        return r.json()

    def comment_on_issue(self, issueIdOrKey, body):
        """
        Comment on a JIRA issue
        https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-issue-comments/#api-group-issue-comments
        """

        api_url = f"/issue/{issueIdOrKey}/comment"
        request = {"body": body}

        r = self._make_request(requests.post, api_url, json=request)

        return r.ok and r.status_code == 201
