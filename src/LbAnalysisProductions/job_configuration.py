###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "from_bk_query",
    "from_bk_query_job",
    "from_job_name",
    "from_transform_ids",
]

import random
import re

from LbAnalysisProductions.logging import log


def lookup_lfns(bk_query, n_lfns=1):
    from LHCbDIRAC.BookkeepingSystem.Client.BKQuery import BKQuery

    lfns = BKQuery(bk_query).getLFNs()
    if len(lfns) >= n_lfns:
        return random.sample(lfns, n_lfns)
    else:
        raise ValueError(
            f"Requests {n_lfns} LFNs but only found {len(lfns)} LFNs in {bk_query}"
        )


def from_bk_query(bk_query, is_turbo, override_filetype=None):
    from DIRAC import gConfig
    from LHCbDIRAC.BookkeepingSystem.Client.BKQuery import BKQuery

    log.debug("Getting info from bookkeeping %s", bk_query)
    result = {"input_type": parse_input_type(override_filetype or bk_query, is_turbo)}

    config_name, config_version = bk_query.split("/")[1:3]
    if config_name in {"LHCb", "validation"}:
        result["simulation"] = False

        match = re.match(r"[^0-9]+(\d\d)(?:[^\d].*)?", config_version)
        if not match:
            raise ValueError(f"Failed to parse config version ({config_version})")
        year = int(match.groups()[0])
        result["data_type"] = str(year if year > 2000 else year + 2000)
    elif config_name == "MC":
        result["simulation"] = True
        result["data_type"] = config_version
    elif gConfig.getValue("/DIRAC/Setup") == "LHCb-Certification":
        result["simulation"] = True
        cond = BKQuery(bk_query).getConditions()
        result["data_type"] = re.findall(r"[^\d](20[\d]{2})[^\d]", cond)[0]
    else:
        raise NotImplementedError(f"Failed to parse config name ({bk_query})")
    if config_version not in ["Upgrade", "Dev"] and not (
        2011 <= int(result["data_type"]) <= 2025
    ):
        raise ValueError(f"Failed to parse config version ({config_version})")

    if result["simulation"]:
        from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import (
            BookkeepingClient,
        )
        from LHCbDIRAC.TransformationSystem.Client.TransformationClient import (
            TransformationClient,
        )

        dddb_tags = set()
        conddb_tags = set()
        for prodID in BKQuery(bk_query).getBKProductions():
            while isinstance(prodID, int):
                res = BookkeepingClient().getProductionInformation(prodID)
                if not res["OK"]:
                    raise RuntimeError(res)
                for _, _, _, _, dddb, conddb, _, _, _ in res["Value"]["Steps"]:
                    if dddb:
                        dddb_tags.add(dddb)
                    if conddb:
                        conddb_tags.add(conddb)
                # In some cases we have to inspect parent transformations to find tags
                parent = (
                    TransformationClient()
                    .getBookkeepingQuery(prodID)
                    .get("Value", {})
                    .get("ProductionID", "")
                )
                log.debug("Found parent production ID %s for %s", parent, prodID)
                prodID = parent

        if len(dddb_tags) != 1 or len(conddb_tags) != 1:
            message = (
                f"Error obtaining database tags for: {bk_query}\n"
                f"  * dddb_tags={dddb_tags!r}\n"
                f"  * conddb_tags={conddb_tags!r}\n"
                "This probably means your bookkeeping path is incorrect. If this "
                "isn't the case, Please open a bug report at https://gitlab.cern.ch/"
                "lhcb-dpa/analysis-productions/LbAnalysisProductions/-/issues"
            )
            raise NotImplementedError(message)
        assert len(dddb_tags) == 1, dddb_tags
        assert len(conddb_tags) == 1, conddb_tags
        assert dddb_tags != {None}, "Simulated database tags should never be None"
        assert conddb_tags != {None}, "Simulated database tags should never be None"

        result["dddb_tag"] = dddb_tags.pop()
        result["conddb_tag"] = conddb_tags.pop()

    return result


def from_bk_query_job(job):
    bk_query = job.input_data["bk_query"]
    log.debug("Setting input of %s from bookkeeping %s", job, bk_query)

    result = from_bk_query(bk_query, job.turbo)
    for k, v in result.items():
        if getattr(job, k) is None:
            setattr(job, k, v)


def from_job_name(job):
    input_job = job.parent
    log.debug("Setting input of %s from job %s", job, input_job)

    if not input_job.automatically_configure:
        raise ValueError("Can only automatically configure jobs with automatic input")

    if not input_job.autoconf_options:
        raise NotImplementedError("This shouldn't happen")

    if len(job.output) != 1:
        raise ValueError(
            f"Only input from jobs with one output is supported {job.output}"
        )

    if job.input_type is None:
        job.input_type = parse_input_type(input_job.output[0], job.turbo)
    if job.simulation is None:
        job.simulation = input_job.simulation
    if job.data_type is None:
        job.data_type = input_job.data_type
    if job.simulation:
        if job.dddb_tag is None:
            job.dddb_tag = input_job.dddb_tag
        if job.conddb_tag is None:
            job.conddb_tag = input_job.conddb_tag


def from_transform_ids(job):
    from dirac_prod.classes.input_dataset import find_bk_query_from_tid

    transform_ids = job.input_data["transform_ids"]
    filetype = job.input_data["filetype"]

    log.debug("Finding input path of %s from bookkeeping", job)
    bk_query = find_bk_query_from_tid(transform_ids, filetype)
    log.debug("Found input path of %s from bookkeeping: %r", job, bk_query)

    result = from_bk_query(bk_query, job.turbo)
    for k, v in result.items():
        if getattr(job, k) is None:
            setattr(job, k, v)


def parse_input_type(file_path, is_turbo):
    extension = file_path.split("/")[-1].split(".")[-1]
    if extension in {"DST", "LDST", "MDST", "XDST"}:
        # Turbo jobs always require MDST
        # https://twiki.cern.ch/twiki/bin/view/LHCb/MakeNTupleFromTurbo
        result = "MDST" if is_turbo else extension
    else:
        raise NotImplementedError(f"Failed to set input type from {file_path!r}")
    log.debug("Set input type of %s for %s", result, file_path)
    return result
