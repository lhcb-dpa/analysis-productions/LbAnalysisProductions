###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import logging
from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
from itertools import chain
from os.path import basename, isdir, join
from tempfile import TemporaryDirectory

from dirac_prod.classes.input_dataset import BookkeepingLFN
from git import GitCommandError, Repo
from gitlab_runner_api import failure_reasons as gitlab_failure_reasons
from LbAPCommon import validators

from LbAnalysisProductions.data_pkg_tagging import prepare_dynamic_options
from LbAnalysisProductions.exceptions import LbAPJobFailed
from LbAnalysisProductions.git import changed_productions
from LbAnalysisProductions.logging import handler, log
from LbAnalysisProductions.models import (
    InputFile,
    OutputFile,
    Pipeline,
    Production,
    Step,
    Test,
)
from LbAnalysisProductions.settings import settings
from LbAnalysisProductions.submission import submit_productions
from LbAnalysisProductions.test_backends import BaseTest, DiracTest
from LbAnalysisProductions.utils import TestStatusEnum, contexted


def start_gitlab_job(gitlab_job, TestClass=DiracTest):
    # log.verbose('Running start_gitlab_job(%r, %r)', gitlab_job.dumps(), TestClass)

    def logger_callback(record, msg):
        if record.levelno >= logging.INFO:
            gitlab_job.log += msg + "\n"

    handler.callback = logger_callback
    step_ids_to_submit = defaultdict(list)
    already_running_step_ids = defaultdict(list)

    try:
        ensure_pipeline_registered(gitlab_job)
        production_ids = create_productions(gitlab_job)
    except LbAPJobFailed as e:
        log.critical(f"Error creating production: {e}")
        while e := e.__context__:
            log.warning(f"{e}")
        gitlab_job.set_failed(gitlab_failure_reasons.ScriptFailure())
        handler.callback = None
        log.exception("Error creating productions")
        return None
    except Exception:
        log.critical(
            "Failed to start for unknown reason, see worker logs for more information"
        )
        gitlab_job.set_failed(gitlab_failure_reasons.RunnerSystemFailure())
        handler.callback = None
        log.exception("Failed to start for unknown reason")
        return None

    try:
        for production_id in production_ids:
            with contexted() as session:
                production = session.query(Production).get(production_id)
                log.always("Submitting jobs for %s", production.name)
                for step in production.steps:
                    if step.running:
                        log.info("Skipping %s: Already running", step)
                        already_running_step_ids[production_id] += [step.id]
                    elif step.completed_successfully:
                        log.info("Skipping %s: Already passed", step)
                    elif not step.can_run:
                        log.info("Skipping %s: Not ready", step)
                    else:
                        for job in step.jobs:
                            test = Test.create(job)
                            session.add(test)
                        step_ids_to_submit[production_id] += [step.id]

        for production_id, step_ids in step_ids_to_submit.items():
            futures = []
            with ThreadPoolExecutor(max_workers=10) as e:
                for i, step_id in enumerate(step_ids, start=1):
                    futures.append(
                        e.submit(launch_test, TestClass, step_id, step_ids, i)
                    )

            # Exceptions in the threadpool are lost, so manually raise them here if needed
            failures = []
            for i, future in enumerate(futures):
                exception = future.result()
                if exception is None:
                    # Test passed
                    continue

                log.fatal("Launching test %d failed", i)
                if isinstance(exception, LbAPJobFailed):
                    # Known failures
                    log.fatal("with error %r", exception)
                    failures.append(exception)
                else:
                    # Unknown failures
                    raise exception

            # Re-raise the first known failure to mark the job as failed
            if failures:
                raise exception

    except LbAPJobFailed as e:
        log.critical(str(e))
        gitlab_job.set_failed(gitlab_failure_reasons.ScriptFailure())

    except Exception:
        log.critical("Unknown system failure, see worker logs for more information")
        gitlab_job.set_failed(gitlab_failure_reasons.RunnerSystemFailure())
        handler.callback = None
        log.exception("Unknown system failure")
        # TODO: This should kill any jobs it has already created
        return None

    else:
        if step_ids_to_submit or already_running_step_ids:
            return (
                list(set(step_ids_to_submit) | set(already_running_step_ids)),
                list(chain(*step_ids_to_submit.values()))
                + list(chain(*already_running_step_ids.values())),
            )
        else:
            log.always("No changes found, marking as success")
            gitlab_job.set_success()
    finally:
        handler.callback = None


def ensure_pipeline_registered(job):
    with contexted() as session:
        pipeline = session.query(Pipeline).get(job.pipeline_id)
        if pipeline is None:
            log.info("Creating new pipeline for ID %s", job.pipeline_id)
            pipeline = Pipeline.create(job)
            session.add(pipeline)
        else:
            log.info(
                "Found pre-existing pipeline for ID %s, this is a retry",
                job.pipeline_id,
            )
        log.always(
            "Results will be available at %s/pipelines/?id=%s",
            settings.domain,
            job.pipeline_id,
        )


def create_productions(gitlab_job):
    production_ids = []
    with contexted() as session:
        pipeline = session.query(Pipeline).get(gitlab_job.pipeline_id)

        with TemporaryDirectory() as tmp_dir:
            repo = Repo.clone_from(gitlab_job.repo_url, tmp_dir)
            try:
                repo.git.checkout(gitlab_job.commit_sha)
            except GitCommandError:
                raise LbAPJobFailed(
                    f"Failed to checkout commit: {gitlab_job.commit_sha}. The most likely cause is "
                    "that you have force pushed meaning the previous commit "
                    "is no longer present on GitLab."
                )

            if gitlab_job._job_info["git_info"]["ref_type"] != "branch":
                raise LbAPJobFailed(
                    f"Running CI on {gitlab_job._job_info['git_info']['ref_type']} "
                    "references is not supported"
                )

            is_merge_request = gitlab_job._job_info["git_info"]["ref"] != "master"

            production_names = changed_productions(tmp_dir, is_merge_request)

            for production_name in production_names:
                productions = session.query(Production).filter_by(
                    pipeline_id=gitlab_job.pipeline_id, name=production_name
                )
                if productions.count() == 0:
                    log.info("Creating production %s", production_name)
                    try:
                        production = Production.create(
                            pipeline, tmp_dir, production_name
                        )
                        session.add(production)
                        session.commit()
                    except Exception:
                        session.rollback()
                        raise LbAPJobFailed(
                            f"Failed to create production for {production_name}"
                        )
                elif productions.count() == 1:
                    production = productions.all()[0]
                    log.info(
                        "Production %s already exists with internal ID %s",
                        production_name,
                        production.id,
                    )
                else:
                    raise NotImplementedError("Something went very wrong")
                production_ids += [production.id]
    return production_ids


def monitor_steps(gitlab_job, running_step_ids):
    # log.verbose('Running monitor_steps(%r, %r)', gitlab_job.dumps(), step_id)
    def logger_callback(record, msg):
        if record.levelno >= logging.INFO:
            gitlab_job.log += msg + "\n"

    handler.callback = logger_callback

    try:
        futures = []
        with ThreadPoolExecutor(max_workers=10) as e:
            for step_id in running_step_ids:
                futures.append(e.submit(monitor_step, gitlab_job, step_id))

        # Exceptions in the threadpool are lost, so manually raise them here if needed
        failures = []
        for i, future in enumerate(futures):
            exception = future.result()
            if exception is None:
                # Monitoring was successful
                continue

            if isinstance(exception, LbAPJobFailed):
                # Known failures
                log.critical(repr(exception))
                failures.append(exception)
            else:
                log.critical(f"Unknown failure for {i}, error is {exception!r}")
                # Unknown failures
                raise exception

        # Re-raise the first known failure to mark the job as failed
        if failures:
            gitlab_job.set_failed(gitlab_failure_reasons.ScriptFailure())

    except Exception as e:
        # Reset the callback first in case the GitLab CI job was cancelled
        handler.callback = None

        # Update the test state one last time if we can
        try:
            with contexted() as session:
                step = session.query(Step).get(step_id)
                for job in step.jobs:
                    update_test_status(job.tests[-1])
        except Exception:
            log.exception("Failed to update the test state while handling exception")
            pass

        log.critical("Unknown system failure, see worker logs for more information")
        gitlab_job.set_failed(gitlab_failure_reasons.RunnerSystemFailure())
        handler.callback = None
        log.exception("Unknown system failure")
        raise e

    finally:
        handler.callback = None


def monitor_step(gitlab_job, step_id):
    try:
        with contexted() as session:
            step = session.query(Step).get(step_id)
            if step.running:
                log.verbose("Running monitor_step for %r %s", step, step.execution_id)
            else:
                return
            passed = BaseTest.test_passed(step)

        if passed is None:  # Still running
            with contexted() as session:
                step = session.query(Step).get(step_id)
                log.verbose(
                    "Test is still running for %s %r", step.production.name, step
                )

        elif passed:
            with contexted() as session:
                step = session.query(Step).get(step_id)
                log.info("Test finished for %s %s", step.production.name, step)
                BaseTest.finalise_test(step)

        else:
            with contexted() as session:
                step = session.query(Step).get(step_id)
                log.warning(
                    "System failure for %s %s DIRAC ID %s",
                    step.production.name,
                    step,
                    step.execution_id,
                )
                BaseTest.restart_test(step)

        # Update the test state
        with contexted() as session:
            step = session.query(Step).get(step_id)
            for job in step.jobs:
                test = job.tests[-1]
                if not test.finished:
                    update_test_status(test)

    except Exception as e:
        return e


def monitor_gitlab_job(gitlab_job, production_ids):
    def logger_callback(record, msg):
        if record.levelno >= logging.INFO:
            gitlab_job.log += msg + "\n"

    handler.callback = logger_callback

    try:
        n_running = 0
        failed_steps = []
        for production_id in production_ids:
            with contexted() as session:
                production = session.query(Production).get(production_id)
                for step in production.steps:
                    if step.running:
                        n_running += 1
                    elif not step.completed_successfully:
                        failed_steps += [str(step)]
        log.verbose(
            "Results from monitoring %s: n_running=%s failed_steps=%s",
            gitlab_job.pipeline_id,
            n_running,
            failed_steps,
        )
        if n_running == 0:
            if failed_steps:
                log.fatal(
                    "One or more tests failed:\n    - %s", "\n    - ".join(failed_steps)
                )
            else:
                log.always("All tests completed successfully")

            log.always(
                "Results will be available at %s/pipelines/?id=%s",
                settings.domain,
                gitlab_job.pipeline_id,
            )
        else:
            log.always(
                "%s jobs still running at %s", n_running, datetime.now().isoformat()
            )
        return n_running == 0, failed_steps

    except LbAPJobFailed as e:
        log.critical(str(e))
        gitlab_job.set_failed(gitlab_failure_reasons.ScriptFailure())

    except Exception:
        log.critical("Unknown system failure, see worker logs for more information")
        gitlab_job.set_failed(gitlab_failure_reasons.RunnerSystemFailure())
        handler.callback = None
        log.exception("Unknown system failure")
        raise

    finally:
        handler.callback = None


def update_test_status(test):
    log.verbose("Updating test status for %r", test)
    xml_summary_fn, xml_bk_fn, log_fn, output_fns = validators.match_output_filenames(
        list(test.s3_filenames),
        test.job.output,
        test.job.application_name,
        test.job.step_index + 1,
    )
    prmon_summary_fn = "prmon.json"  # hardcode for now

    if not test.step.running:
        test.status = TestStatusEnum.FAILED
    if test.status == TestStatusEnum.UNKNOWN and BaseTest.test_started(test.step):
        test.status = TestStatusEnum.RUNNING

    job_finished = xml_summary_fn is not None
    if test.job.checks:
        job_finished = "checks.log" in test.s3_filenames and job_finished

    # FIXME: prmon.json is only uploaded for the first job in a step
    parent_job = test.job
    while parent_job.parent:
        parent_job = parent_job.parent
    if prmon_summary_fn in parent_job.tests[-1].s3_filenames:
        try:
            prmon_obj = settings.s3.client.get_object(
                Bucket=parent_job.tests[-1].s3_bucket,
                Key=parent_job.tests[-1].s3_filenames[prmon_summary_fn],
            )
            prmon_summary = json.load(prmon_obj["Body"])
            test.memory_max_rss = prmon_summary["Max"]["rss"]
            test.memory_max_pss = prmon_summary["Max"]["pss"]
            test.memory_max_swap = prmon_summary["Max"]["swap"]
        except Exception:
            log.exception(
                "Caught exception while aggregating job memory usage summary (prmon) statistics."
            )

    if job_finished:
        obj = settings.s3.client.get_object(
            Bucket=test.s3_bucket, Key=test.s3_filenames[xml_summary_fn]
        )
        if validators.status_from_xml_summary(obj["Body"]):
            # TODO: Properly validate output_fns
            errored = False
            for filetype, filename in output_fns.items():
                if filename is None:
                    log.error(
                        "Failed to find output file for %s in %s %s",
                        filetype,
                        test.production.name,
                        test.job.name,
                    )
                    errored = True

            if not test.step.running and xml_bk_fn is None:
                log.error("%s has finished but the xml_bk_fn is still None", test.step)
                errored = True

            if errored:
                test.status = TestStatusEnum.FAILED
                _log = log.error
            elif xml_bk_fn is None:
                # Always wait until the bookeeping XML is available
                test.status = TestStatusEnum.RUNNING
                return
            elif test.job.checks:
                if "checks.json" in test.s3_filenames:
                    obj = settings.s3.client.get_object(
                        Bucket=test.s3_bucket, Key=test.s3_filenames["checks.json"]
                    )
                    checks_dict = json.load(obj["Body"])
                    for check, results in checks_dict[test.job.name].items():
                        if not results["passed"]:
                            log.error(
                                f"{check} failed for {test.job.name} check the webpage job summary for further information."
                            )
                            test.status = TestStatusEnum.FAILED
                            _log = log.error
                            break
                    else:
                        test.status = TestStatusEnum.SUCCESS
                        _log = log.info
                else:
                    log.error(
                        f"Checks were requested but checks.json was not found in the output for {test.job.name}. See the checks.log for further information."
                    )
                    test.status = TestStatusEnum.FAILED
                    _log = log.error

            else:
                test.status = TestStatusEnum.SUCCESS
                _log = log.info
        elif test.step.running:
            # The summary reports the job as having failed while it is still running
            test.status = TestStatusEnum.RUNNING
            return
        else:
            test.status = TestStatusEnum.FAILED
            _log = log.error
        _log(
            "Status changed to %s for %s %s",
            test.status.name,
            test.production.name,
            test.job.name,
        )

    if log_fn is not None:
        counts = validators.count_log_messages(
            settings.s3.client.get_object(
                Bucket=test.s3_bucket, Key=test.s3_filenames[log_fn]
            )["Body"]
            .read()
            .decode()
        )
        test.n_log_warning, test.n_log_error, test.n_log_fatal = counts

    if xml_bk_fn is not None:
        obj = settings.s3.client.get_object(
            Bucket=test.s3_bucket, Key=test.s3_filenames[xml_bk_fn]
        )

        def expected_input_files(lfn):
            from LbAPCommon.validators.bookkeeping_xml import InputFile

            if test.job.parent is None:
                return InputFile(
                    path=lfn,
                    size=BookkeepingLFN(lfn).size,
                    dataset_size=test.job.input.size,
                )
            else:
                for output_file in test.job.parent.tests[-1].output_files:
                    if basename(output_file.path) == basename(lfn):
                        return InputFile(
                            path=output_file.path,
                            size=output_file.size,
                            dataset_size=output_file.dataset_size,
                        )
                else:
                    raise NotImplementedError(
                        lfn, repr(test.job.parent.tests[-1].output_files)
                    )

        try:
            (
                test.events_processed,
                test.events_requested,
                test.run_time,
                test.cpu_norm,
                input_files,
                output_files,
            ) = validators.parse_bookkeeping_xml(obj["Body"], expected_input_files)
        except Exception:
            log.exception("Failed to parse bookkeeping XML")
            log.error(
                "Error parsing bookkeeping XML for %s %s, marking as failed",
                test.production.name,
                test.job.name,
            )
            test.status = TestStatusEnum.FAILED
        else:
            test.input_files = [
                InputFile(path=f.path, size=f.size, dataset_size=f.dataset_size)
                for f in input_files
            ]
            test.output_files = [
                OutputFile(path=f.path, size=f.size, dataset_size=f.dataset_size)
                for f in output_files
            ]


def kill_running(production_ids):
    try:
        for production_id in production_ids:
            with contexted() as session:
                production = session.query(Production).get(production_id)
                for step in production.steps:
                    step.running = False
    except Exception:
        log.exception("Unknown system failure")


def launch_test(TestClass, step_id, step_ids, i):
    with contexted() as session:
        step = session.query(Step).get(step_id)
        log.info(
            "Submitting test for %s %s (%s/%s)",
            step.production.name,
            step,
            i,
            len(step_ids),
        )
        try:
            step.launch_test(TestClass)
        except Exception as e:
            return e


def tag_data_pkg(gitlab_job):
    def logger_callback(record, msg):
        if record.levelno >= logging.INFO:
            gitlab_job.log += msg + "\n"

    handler.callback = logger_callback

    try:
        log.always("Deploying option files...")
        tag_name = prepare_dynamic_options(gitlab_job.pipeline_id)
        log.always("Waiting for data package version %s to be deployed", tag_name)
        return tag_name
    except Exception as e:
        log.critical("Unknown system failure, see worker logs for more information")
        handler.callback = None
        log.exception("Unknown system failure")
        raise e
    finally:
        handler.callback = None


def do_submission(gitlab_job, tag_name, dry_run=False):
    def logger_callback(record, msg):
        if record.levelno >= logging.INFO:
            gitlab_job.log += msg + "\n"

    handler.callback = logger_callback
    try:
        if dry_run:
            log.always("Dry-run submission of productions to LHCbDIRAC")
            submit_productions(
                gitlab_job.pipeline_id,
                tag_name,
                dry_run=True,
                create_jira=False,
                clean_up=False,
            )
        else:
            path_exists = is_data_pkg_deployed(tag_name)
            if not path_exists:
                return False
            log.always("Package deployed, submitting productions to LHCbDIRAC")
            submit_productions(gitlab_job.pipeline_id, tag_name, dry_run=dry_run)
        return True

    except Exception as e:
        log.critical("Unknown system failure, see worker logs for more information")
        handler.callback = None
        log.exception("Unknown system failure")
        raise e
    finally:
        handler.callback = None


def is_data_pkg_deployed(tag_name):
    if isdir(join("/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AnalysisProductions", tag_name)):
        return True
    else:
        return False
