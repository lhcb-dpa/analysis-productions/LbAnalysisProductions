###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = ("prepare_dynamic_options",)

import importlib.resources
import os
import sys
from os.path import dirname, isdir, join
from pathlib import Path
from tempfile import TemporaryDirectory

import git

import LbAnalysisProductions
from LbAnalysisProductions.logging import log
from LbAnalysisProductions.settings import settings


def prepare_dynamic_options(pipeline_id):
    return _tag_dynamic_options(
        pipeline_id, *_prepare_dynamic_options(pipeline_id=pipeline_id)
    )


class LbAPGitRepo:
    def __init__(self, ssh_url, git_rev):
        self._tmpdir = TemporaryDirectory()  # pylint: disable=consider-using-with
        # OpenSSH reads the passwd file to determine if the permissions on SSH keys
        # are appropriate. As OpenShift runs the container as a random UID we have
        # to use nss_wrapper to fake the user and group information
        self._write_passwd_file()
        self._write_group_file()
        log.verbose("Cloning sources into %s from %s", self.path, ssh_url)
        self.repo = git.Repo.clone_from(ssh_url, self.path, env=self.git_env)
        self.repo.config_writer().set_value(
            "user", "name", settings.tagging.actor_name
        ).release()
        self.repo.config_writer().set_value(
            "user", "email", settings.tagging.actor_email
        ).release()
        log.verbose("Checking out %s in %s", git_rev, self.path)
        self.repo.git.checkout(git_rev)

    def _write_passwd_file(self):
        with open(self.passwd_fn, "w") as fp:
            fp.write(
                f"lbapuser:x:{os.getuid()}:{os.getgid()}:Lb AP:/tmp/home:/bin/false\n"
                "root:x:0:0:root:/root:/bin/bash"
            )

    def _write_group_file(self):
        with open(self.group_fn, "w") as fp:
            fp.write(f"lbapuser:x:{os.getgid()}:\nroot:x:0:")

    def __enter__(self):
        self._tmpdir.__enter__()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._tmpdir.__exit__(exc_type, exc_value, traceback)

    @property
    def path(self):
        return join(self._tmpdir.name, "repo")

    @property
    def passwd_fn(self):
        return join(self._tmpdir.name, "passwd")

    @property
    def group_fn(self):
        return join(self._tmpdir.name, "group")

    @property
    def git_env(self):
        env = {
            "NSS_WRAPPER_PASSWD": self.passwd_fn,
            "NSS_WRAPPER_GROUP": self.group_fn,
        }
        if settings.ssh_key:
            env["GIT_SSH_COMMAND"] = (
                f"ssh -i {settings.ssh_key.path} -o StrictHostKeyChecking=no"
            )
        libnss_wrapper = (
            Path(sys.executable).parent.parent / "lib" / "libnss_wrapper.so"
        )
        if libnss_wrapper.exists():
            env["LD_PRELOAD"] = str(libnss_wrapper)
        return env


def _prepare_dynamic_options(pipeline_id):
    with LbAnalysisProductions.utils.contexted() as session:
        pipeline = session.query(LbAnalysisProductions.models.Pipeline).get(pipeline_id)

        url = pipeline.job.project_url
        if not url.startswith("https://gitlab.cern.ch/"):
            raise ValueError(url)
        ssh_url = url.replace(
            "https://gitlab.cern.ch/", "ssh://git@gitlab.cern.ch:7999/", 1
        )
        git_rev = pipeline.job.commit_sha

        files = {}
        for production in pipeline.productions:
            for job in production.jobs:
                if job.automatically_configure:
                    fn = join("dynamic", job.dynamic_options_path)
                    files[fn] = job.autoconf_options

    return ssh_url, git_rev, files


def _tag_dynamic_options(pipeline_id, ssh_url, git_rev, files):
    with LbAPGitRepo(ssh_url, git_rev) as tmp:
        tag_name = (
            f"v{settings.tagging.major}" f"r{settings.tagging.minor}" f"p{pipeline_id}"
        )
        if tag_name in tmp.repo.tags:
            log.info("Tag %s already exists, skipping...", tag_name)
            return tag_name

        # Write the dynamic options to disk
        for path, data in files.items():
            fn = join(tmp.path, path)
            if not isdir(dirname(fn)):
                os.makedirs(dirname(fn))
            log.verbose("Writing dynamic options to %s", path)
            with open(fn, "w") as fp:
                fp.write(data)
        # Write the merger options to disk
        path = "ap_merger.py"
        log.verbose("Writing merger options to %s", path)
        merger_options = importlib.resources.read_text(
            "LbAnalysisProductions.data", "ap_merger.py"
        )
        with open(join(tmp.path, path), "w") as fp:
            fp.write(merger_options)
        # Prevent the .gitignore from stopping the dynamic options from being committed
        os.remove(join(tmp.path, ".gitignore"))
        # Commit the dynamic options
        for entry in tmp.repo.index.add(tmp.repo.untracked_files):
            log.info("Added %s to staging area", entry.path)
        actor = settings.tagging.actor
        commit = tmp.repo.index.commit(
            "Add dynamic options", author=actor, committer=actor
        )
        log.info("Commited options in %s", commit.hexsha)
        # Create the corrosponding tag and push
        tmp.repo.create_tag(
            tag_name,
            ref=commit,
            message=f"Release for pipeline ID {pipeline_id}",
        )
        log.info("Created tag %s from %s", tag_name, commit.hexsha)
        tmp.repo.remotes.origin.push(tag_name)
        log.info("Pushed %s to remote", tag_name)
        return tag_name
