###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import importlib.resources
import ipaddress
import os
import subprocess
import sys
import tempfile
import time
from datetime import datetime, timedelta
from pathlib import Path
from telnetlib import Telnet
from typing import Literal, Optional

import boto3
import dateparser
import git
from botocore.client import ClientError as BotoClientError
from gitlab import Gitlab
from pydantic import (
    AnyUrl,
    BaseModel,
    HttpUrl,
    IPvAnyNetwork,
    SecretBytes,
    SecretStr,
)
from pydantic_settings import BaseSettings

settings_initialised = False

HOST_INACCESSIBLE_SSHUTTTLE = (
    "This is likely due to you being located outside of the CERN firewall and "
    "can likely be fixed by tunneling your traffic over SSH with:\n"
    "sshuttle -vr lxtunnel.cern.ch -x 172.17.0.2/16 137.138.0.0/16 "
    "128.141.0.0/16 128.142.0.0/16 188.184.0.0/15 "
    "--pidfile /tmp/sshuttle.pid --python=python"
)
HOST_INACCESSIBLE_OPENSHIFT = (
    "This is likely because you are running outside of the CERN OpenShift "
    "cluster and can be fixed by:\n"
    "  1. oc login https://openshift.cern.ch\n"
    "  2. oc project PROJECT_NAME\n"
    "  3. oc port-forward CONTAINER_NAME {port}:{port}\n"
    '  4. Adjust the configuration file to replace {host!r} with "localhost"'
)


GITLAB_FIRSTPROD_WELCOME_MESSAGE = """
:wave: Welcome{back} to Analysis Productions!

This is the first production under the name `{production_name}`.

---

{extra_guidance}
"""

GITLAB_PROD_WITH_HISTORY_WELCOME = """
:wave: Welcome{back} to Analysis Productions!

There are {n_versions} existing deployment(s) under the name `{production_name}`:

{previous_release_table}

---

{extra_guidance}
"""


def check_accessible(host, port, message):
    """Use telnet to check if the host is accessible"""
    try:
        with Telnet(host, port, timeout=2):
            pass
    except Exception as e:
        raise RuntimeError(
            f"Error connecting to {host} with: {e!r}\n"
            + message.format(host=host, port=port)
        ) from None


class MySQLDsn(AnyUrl):
    allowed_schemes = {"mysql"}
    user_required = True


class RabbitMQDsn(AnyUrl):
    allowed_schemes = {"amqp"}
    user_required = True

    @property
    def vhost(self):
        return self.path[self.path.startswith("/") :]

class OpensearchDsn(AnyUrl):
    allowed_schemes = {"http", "https", "https+noverify"}
    user_required = True

class S3Auth(BaseModel):
    bucket: str
    access_key_id: str
    secret_access_key: str
    endpoint: HttpUrl = "https://s3.cern.ch"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ensure_bucket_exists()

    def ensure_bucket_exists(self):
        try:
            self.client.head_bucket(Bucket=self.bucket)
        except BotoClientError as e:
            print("Failed to access S3 bucket, trying to create...")
            print(e)
            self.client.create_bucket(Bucket=self.bucket)

    @property
    def client(self):
        return boto3.client(
            "s3",
            endpoint_url=self.endpoint,
            aws_access_key_id=self.access_key_id,
            aws_secret_access_key=self.secret_access_key,
        )


class Tagging(BaseModel):
    major: int = 0
    minor: int = 0
    actor_name: str = "LHCb Analysis Productions Bot"
    actor_email: str = "lhcb-dpa-wp2-admins@cern.ch"

    @property
    def actor(self):
        return git.Actor(self.actor_name, self.actor_email)


class CERNCredentials(SecretStr):
    user: str
    password: str

    def __init__(self, user: str, password: str):
        super().__init__(f"{user}:{password}")
        self.user, self.password = user, password
        self.krb5_config_path = importlib.resources.path(  # pylint: disable=no-member
            "LbAnalysisProductions.data", "krb5.conf"
        ).__enter__()
        os.environ["KRB5_CONFIG"] = str(self.krb5_config_path)

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("string required")
        user, password = v.split(":", 1)
        return cls(user, password)

    def krb_init(self, *, retries=3, min_time_left=timedelta(hours=2), timeout=10):
        """Ensure kerberos credentials are available

        Generated new kerberos credentials if there is less than ``min_time_left``
        remaining on any current credentials.

        :param retries: Path to a file to analyse
        :param timeout: Maximum number of seconds calling ``kinit`` can take
        :param min_time_left: Time that must be remaining on the credentials
        :raises RuntimeError: If all ``retries`` failed to generate credentials
        """
        if self.krb_valid(min_time_left=min_time_left):
            return
        print("Generating kerberos credentials")
        try:
            proc = subprocess.run(
                ["kinit", f"{self.user}@CERN.CH"],
                input=self.password,
                text=True,
                capture_output=True,
                timeout=timeout,
            )
        except subprocess.TimeoutExpired:
            error = f"Call to kinit timed out after {timeout} seconds"
        else:
            if proc.returncode == 0:
                return
            error = f"Call to kinit failed: {proc!r}"
        if retries > 0:
            print("Sleeping 60 seconds before retrying kinit")
            time.sleep(60)
            return self.krb_init(retries=retries - 1, min_time_left=min_time_left)
        raise RuntimeError(error)

    def krb_valid(self, *, min_time_left=timedelta(hours=2)):
        """Check if valid kerberos credentials are currently available

        :param min_time_left: Time that must be remaining on the credentials
        """
        proc = subprocess.run(["klist"], text=True, capture_output=True)
        for line in proc.stdout.split("\n"):
            if not line.strip().endswith("krbtgt/CERN.CH@CERN.CH"):
                continue
            start, expiry, principal = line.split("  ")
            if datetime.now() + min_time_left < dateparser.parse(expiry):
                return True


class CredentialsFile(SecretBytes):
    """Pydantic model for storing X509 and SSH keys"""

    data: bytes
    file: tempfile.NamedTemporaryFile

    def __init__(self, data: bytes):
        super().__init__(data)
        self.data = data
        self.file = tempfile.NamedTemporaryFile()  # pylint: disable=consider-using-with
        self.file.write(self.data)
        self.file.flush()

    @property
    def path(self) -> str:
        return self.file.name


class GitLabAuth(SecretStr):
    token: str
    instance: HttpUrl = "https://gitlab.cern.ch"

    def __init__(self, token: str | None = None):
        self.token = token
        if token is None:
            token = "unauthenticated"
            self.token = None
        super().__init__(token)

    @property
    def api(self):
        kwargs = {}
        if self.token:
            kwargs["private_token"] = self.token
        glab = Gitlab(self.instance, **kwargs)
        if self.token:
            try:
                glab.auth()
            except Exception as e:
                raise ValueError(
                    f"Could not authenticate to Gitlab instance {self.instance}: {e}"
                )
        return glab


class Settings(BaseSettings):
    domain: str = "https://lhcb-analysis-productions.web.cern.ch"
    alert_email: str = "lhcb-dpa-wp2-managers@cern.ch"
    tagging: Tagging = Tagging()

    # DIRAC (required in celery worker)
    dirac_host_cert: Optional[CredentialsFile] = None
    dirac_host_key: Optional[CredentialsFile] = None
    environment_base: str | None = None
    lb_dirac: list[str] = ["lb-dirac"]
    ci_job_backend: str = "dirac"
    ci_job_local_dir: Path = Path(tempfile.gettempdir())

    # Required in the celery beat job
    gitlab_runner_data: str | None = None
    gitlab: Optional[GitLabAuth] = GitLabAuth()
    gitlab_data_repository: str = "lhcb-datapkg/AnalysisProductions"

    # Used to validate webhook requests to LbAPI
    gitlab_webhook_secret: str | None = None

    # Used to send traces to Sentry
    sentry_dsn: str | None = None

    # Required in the celery worker
    ssh_key: Optional[CredentialsFile] = None
    cern_credentials: Optional[CERNCredentials] = None

    # Required in all services
    celery_broker: Optional[RabbitMQDsn] = None
    db_url: Optional[MySQLDsn] = None
    db_use_null_pool: bool = False
    logging_broker: Optional[RabbitMQDsn] = None
    lbapi_broker: Optional[RabbitMQDsn] = None
    lbapi_backend: Optional[MySQLDsn] = None
    lbapi_url: str = "https://lbap.app.cern.ch"
    s3: Optional[S3Auth] = None

    # LbAPI settings
    token_salt: Optional[bytes] = None
    token_algorithm: str = "sha256"
    token_rounds: int = 2_000
    eos_token_user: str | None = None
    eos_token_password: str | None = None

    lbodreq_user: str | None = None
    lbodreq_password: str | None = None

    cern_sso_well_known: str = (
        "https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration"
    )
    cern_sso_client_id: str = "lhcb-analysis-productions"
    cern_gitlab_well_known: str = (
        "https://gitlab.cern.ch/.well-known/openid-configuration"
    )
    cern_networks: list[IPvAnyNetwork] = [
        ipaddress.ip_network("128.141.0.0/16"),
        ipaddress.ip_network("128.142.0.0/16"),
        ipaddress.ip_network("137.138.0.0/16"),
        ipaddress.ip_network("185.249.56.0/22"),
        ipaddress.ip_network("188.184.0.0/15"),
        ipaddress.ip_network("194.12.128.0/18"),
        ipaddress.ip_network("2001:1458::/32"),
        ipaddress.ip_network("FD01:1458::/32"),
        ipaddress.ip_network("FD01:1459::/32"),
    ]
    opensearch: Optional[OpensearchDsn] = None
    app_logs_index: str = "app-logs"
    app_logs_index_size: int = 10_000

    # Various messages
    HOST_INACCESSIBLE_SSHUTTTLE: Literal[HOST_INACCESSIBLE_SSHUTTTLE] = HOST_INACCESSIBLE_SSHUTTTLE
    HOST_INACCESSIBLE_OPENSHIFT: Literal[HOST_INACCESSIBLE_OPENSHIFT] = HOST_INACCESSIBLE_OPENSHIFT
    GITLAB_FIRSTPROD_WELCOME_MESSAGE: Literal[GITLAB_FIRSTPROD_WELCOME_MESSAGE] = GITLAB_FIRSTPROD_WELCOME_MESSAGE
    GITLAB_PROD_WITH_HISTORY_WELCOME: Literal[GITLAB_PROD_WITH_HISTORY_WELCOME] = GITLAB_PROD_WITH_HISTORY_WELCOME

    @property
    def use_certificates(self):
        return bool(self.dirac_host_cert and self.dirac_host_key)

    class Config:
        env_prefix = "LBAP_"
        json_encoders = {
            SecretStr: lambda v: v.get_secret_value() if v else None,
            SecretBytes: lambda v: v.get_secret_value() if v else None,
        }


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        global settings_initialised
        if not settings_initialised:
            # Check we can connect to the various firewalled services
            if self.db_url:
                check_accessible(
                    self.db_url.host, self.db_url.port, HOST_INACCESSIBLE_SSHUTTTLE
                )
            if self.celery_broker:
                check_accessible(
                    self.celery_broker.host,
                    self.celery_broker.port,
                    HOST_INACCESSIBLE_OPENSHIFT,
                )
            if self.logging_broker:
                check_accessible(
                    self.logging_broker.host,
                    self.logging_broker.port,
                    HOST_INACCESSIBLE_OPENSHIFT,
                )

            if self.dirac_host_cert and self.dirac_host_key:
                print("Found host certificate for DIRAC, configuring...")
                argv, sys.argv = sys.argv, []
                # Setup DIRAC for using the host certificate
                from DIRAC.ConfigurationSystem.Client.LocalConfiguration import (
                    LocalConfiguration,
                )

                localCfg = LocalConfiguration()
                localCfg.addDefaultEntry("/DIRAC/Security/UseServerCertificate", "yes")
                localCfg.addDefaultEntry(
                    "/DIRAC/Security/CertFile", self.dirac_host_cert.path
                )
                localCfg.addDefaultEntry(
                    "/DIRAC/Security/KeyFile", self.dirac_host_key.path
                )
                result = localCfg.loadUserData()
                if not result["OK"]:
                    raise RuntimeError(f"Failed to configure DIRAC {result!r}")
                # Restore command line arguments
                sys.argv = argv

            settings_initialised = True


settings = Settings(_env_file=os.environ.get("LBAP_DOTENV"))
