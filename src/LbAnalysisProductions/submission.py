###############################################################################
# (c) Copyright 2020-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
import smtplib
import subprocess
import tempfile
from datetime import timedelta
from difflib import SequenceMatcher
from email.message import EmailMessage
from itertools import chain, tee
from pathlib import Path

import git
import yaml
from LbAPCommon.parsing import create_proc_pass_map

from LbAnalysisProductions.data_pkg_tagging import LbAPGitRepo
from LbAnalysisProductions.logging import log
from LbAnalysisProductions.models import Pipeline
from LbAnalysisProductions.models.job import _format_bkquery_runs
from LbAnalysisProductions.settings import settings
from LbAnalysisProductions.utils import DownloadedProxyFile, TestStatusEnum, contexted


def pairwise(iterable):
    # pairwise('ABCDEFG') --> AB BC CD DE EF FG
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def submit_gitlab_task(
    working_groups,
    ap_title,
    version,
    pipeline_id,
    job_statistics,
    request_metadata,
    inform_emails=None,
    user_comment="",
):
    # grab merge request IID(s)
    log.debug("Get merge request links...")
    project = settings.gitlab.api.projects.get(settings.gitlab_data_repository)
    pipeline = project.pipelines.get(pipeline_id)
    merge_commit = project.commits.get(pipeline.sha)
    merge_requests = merge_commit.merge_requests()

    MR_links = "\n".join([f"- !{mr['iid']}+s" for mr in merge_requests])

    gitlab_pipeline_url = (
        f"{settings.gitlab.instance}/{settings.gitlab_data_repository}/-/pipelines"
    )

    notify_users = set()
    for mr in merge_requests:
        notify_users.add(mr["author"]["username"])

        if mr["assignee"] is not None:
            notify_users.add(mr["assignee"]["username"])

    for email in inform_emails or []:
        # We cannot search by email on GitLab.
        for user in settings.gitlab.api.users.list(search=email, iterator=True) or []:
            if user.username == email:
                log.info(
                    f"Inform field {email!r} has a matching GitLab username {user.username!r}"
                )
                notify_users.add(user.username)

    task_summary = (
        f"[{', '.join(working_groups)}] - {ap_title} - AnalysisProductions {version}"
    )
    newline = "\n"

    if len(user_comment.strip()) > 0:
        user_comment = f"**Comments:** {user_comment}\n"

    jira_body = f"""
:information_source: A new version `{version}` of the Analysis Production `{ap_title}` for the {', '.join(working_groups)} WG is now available.

{user_comment}

cc {", ".join([f"@{username}" for username in notify_users])}

### Production Testing Details (Pipeline #{pipeline_id})

{MR_links}

- [GitLab Pipeline]({gitlab_pipeline_url}/{pipeline_id})
- [Pipeline Test Details]({settings.domain}/pipelines/?id={pipeline_id})

#### Summary table

| Step | Priority | Completion Thr. (%) | Production ID | Run time | CPU Norm. | Output Size (MB) |
| ---- | -------- | ------------------- | ------------- | -------- | --------- | ---------------- |
{newline.join([f"| {s[0]} | {s[1]} | {s[2]} | {s[3]} | {s[4]} | {s[5]}x | {s[6]} |" for s in job_statistics])}

<details><summary>Metadata</summary>

```json
{json.dumps(request_metadata)}
```

</details>

"""
    log.debug("Create a new GitLab issue")

    new_issue = project.issues.create(
        {
            "title": str(task_summary),
            "description": str(jira_body),
            "labels": list(working_groups) + ["Production"],
            "confidential": True,
        }
    )

    return new_issue.iid, new_issue.web_url


def gitlab_notify_submission(pipeline_id, version, task_key, task_url):
    """
    Notify involved parties on GitLab that the production based off pipeline_id
    was submitted.
    """

    project = settings.gitlab.api.projects.get(settings.gitlab_data_repository)

    pipeline = project.pipelines.get(pipeline_id)
    merge_commit = project.commits.get(pipeline.sha)
    merge_requests = merge_commit.merge_requests()

    message = f""":rocket: Version `{version}` of this Analysis Production has been submitted to LHCbDIRAC.

#{task_key}+s, has been created to track the production.
Progress can also be monitored on the Analysis Productions website.

"""

    for mr in merge_requests:
        if mr["state"] == "merged":
            mr_obj = project.mergerequests.get(mr["iid"])
            disc = mr_obj.discussions.create({"body": message})
            disc.resolved = True
            disc.save()


def send_email_alert(pipeline_id, subject, content, to="lhcb-dpa-wp2-managers@cern.ch"):
    try:
        msg = EmailMessage()
        msg.set_content(content)
        msg["Subject"] = f"AnalysisProductions Pipeline {pipeline_id} - {subject}"
        msg["From"] = "lhcb-analysis-productions@cern.ch"
        msg["To"] = [to]

        with smtplib.SMTP("cernmx.cern.ch", timeout=2) as smtp:
            smtp.send_message(msg)
    except Exception:
        log.exception("Failed to send alarm email!")


def clean_repository(pipeline):
    """Clean up the Analysis Productions submission repository.

    Creates a commit to the repository to remove the directory of the production that was just submitted to maintain a clean state in the repository.

    Args:
        pipeline: The pipeline query corresponding to the production submission to be cleaned from the repository.
    """
    url = pipeline.job.project_url
    if not url.startswith("https://gitlab.cern.ch/"):
        raise ValueError(url)
    ssh_url = url.replace(
        "https://gitlab.cern.ch/", "ssh://git@gitlab.cern.ch:7999/", 1
    )
    git_rev = pipeline.job.commit_sha

    with LbAPGitRepo(ssh_url, "master") as tmp:
        production_names = [
            production.name
            for production in pipeline.productions
            if (Path(tmp.path) / production.name).exists()
        ]
        if not production_names:
            log.info("No productions to remove, skipping cleaning...")
            return

        tmp.repo.git.checkout(git_rev, b="tmp/clean_repository")

        for production in production_names:
            tmp.repo.index.remove(production, working_tree=True, r=True)
        actor = settings.tagging.actor
        commit = tmp.repo.index.commit(
            f"[skip ci] Cleaned up productions {', '.join([production.name for production in pipeline.productions])} from {git_rev}",
            author=actor,
            committer=actor,
        )
        log.info("Cleaned %r from %s in %s", production_names, url, commit.hexsha)

        tmp.repo.git.checkout("master")
        try:
            tmp.repo.git.cherry_pick(commit)
        except git.GitCommandError:
            log.exception("Failed to cherry-pick %s", commit.hexsha)
            return

        push_info = tmp.repo.remotes.origin.push("master")[0]
        if push_info.flags & git.PushInfo.ERROR:
            log.error(
                "Could not merge and push to master with %r options files removed: %s",
                production_names,
                push_info.summary,
            )
            return
        log.info("Pushed to master with %r options files removed", production_names)


def submit_productions(
    pipeline_id, tag_name, *, dry_run=False, create_jira=True, clean_up=True
):
    all_request_ids = set()
    with contexted() as session:
        pipeline = session.query(Pipeline).get(pipeline_id)
        for production in pipeline.productions:
            to_submit = []
            job_statistics = []
            for step in production.steps:
                data = convert_to_dirac_dict(step, tag_name)
                to_submit.append(data)
                job_statistics.append(make_stats(step, data))

            with tempfile.TemporaryDirectory() as tmpdir:
                yaml_fn = Path(tmpdir) / "requests.yaml"
                yaml_fn.write_text(yaml.safe_dump(to_submit, sort_keys=False))
                log.debug("Submitting with:\n%s", yaml_fn.read_text())
                ids_fn = Path(tmpdir) / "ids.json"
                cmd = settings.lb_dirac + ["dirac-production-request-submit"]
                cmd += [str(yaml_fn), f"--output-json={ids_fn}", "--create-filetypes"]
                if not dry_run:
                    cmd += ["--submit"]

                with DownloadedProxyFile("cburr", "lhcb_user") as proxy_file:
                    subprocess.check_call(
                        cmd, env={**os.environ, "X509_USER_PROXY": proxy_file.name}
                    )

                request_ids = json.loads(ids_fn.read_text())

            for (_, request_id, _), stats in zip(request_ids, job_statistics):
                stats[3] = request_id
                log.always(
                    "Production ID %s took %s on a %sx machine and produced a %d MB file",
                    *stats[3:],
                )

                all_request_ids.add(request_id)

            wgs = {x["wg"] for x in to_submit}
            inform_emails = {pipeline.job.username} | set(
                chain(*(x["inform"] for x in to_submit))
            )
            if create_jira:
                try:
                    task_key, task_url = submit_gitlab_task(
                        wgs,
                        production.name,
                        tag_name,
                        pipeline_id,
                        job_statistics,
                        {"request_ids": list(all_request_ids), "type": "ANA_PROD"},
                        inform_emails=list(inform_emails),
                        user_comment=production.comment,
                    )

                    # if this was successful, we can also notify about the
                    # deployment on the MR:
                    gitlab_notify_submission(pipeline_id, tag_name, task_key, task_url)
                except Exception:
                    log.exception(
                        "GitLab Issue creation failed, or failed to post to GitLab."
                    )
                    send_email_alert(
                        pipeline_id,
                        subject="GitLab update failure",
                        content=(
                            f"There was a problem creating the JIRA task or updating the GitLab MR during pipeline #{pipeline_id}, while deploying "
                            f"{tag_name} ({production.name}) \n\n"
                            f"To understand the problem further, please check the logs: {settings.gitlab.instance}/{settings.gitlab_data_repository}/-/pipelines/{pipeline_id} \n\n"
                            "N.B. This is an automated email, and replies are not monitored."
                        ),
                    )
        if clean_up:
            clean_repository(pipeline)


def convert_to_dirac_dict(step, tag_name, *, with_merging=True):
    """Convert a AnalysisProductions step object to a LHCbDIRAC production"""
    from dirac_prod.classes import InputDataset

    wgs = {job.wg for job in step.jobs}
    if len(wgs) != 1:
        raise NotImplementedError("Found a step with multiple WGs: " + repr(step))
    wg = wgs.pop()

    proc_pass_map = create_proc_pass_map([j.name for j in step.jobs], tag_name)

    steps = []
    for i, job in enumerate(step.jobs):
        options = {}
        if isinstance(job.options, list):
            options["files"] = job.options
            options["format"] = "WGProd"
        elif isinstance(job.options, dict) and "files" in job.options:
            if "command" in job.options:
                options["command"] = job.options["command"]
            options["files"] = job.options["files"]
            options["format"] = "WGProd"
        elif isinstance(job.options, dict):
            options["entrypoint"] = job.options["entrypoint"]
            options["extra_options"] = job.options["extra_options"]
            if "extra_args" in job.options:
                options["extra_args"] = job.options["extra_args"]
        else:
            raise NotImplementedError(type(job.options), job.options)

        application = {
            "name": job.application_name,
            "version": job.application_version,
        }
        if "@" in job.application_version:
            app_version, binary_tag = job.application_version.split("@", 1)
            application["version"] = app_version
            application["binary_tag"] = binary_tag

        steps.append(
            {
                "name": f"AnaProd#{tag_name}#{job.name}",
                "processing_pass": proc_pass_map[job.name],
                "application": application,
                "options": options,
                "data_pkgs": [{"name": "AnalysisProductions", "version": tag_name}],
                "input": [{"type": job.input.filename, "visible": i == 0}],
                "output": [
                    {"type": ft, "visible": not with_merging} for ft in job.output
                ],
                "visible": True,
                "ready": True,
                "obsolete": True,
            }
        )

    merge_step = {
        "name": f"AnaProd#Merge#{step.production.name}",
        "processing_pass": "merged",
        "application": {"name": "LHCb", "version": "v55r1"},
        "options": {
            "entrypoint": "ap_merger:hadd",
            "extra_options": {},
            "extra_args": ["ZSTD:9"],
        },
        "data_pkgs": [{"name": "AnalysisProductions", "version": tag_name}],
        "input": [{"type": str(ft), "visible": False} for ft in job.output],
        "output": [{"type": str(ft), "visible": False} for ft in job.output],
        "visible": False,
        "ready": True,
        "obsolete": True,
    }
    if with_merging:
        steps.append(merge_step)

    if "bk_query" in step.jobs[0].input_data:
        input_dataset = InputDataset.from_bk_path(
            step.jobs[0].input_data["bk_query"],
            runs=_format_bkquery_runs(step.jobs[0].input_data),
        )
    elif "transform_ids" in step.jobs[0].input_data:
        input_dataset = InputDataset.from_transform_ids(
            step.jobs[0].input_data["transform_ids"],
            step.jobs[0].input_data["filetype"],
            runs=_format_bkquery_runs(step.jobs[0].input_data),
        )
    else:
        raise NotImplementedError(step.jobs[0].input_data)
    input_dataset.set_dq_flags(step.jobs[0].input_data.get("dq_flags", ["OK"]))
    smog2_state = step.jobs[0].input_data.get("smog2_state")
    if smog2_state:
        input_dataset.set_smog2_state(smog2_state)

    # Remove duplicated parts of the production name to avoid it being too long
    step_name = step.jobs[0].name
    for a, b in pairwise(step.jobs):
        match = SequenceMatcher(None, a.name, b.name).find_longest_match()
        step_name += "," + b.name.replace(a.name[match.a : match.a + match.size], "")

    data = {
        "type": "AnalysisProduction",
        "name": f"AnaProd#{step.production.name}#{step_name}",
        "priority": step.jobs[0].priority,
        "author": "cburr",
        "inform": list(chain(*[job.inform for job in step.jobs])),
        "wg": wg,
        "comment": step.production.comment,
        "input_dataset": {
            "conditions_dict": input_dataset.conditions_dict,
            "conditions_description": input_dataset.conditions_description,
            "event_type": input_dataset.event_type,
        },
        "steps": steps,
    }

    return data


def make_stats(step, data):
    if step.jobs[-1].tests[-1].status != TestStatusEnum.SUCCESS:
        raise NotImplementedError("Failed to find output statistics for " + repr(step))
    run_time = sum((j.tests[-1].run_time for j in step.jobs), timedelta(0))
    cpu_norm = step.jobs[0].tests[-1].cpu_norm
    output_size = sum(of.size for of in step.jobs[-1].tests[-1].output_files)
    return [
        step.name,
        data["priority"],
        step.jobs[0].completion_percentage,
        None,
        run_time,
        cpu_norm,
        round(output_size / 1e6),
    ]
