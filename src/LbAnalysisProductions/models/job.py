###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "Job",
    "BookkeepingInputDataset",
    "JobInputDataset",
    "ProductionIDInputDataset",
]

import re
import time
from abc import ABC, abstractmethod
from os.path import join

from LbAPCommon.config import allowed_priorities, known_working_groups
from sqlalchemy import (
    JSON,
    Boolean,
    Column,
    Enum,
    Float,
    ForeignKey,
    Integer,
    String,
    UnicodeText,
    UniqueConstraint,
)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref, deferred, relationship

from LbAnalysisProductions import job_configuration, templates
from LbAnalysisProductions.logging import log

from . import Base

RE_JOB_NAME = re.compile(r"^[a-z0-9][a-z0-9_\-]+$", re.IGNORECASE)
RE_OUTPUT_FILE_TYPE = re.compile(
    r"^([a-z][a-z0-9_]+\.)+(ROOT|HIST|.?DST)$", re.IGNORECASE
)
RE_OPTIONS_FN = re.compile(r"^\$?[a-z0-9/\.\-\+\=_]+$", re.IGNORECASE)
RE_INFORM = re.compile(r"^(?:[a-z]{3,}|[^@\s]+@[^@\s]+\.[^@\s]+)$")


def _format_bkquery_runs(input_data):
    # This became necessary because DIRAC cannot handle
    # lists of int, only list of str.

    runs = input_data.get("runs", None)
    if runs:
        return [str(r) for r in runs]

    return None


class Job(Base):
    @classmethod
    def create(
        cls,
        name,
        application,
        input,
        output,
        options,
        wg,
        inform=None,
        checks=None,
        **kwargs,
    ):
        if not RE_JOB_NAME.match(name):
            raise ValueError(f"Job name ({name}) must match {RE_JOB_NAME.pattern}")

        if not isinstance(application, str) or application.count("/") < 1:
            raise ValueError(
                "application must be a string of the form APP_NAME/VERSION"
            )
        application_name = "/".join(application.split("/")[:-1])
        application_version = application.split("/")[-1]

        if not (isinstance(input, dict) and all(isinstance(s, str) for s in input)):
            raise ValueError(f"Field 'input' ({input}) must have type dict[str]")

        if isinstance(output, str):
            output = [output]
        if not (isinstance(output, list) and all(isinstance(s, str) for s in output)):
            raise ValueError(f"Field 'output' must have type str or list[str] {output}")
        if len(output) < 1:
            raise ValueError(f"No output found for {name}")
        for output_type in output:
            if not RE_OUTPUT_FILE_TYPE.match(output_type):
                raise ValueError(
                    f'Each job output ({output}) must match "{RE_OUTPUT_FILE_TYPE.pattern}"'
                )

        if isinstance(options, str):
            options = [options]
        if isinstance(options, list) and all(isinstance(s, str) for s in options):
            if len(options) < 1:
                raise ValueError(f"No options found for {name}")
            for options_fn in options:
                if not RE_OPTIONS_FN.match(options_fn):
                    raise ValueError(
                        f'Each option filename ({output}) must match "{RE_OPTIONS_FN.pattern}"'
                    )
        elif isinstance(options, dict):
            # LbExec
            pass
        else:
            raise ValueError("Field 'options' must have type list[str] or dict")

        if wg not in known_working_groups:
            raise ValueError(
                f'Unrecognised WG "{wg}", known values are {known_working_groups}'
            )

        if inform is None:
            inform = []
        if isinstance(inform, str):
            inform = [inform]
        if not (isinstance(inform, list) and all(isinstance(s, str) for s in inform)):
            raise ValueError("Field 'inform' must have type str or list[str]")
        for s in inform:
            if not RE_INFORM.match(s):
                raise ValueError(
                    f'Each inform entry ({s}) must match "{RE_INFORM.pattern}"'
                )

        self = Job(
            name=name,
            application_name=application_name,
            application_version=application_version,
            input_data=input,
            output=output,
            options=options,
            wg=wg,
            inform=inform,
            checks=checks,
            **kwargs,
        )
        assert self.input
        return self

    __tablename__ = "jobs"
    __table_args__ = (
        UniqueConstraint("name", "production_id"),
        UniqueConstraint("step_id", "step_index"),
    )
    id = Column(Integer, primary_key=True)
    # Columns
    name = Column(String(256), nullable=False)
    application_name = Column(String(50), nullable=False)
    application_version = Column(String(50), nullable=False)
    input_data = Column(JSON, nullable=False)
    output = Column(JSON, nullable=False)
    options = Column(JSON, nullable=False)
    n_test_events = Column(Integer, nullable=False, default=-1)
    # TODO: Remove from DB?
    n_test_lfns = Column(Integer, nullable=False, default=1)
    autoconf_options = deferred(Column(UnicodeText))
    # optional
    inform = Column(JSON, nullable=False, default=[])
    checks = Column(JSON, nullable=True)
    turbo = Column(Boolean, nullable=True)
    root_in_tes = Column(String(50), nullable=True)
    wg = Column(String(50), nullable=False)
    automatically_configure = Column(Boolean, nullable=False, default=False)
    # automatically initialisable
    AUTO_CONF_COLUMNS = ["simulation", "luminosity", "data_type", "input_type"]
    simulation = Column(Boolean, nullable=True)
    luminosity = Column(Boolean, nullable=True)
    data_type = Column(String(50), nullable=True)
    input_type = Column(String(50), nullable=True)
    dddb_tag = Column(String(50), nullable=True)
    conddb_tag = Column(String(50), nullable=True)
    # user provided metadata
    priority = Column(Enum(*tuple(allowed_priorities)), nullable=False, default="1b")
    completion_percentage = Column(Float, nullable=False, default=100.0)
    tags = Column(JSON, nullable=True)
    # Relationships
    parent_job_id = Column(Integer, ForeignKey("jobs.id"))
    children = relationship(
        f"{__name__}.Job", backref=backref("parent", remote_side=[id]), lazy="select"
    )
    production_id = Column(Integer, ForeignKey("productions.id"), nullable=False)
    production = relationship("Production", back_populates="jobs", lazy="joined")
    step_id = Column(Integer, ForeignKey("steps.id"), nullable=False)
    step_index = Column(Integer, nullable=False)
    step = relationship("Step", back_populates="jobs", lazy="joined")
    tests = relationship(
        "Test",
        order_by="Test.id",
        back_populates="job",
        cascade="all, delete, delete-orphan",
        lazy="joined",
    )

    def __repr__(self):
        return f"Job({self.production}, {self.name})"

    @hybrid_property
    def pipeline(self):
        return self.production.pipeline

    @property
    def input(self):
        return InputDataset.create(self)

    def generate_configuration(self):
        if not self.automatically_configure:
            raise RuntimeError(
                "Can't generate automatic configuration when Job.automatically_configure == False"
            )
        if not isinstance(self.options, dict):
            raise NotImplementedError(
                "generate_configuration received dict, this should no longer happen!"
            )
        if "files" not in self.options:
            raise NotImplementedError(
                "generate_configuration is not yet supported for lbexec based jobs"
            )
        self.input.configure_func()
        if self.luminosity is None:
            self.luminosity = not self.simulation
        if any(getattr(self, c) is None for c in self.AUTO_CONF_COLUMNS):
            configuration = {c: getattr(self, c) for c in self.AUTO_CONF_COLUMNS}
            raise RuntimeError(
                f"Generated invalid automatic configuration {configuration} for {self.input_data}"
            )
        if self.simulation:
            assert self.dddb_tag is not None, self.dddb_tag
            assert self.conddb_tag is not None, self.conddb_tag

        self.autoconf_options = templates.get_template(
            "configure-application.py.j2"
        ).render(
            application_name=self.application_name,
            conddb_tag=self.conddb_tag,
            data_type=self.data_type,
            dddb_tag=self.dddb_tag,
            input_type=self.input_type,
            luminosity=self.luminosity,
            root_in_tes=self.root_in_tes,
            simulation=self.simulation,
            turbo=self.turbo,
        )
        dynamic_options_name = join(
            "$ANALYSIS_PRODUCTIONS_DYNAMIC", self.dynamic_options_path
        )
        if self.options.get("command") == ["python"]:
            self.options["files"].insert(1, dynamic_options_name)
        else:
            self.options["files"].insert(0, dynamic_options_name)

    @property
    def dynamic_options_path(self):
        assert self.automatically_configure
        return join(self.production.name, self.name + "_autoconf.py")

    @property
    def keep_output(self):
        return not self.children


class InputDataset(ABC):
    known_keys = set()
    n_test_lfns = 1

    @property
    @abstractmethod
    def primary_key(self):
        raise NotImplementedError()

    @classmethod
    def create(cls, job):
        primary_keys = []
        matched_classes = []
        for sub_class in InputDataset.__subclasses__():
            primary_keys.append(sub_class.primary_key)
            if sub_class.primary_key in job.input_data:
                matched_classes.append(sub_class)

        if len(matched_classes) == 1:
            return matched_classes[0](job)
        elif matched_classes:
            raise RuntimeError(
                f"Found multiple matching input types {matched_classes!r}"
            )
        else:
            raise ValueError(f"No input type given, choose one of {primary_keys!r}")

    def __init__(self, job):
        unrecognised_keys = set(job.input_data) - self.known_keys - {self.primary_key}
        if unrecognised_keys:
            raise ValueError(
                f"Unrecognised key(s) for {self.__class__.__name__}: {unrecognised_keys}"
            )
        self.job = job

    @abstractmethod
    def configure_func(self):
        raise NotImplementedError()

    @property
    @abstractmethod
    def filename(self):
        raise NotImplementedError()


class BookkeepingInputDataset(InputDataset):
    primary_key = "bk_query"
    known_keys = {
        "n_test_lfns",
        "dq_flags",
        "smog2_state",
        "runs",
        "keep_running",
        "input_plugin",
    }

    def configure_func(self):
        return job_configuration.from_bk_query_job(self.job)

    @property
    def filename(self):
        return str(self.bk_query.split("/")[-1])

    @property
    def bk_query(self):
        return self.job.input_data["bk_query"]

    @property
    def n_test_lfns(self):
        return self.job.input_data.get("n_test_lfns", 1)

    @property
    def lfns(self):
        from dirac_prod.classes import InputDataset

        start = time.time()
        ds = InputDataset.from_bk_path(
            self.bk_query, runs=_format_bkquery_runs(self.job.input_data)
        )
        ds.set_dq_flags(self.job.input_data.get("dq_flags", ["OK"]))
        smog2_state = self.job.input_data.get("smog2_state")
        if smog2_state:
            ds.set_smog2_state(smog2_state)
        lfns = ds.lfns
        log.verbose("Getting LFNs took %s for %s", time.time() - start, self.bk_query)
        return lfns

    @property
    def available_lfns(self):
        return [lfn for lfn in self.lfns if lfn.has_replica]

    @property
    def size(self):
        from dirac_prod.classes import InputDataset

        ds = InputDataset.from_bk_path(
            self.bk_query, runs=_format_bkquery_runs(self.job.input_data)
        )
        ds.set_dq_flags(self.job.input_data.get("dq_flags", ["OK"]))
        smog2_state = self.job.input_data.get("smog2_state")
        if smog2_state:
            ds.set_smog2_state(smog2_state)
        return ds.size


class JobInputDataset(InputDataset):
    primary_key = "job_name"
    known_keys = {"filetype"}

    def configure_func(self):
        return job_configuration.from_job_name(self.job)

    @property
    def filename(self):
        # TODO: At some point LbAPCommon should be made to always set filetype
        # This would allow the initial two if conditions to be removed entirely
        if len(self.job.parent.output) == 1:
            if self.filetype and not self.job.parent.output[0].lower().endswith(
                self.filetype.lower()
            ):
                raise RuntimeError(
                    f"Filetype mismatch! {self.filetype=} with {self.job.parent.output!r}"
                )
            return str(self.job.parent.output[0])
        if self.filetype is None:
            raise RuntimeError("Multiple output files but no filetype given")
        for fn in self.job.parent.output:
            if str(fn).lower().endswith(self.filetype.lower()):
                return str(fn)
        raise RuntimeError(
            f"Failed to find {self.filetype=} from {self.job.parent.output!r}"
        )

    @property
    def job_name(self):
        return self.job.input_data["job_name"]

    @property
    def filetype(self):
        return self.job.input_data.get("filetype")


class ProductionIDInputDataset(InputDataset):
    primary_key = "transform_ids"
    known_keys = {
        "filetype",
        "n_test_lfns",
        "dq_flags",
        "smog2_state",
        "runs",
        "keep_running",
        "input_plugin",
    }

    def configure_func(self):
        return job_configuration.from_transform_ids(self.job)

    @property
    def lfns(self):
        from dirac_prod.classes import InputDataset

        start = time.time()
        ds = InputDataset.from_transform_ids(
            transform_ids=self.transform_ids,
            file_type=self.filename,
            runs=_format_bkquery_runs(self.job.input_data),
        )
        ds.set_dq_flags(self.job.input_data.get("dq_flags", ["OK"]))
        smog2_state = self.job.input_data.get("smog2_state")
        if smog2_state:
            ds.set_smog2_state(smog2_state)
        lfns = ds.lfns
        log.verbose(
            "Getting LFNs took %s for %s", time.time() - start, self.transform_ids
        )
        return lfns

    @property
    def available_lfns(self):
        return [lfn for lfn in self.lfns if lfn.has_replica]

    @property
    def filename(self):
        return self.job.input_data["filetype"]

    @property
    def n_test_lfns(self):
        return self.job.input_data.get("n_test_lfns", 1)

    @property
    def transform_ids(self):
        return self.job.input_data["transform_ids"]

    @property
    def size(self):
        from dirac_prod.classes import InputDataset

        ds = InputDataset.from_transform_ids(
            transform_ids=self.transform_ids,
            file_type=self.filename,
            runs=_format_bkquery_runs(self.job.input_data),
        )
        ds.set_dq_flags(self.job.input_data.get("dq_flags", ["OK"]))
        smog2_state = self.job.input_data.get("smog2_state")
        if smog2_state:
            ds.set_smog2_state(smog2_state)
        return ds.size
