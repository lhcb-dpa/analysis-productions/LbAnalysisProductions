###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "Test",
]

from os.path import basename, join

import botocore
from sqlalchemy import (
    JSON,
    BigInteger,
    Column,
    Enum,
    Float,
    ForeignKey,
    Integer,
    Interval,
    String,
    Unicode,
)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, validates

from LbAnalysisProductions.logging import log
from LbAnalysisProductions.settings import settings
from LbAnalysisProductions.utils import TestStatusEnum

from . import Base


class Test(Base):
    @classmethod
    def create(cls, job):
        self = cls()
        self.job = job

        self.s3_bucket = settings.s3.bucket
        signed_key = join(
            str(self.pipeline.gitlab_job_id),  # pylint: disable=no-member
            self.production.name,  # pylint: disable=no-member
            self.job.name,
            str(self.attempt),
            "${filename}",
        )
        log.debug("Signing URL for POST to s3://%s/%s", self.s3_bucket, signed_key)
        response = settings.s3.client.generate_presigned_post(
            self.s3_bucket,
            signed_key,
            Fields={"acl": "private"},
            Conditions=[{"acl": "private"}, ["content-length-range", 0, 2 * 1024**3]],
            ExpiresIn=24 * 60 * 60,
        )
        self.s3_post_url = response["url"]
        self.s3_fields = response["fields"]
        return self

    __tablename__ = "tests"
    id = Column(Integer, primary_key=True)
    # Columns
    status = Column(
        Enum(TestStatusEnum), nullable=False, default=TestStatusEnum.UNKNOWN
    )
    # Results of the test
    events_requested = Column(BigInteger)
    events_processed = Column(BigInteger)
    run_time = Column(Interval)
    cpu_norm = Column(Float)
    n_log_warning = Column(Integer)
    n_log_error = Column(Integer)
    n_log_fatal = Column(Integer)
    # Memory use statistics
    memory_max_pss = Column(Integer)  # KB
    memory_max_rss = Column(Integer)  # KB
    memory_max_swap = Column(Integer)  # KB
    # Output from the test
    s3_bucket = Column(String(256), nullable=False, default="")
    s3_post_url = Column(Unicode(1024), nullable=False, default="")
    s3_fields = Column(JSON, nullable=False, default={})
    # Relationships
    job_id = Column(Integer, ForeignKey("jobs.id"), nullable=False)
    job = relationship(
        "LbAnalysisProductions.models.job.Job", back_populates="tests", lazy="joined"
    )
    input_files = relationship(
        "InputFile",
        order_by="InputFile.path",
        back_populates="test",
        cascade="all, delete, delete-orphan",
        lazy="select",
    )
    output_files = relationship(
        "OutputFile",
        order_by="OutputFile.path",
        back_populates="test",
        cascade="all, delete, delete-orphan",
        lazy="select",
    )

    def __repr__(self):
        return f"{self.__class__.__name__}({self.job}, {self.attempt})"

    @hybrid_property
    def pipeline(self):
        return self.production.pipeline  # pylint: disable=no-member

    @hybrid_property
    def production(self):
        return self.step.production  # pylint: disable=no-member

    @hybrid_property
    def step(self):
        return self.job.step

    @hybrid_property
    def attempt(self):
        return self.job.tests.index(self)

    @property
    def s3_filenames(self):
        assert self.s3_fields["key"].endswith("${filename}"), self.s3_fields["key"]
        prefix = self.s3_fields["key"][: -len("${filename}")]
        try:
            keys = [
                d["Key"]
                for d in settings.s3.client.list_objects(
                    Bucket=self.s3_bucket, Prefix=prefix
                ).get("Contents", [])
            ]
        except botocore.exceptions.ClientError:
            log.exception("Failed to list %s in %s", self.s3_bucket, prefix)
            return {}
        else:
            return {basename(k): k for k in keys}

    @validates("status")
    def validate_status(self, key, new_status):
        """State machine for test statuses"""
        # It's always valid to stay in the same state or fail
        if self.status in [new_status, TestStatusEnum.FAILED]:
            return new_status

        if self.status == TestStatusEnum.UNKNOWN:
            # Unknown can transition to any state
            return new_status
        elif self.status == TestStatusEnum.RUNNING:
            # Running tests can only complete
            if new_status in [TestStatusEnum.SUCCESS, TestStatusEnum.FAILED]:
                return new_status
        elif self.status == TestStatusEnum.SUCCESS:
            # The only valid transition is to fail
            if new_status in [TestStatusEnum.FAILED]:
                return new_status
        elif self.status == TestStatusEnum.FAILED:
            # No valid transitions are available
            pass
        else:
            raise NotImplementedError(self.status)

        log.error(
            "Forbidden transition for %r from %s to %s, marking as failed",
            self,
            self.status,
            new_status,
        )
        self.status = TestStatusEnum.FAILED
        raise NotImplementedError(self, self.status, new_status)

    @property
    def finished(self):
        return self.status in [TestStatusEnum.SUCCESS, TestStatusEnum.FAILED]
