###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "Production",
]

import re
from concurrent.futures import ThreadPoolExecutor
from os.path import join
from tempfile import TemporaryDirectory

import LbAPCommon
from graphviz import Digraph
from LbAPCommon.parsing import create_proc_pass_map
from sqlalchemy import (
    JSON,
    Column,
    ForeignKey,
    Integer,
    String,
    UnicodeText,
    UniqueConstraint,
)
from sqlalchemy.dialects import mysql
from sqlalchemy.orm import deferred, relationship

from LbAnalysisProductions import utils
from LbAnalysisProductions.exceptions import LbAPJobFailed
from LbAnalysisProductions.logging import log

from . import Base
from .job import Job, JobInputDataset
from .step import Step

RE_PRODUCTION_NAME = re.compile(
    r"^(?:[a-z][a-z0-9_]+/)*[a-z][a-z0-9_]+/?$", re.IGNORECASE
)


class Production(Base):
    @classmethod
    def create(cls, pipeline, repo_root, name):
        if not RE_PRODUCTION_NAME.match(name):
            raise ValueError(
                f"Production name must match {RE_PRODUCTION_NAME.pattern!r}"
            )

        self = cls()
        self.pipeline = pipeline
        self.name = name

        # Render using jinja2 and load
        info_fn = join(repo_root, name, "info.yaml")
        with open(info_fn) as fp:
            self.yaml = fp.read()
        self.rendered_yaml = LbAPCommon.render_yaml(self.yaml)
        try:
            jobs_data, checks_data = LbAPCommon.parse_yaml(self.rendered_yaml)
            warnings = (  # pylint: disable=assignment-from-no-return
                LbAPCommon.validate_yaml(jobs_data, checks_data, repo_root, self.name)
            )
        except Exception as e:
            log.critical(f"{e}")
            while e := e.__context__:
                log.critical(f"{e}")
            raise LbAPJobFailed(f"Failed to parse and validate {name!r}")
        else:
            for warning in warnings or []:
                log.warning(warning)

        self.checks_data = checks_data

        # Create the jobs
        self.jobs = []
        job_names = []
        for name, job_spec in jobs_data.items():
            self.jobs.append(Job.create(name=name, **job_spec))
            job_names.append(name)
        if len(self.jobs) == 0:
            raise LbAPJobFailed("No jobs found")

        # Build the DAG
        for job in self.jobs:
            if isinstance(job.input, JobInputDataset):
                job.parent = self[job.input.job_name]
        # Make groups of jobs which have flat dependency chains
        edges = [job for job in self.jobs if not job.children]
        while edges:
            job = edges.pop()
            step_jobs = [job]
            # Include parent productions which only have one child
            job = job.parent
            while job:
                if len(job.children) == 1:
                    step_jobs.insert(0, job)
                else:
                    assert len(job.children) > 1, (
                        "Something went very wrong with job.children",
                        len(job.children),
                    )
                    # Productions with multiple children have to be ran separately
                    edges.append(job)
                job = job.parent
            # Set the job indices to ensure they're executed in the correct order
            for i, job in enumerate(step_jobs):
                job.step_index = i
            self.steps += [Step.create(step_jobs)]
        _step_jobs = {j.name for s in self.steps for j in s.jobs}
        _all_jobs = {j.name for j in self.jobs}
        if _step_jobs != _all_jobs:
            raise LbAPJobFailed(
                "There is an issue in the YAML. The most likely cause is that "
                "the graph of jobs is (i.e. jobs with 'job_name' inputs) invalid "
                "due to badly defined input datasets. The best guess at the "
                "problem is that it is in one or more of:"
                + repr(_all_jobs.symmetric_difference(_step_jobs))
            )

        # attempt to build the processing path map with a dummy version string
        # to check that the job names are not too long
        for step in self.steps:
            try:
                create_proc_pass_map([j.name for j in step.jobs], "v0r0p00000000")
            except ValueError as e:
                raise LbAPJobFailed(str(e))

        # Automatic configuration can only be generated after building the graph
        # This must be ran in a valid execution order!!!
        with utils.RunAsDIRACUser(self.pipeline.job.username):
            futures = []
            with ThreadPoolExecutor(max_workers=10) as e:
                for nstep, step in enumerate(self.steps, start=1):

                    def _generate_configuration(self=self, nstep=nstep, step=step):
                        for njob, job in enumerate(step.jobs, start=1):
                            log.info(
                                "Generating configuration options for %s %s (%s.%s/%s)",
                                self.name,
                                job.name,
                                nstep,
                                njob,
                                len(self.steps),
                            )
                            try:
                                job.generate_configuration()
                            except Exception as e:
                                log.fatal(
                                    "Generating automatically_configure options failed for %s",
                                    job,
                                )
                                return e

                    if step.jobs[0].automatically_configure:
                        futures.append(e.submit(_generate_configuration))

            for future in futures:
                exception = future.result()
                if exception is not None:
                    raise exception

        if "comment" in jobs_data[job_names[0]]:
            self.comment = jobs_data[job_names[0]]["comment"]
        else:
            self.comment = ""

        return self

    __tablename__ = "productions"
    __table_args__ = (UniqueConstraint("name", "pipeline_id"),)
    id = Column(Integer, primary_key=True)
    # Columns
    name = Column(String(256), nullable=False)
    comment = Column(String(5000), nullable=False, default="")

    # To save long rendered yaml files
    UT_long = UnicodeText()
    UT_long = UT_long.with_variant(mysql.MEDIUMTEXT, "mysql")

    yaml = deferred(Column(UT_long, nullable=False))
    rendered_yaml = deferred(Column(UT_long, nullable=False))

    checks_data = Column(JSON, nullable=True)

    # Relationships
    pipeline_id = Column(Integer, ForeignKey("pipelines.gitlab_job_id"))
    pipeline = relationship(
        "LbAnalysisProductions.models.pipeline.Pipeline",
        back_populates="productions",
        lazy="joined",
    )
    jobs = relationship(
        "LbAnalysisProductions.models.job.Job",
        order_by="LbAnalysisProductions.models.job.Job.name",
        back_populates="production",
        cascade="all, delete, delete-orphan",
        lazy="select",
    )
    steps = relationship(
        "LbAnalysisProductions.models.step.Step",
        order_by="Step.id",
        back_populates="production",
        cascade="all, delete, delete-orphan",
        lazy="select",
    )

    def __repr__(self):
        return f"Production({self.pipeline}, {self.name})"

    def __getitem__(self, name):
        for job in self.jobs:
            if job.name == name:
                return job
        raise KeyError(f"Failed to find job in {self.name} with name {name}")

    @property
    def svg(self):
        try:
            return self._svg()
        except Exception:
            log.exception("Failed to generate SVG file")
            return "Error generating SVG!"

    def _svg(self):
        graph = Digraph(comment=f"Analysis production for {self.name}")
        graph.attr(rankdir="LR")
        graph.node_attr["shape"] = "box"
        graph.node_attr["style"] = "filled"
        graph.node_attr["fillcolor"] = "white"
        graph.node_attr["margin"] = "0.1,0.025"
        graph.node_attr["width"] = "0"
        graph.node_attr["height"] = "0"

        graph.attr(color="black")
        graph.node(f"start_{self.name}", self.name)

        for i, step in enumerate(self.steps):
            with graph.subgraph(name=f"cluster_{i}") as sub_graph:
                sub_graph.attr(color="black", margin="5")
                for job in step.jobs:
                    sub_graph.node(
                        f"job_{job.name}",
                        job.name,
                        URL=join(self.name, job.name),
                    )
                    if job.parent is None:
                        graph.edge(f"start_{self.name}", f"job_{job.name}")
                    else:
                        graph.edge(
                            f"job_{job.input.job_name}",
                            f"job_{job.name}",
                        )

        with TemporaryDirectory() as temp_dir:
            graph.render(join(temp_dir, "graph"), format="svg")
            with open(join(temp_dir, "graph.svg")) as fp:
                return fp.read()
