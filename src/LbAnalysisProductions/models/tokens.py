###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = [
    "WorkerToken",
]
import secrets
from datetime import datetime, timedelta

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String

from LbAnalysisProductions.models import Base

# class UserToken(Base):
#     id = Column(Integer, primary_key=True, nullable=False)

#     token = Column(String(1024), nullable=False)
#     ip = Column(BigInteger, nullable=False)
#     expiry = Column(BigInteger, nullable=False)


class WorkerToken(Base):
    __tablename__ = "worker_tokens"

    id = Column(Integer, primary_key=True, nullable=False)

    test_id = Column(Integer, ForeignKey("tests.id"), nullable=False)
    # test = relationship("Test", back_populates="token", lazy="joined")
    token = Column(String(1024), nullable=False)
    expires = Column(DateTime, nullable=False)
    revoked = Column(Boolean, nullable=False, default=False)

    def __init__(self, test_id, *, reference=None):
        self.test_id = test_id
        if reference:
            self.token = reference.token
            self.expires = reference.expires
        else:
            self.token = secrets.token_urlsafe(64)
            self.expires = datetime.utcnow() + timedelta(days=2)
