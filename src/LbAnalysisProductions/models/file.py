###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "InputFile",
    "OutputFile",
]

from sqlalchemy import BigInteger, Column, ForeignKey, Integer, Unicode
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from . import Base


class File(Base):
    __abstract__ = True
    id = Column(Integer, primary_key=True, nullable=False)
    # Columns
    path = Column(Unicode(1024), nullable=False)
    size = Column(BigInteger, nullable=False)
    dataset_size = Column(BigInteger, nullable=False)

    # Relationships
    @declared_attr
    def test_id(self):
        return Column(Integer, ForeignKey("tests.id"), nullable=False)

    @declared_attr
    def test(self):
        return relationship("Test", lazy="select")

    def __repr__(self):
        return f"{self.__class__.__name__}({self.test}, {self.path})"


class InputFile(File):
    __tablename__ = "input_files"


class OutputFile(File):
    __tablename__ = "output_files"
