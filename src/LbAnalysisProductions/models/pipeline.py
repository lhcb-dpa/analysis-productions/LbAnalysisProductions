###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "Pipeline",
]

from gitlab_runner_api import Job as GitLabJob
from sqlalchemy import Column, Integer, UnicodeText
from sqlalchemy.orm import relationship

from . import Base


class Pipeline(Base):
    @classmethod
    def create(cls, gitlab_job):
        self = cls()
        self.gitlab_job_id = gitlab_job.pipeline_id
        self.gitlab_job_dump = gitlab_job.dumps()
        return self

    __tablename__ = "pipelines"
    # Columns
    gitlab_job_id = Column(Integer, primary_key=True, nullable=False)
    gitlab_job_dump = Column(UnicodeText, nullable=False)
    # Relationships
    productions = relationship(
        "Production",
        order_by="Production.name",
        back_populates="pipeline",
        cascade="all, delete, delete-orphan",
        lazy="joined",
    )

    def __repr__(self):
        return f"Pipeline({self.gitlab_job_id})"

    @property
    def id(self):
        return self.gitlab_job_id

    @property
    def job(self):
        return GitLabJob.loads(self.gitlab_job_dump)
