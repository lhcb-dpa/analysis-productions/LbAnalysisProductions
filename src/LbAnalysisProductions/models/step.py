###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "Step",
]

from sqlalchemy import Boolean, Column, Enum, ForeignKey, Integer, String
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

from LbAnalysisProductions.utils import ExecutionBackendEnum, TestStatusEnum

from . import Base


class Step(Base):
    @classmethod
    def create(cls, jobs):
        self = cls()
        self.jobs = jobs
        return self

    __tablename__ = "steps"
    id = Column(Integer, primary_key=True)
    # Columns
    running = Column(Boolean, nullable=False, default=False)
    execution_type = Column(Enum(ExecutionBackendEnum))
    execution_id = Column(String(50))
    # Relationships
    production_id = Column(Integer, ForeignKey("productions.id"), nullable=False)
    production = relationship("Production", back_populates="steps", lazy="joined")
    jobs = relationship(
        "LbAnalysisProductions.models.job.Job",
        order_by="LbAnalysisProductions.models.job.Job.step_index",
        back_populates="step",
        cascade="all, delete, delete-orphan",
        lazy="joined",
    )

    def __str__(self):
        return ",".join(j.name for j in self.jobs) or repr(self)

    def __repr__(self):
        return f"Step({self.production}, {self.name})"

    def launch_test(self, TestClass):
        if self.running:
            raise NotImplementedError("This should never happen")
        test = TestClass(self)
        self.execution_type = test.execution_type
        self.execution_id = test.execution_id
        self.running = True

    @hybrid_property
    def pipeline(self):
        return self.production.pipeline

    @hybrid_property
    def can_run(self):
        if self.jobs[0].parent is None:
            return True
        return self.jobs[0].parent.step.completed_successfully

    @hybrid_property
    def completed_successfully(self):
        if self.running:
            return False
        return all(
            job.tests and job.tests[-1].status == TestStatusEnum.SUCCESS
            for job in self.jobs
        )

    @property
    def name(self):
        return ", ".join(j.name for j in self.jobs)

    @property
    def input(self):
        return self.jobs[0].input
