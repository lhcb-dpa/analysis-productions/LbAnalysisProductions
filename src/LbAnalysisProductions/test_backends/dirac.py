###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "DiracTest",
]
import shlex
from os.path import basename
from pathlib import Path
from tempfile import TemporaryDirectory

from DIRAC.Interfaces.API.Dirac import Dirac
from DIRAC.Interfaces.API.Job import Job
from dirac_prod.classes.input_dataset import BookkeepingLFN
from dirac_prod.utils import dw

from LbAnalysisProductions import templates, utils
from LbAnalysisProductions.logging import log
from LbAnalysisProductions.models import InputFile, WorkerToken
from LbAnalysisProductions.settings import settings
from LbAnalysisProductions.utils import contexted

from .base import BaseTest

# True: Successful
# False: Failed
# None: Running
# unset: Waiting
STATUS_MAP = {
    # "New": ,
    # "Received": ,
    # "Checking": ,
    # "Staging": ,
    # "Waiting": ,
    # "Matched": ,
    "Running": None,
    "Stalled": False,
    "Completing": None,
    "Completed": None,
    "Done": True,
    "Failed": False,
    "Killed": False,
}


class DiracTest(BaseTest):
    execution_type = utils.ExecutionBackendEnum.DIRAC

    def __init__(self, step):
        super().__init__(step)
        # FIXME: Picking up still-running jobs is currently broken due to the
        # fact that start_gitlab_job always adds a new Test row in the database.
        # This causes test.attempt to be incremented and the Test will look for
        # files in the wrong S3 prefix
        if False and step.execution_id and self.test_passed(step) is None:
            self._dirac_id = step.execution_id
            log.info("Found existing DIRAC job with ID %s", self._dirac_id)
        else:
            # Check the LFNs attribute exists by accessing it somehow here
            log.debug("%d input LFNs found", len(self.lfns))

            env_activation_command = " ".join(
                [
                    "source",
                    "/cvmfs/lhcbdev.cern.ch/conda/miniconda/linux-64/prod/bin/activate",
                    f"{settings.environment_base}/linux-64",
                ]
            )

            step.jobs[0].tests[-1].input_files = [
                InputFile(
                    path=lfn,
                    size=BookkeepingLFN(lfn).size,
                    dataset_size=step.jobs[0].input.size,
                )
                for lfn in self.lfns
            ]

            attempt = None
            token = None
            with contexted() as session:
                for job in step.jobs:
                    # FIXME: This is a hack to get around the fact that the attempt
                    # is defined the Test is associated with a Job and not a Step
                    # but we want to have a single per-step token
                    if attempt is None:
                        attempt = job.tests[-1].attempt
                    elif attempt != job.tests[-1].attempt:
                        raise NotImplementedError(attempt, job.tests[-1].attempt)

                    token = WorkerToken(job.tests[-1].id, reference=token)
                    session.add(token)
                    session.commit()
                token = token.token

            wrapper = templates.get_template("job_script.sh").render(
                env_activation_command=env_activation_command,
                pipline_id=shlex.quote(str(self.step.pipeline.id)),
                production_name=shlex.quote(self.step.production.name),
                job_name=shlex.quote(self.step.jobs[0].name),
                attempt=shlex.quote(str(attempt)),
                token=shlex.quote(token),
            )
            with TemporaryDirectory() as tmpdir:
                wrapper_fn = Path(tmpdir) / "job_script.sh"
                wrapper_fn.write_text(wrapper)

                script_fn = templates.get_template("run_test.py").filename

                j = Job()
                j.setType("Test")
                j.setName(f"AP - {self.step.pipeline.id} - {str(self.step.name)[:50]}")
                j.setJobGroup(f"AP_{self.step.pipeline.id}")
                j.setBannedSites(
                    [
                        "LCG.NCBJ.pl",
                        "LCG.Pisa.it",
                        "LCG.LAPP.fr",
                        # SLC6 only
                        "LCG.JINR.ru",
                        # OpenBLAS Warning : The number of CPU/Cores(272) is
                        # beyond the limit(256). Terminated.
                        # 1648765/BsToJpsiPhi/MC_2016_MagUp_JpsiKstar_sim09b_s26_dst
                        "LCG.CINECA.it",
                        # Jobs are very slow to start
                        "DIRAC.SDumont.br",
                        # High failure rates
                        "DIRAC.Sibir.ru",
                        "LCG.IN2P3.fr",
                        "DIRAC.UZH.ch",
                    ]
                )

                j.setExecutable(basename(wrapper_fn))
                j.setInputSandbox([str(wrapper_fn), script_fn])
                # j.setPlatform("nehalem-centos7")
                j.setCPUTime(20 * 60 * 60)
                j.setTag(["/cvmfs/lhcbdev.cern.ch/"])
                # j.setOutputData(['output.tar.bz2'])
                # j.setOutputSandbox(['StdOut', 'StdErr'])
                with utils.RunAsDIRACUser(step.production.pipeline.job.username):
                    self._dirac_id = int(
                        dw(
                            Dirac(useCertificates=settings.use_certificates).submitJob(
                                j
                            )
                        )
                    )

            log.info(
                "Submitted DIRAC job for %s %s with ID %s",
                step.production.name,
                step,
                self._dirac_id,
            )

    @property
    def execution_id(self):
        return self._dirac_id

    @classmethod
    def _status_from_dirac(cls, step):
        log.verbose(
            "Checking status of DIRAC job ID %s for %r", step.execution_id, step
        )
        status = dw(
            Dirac(useCertificates=settings.use_certificates).getJobStatus(
                [int(step.execution_id)]
            )
        )[int(step.execution_id)]["Status"]
        log.verbose(
            "DIRAC job ID %s for %r has status %s", step.execution_id, step, status
        )
        return status

    @classmethod
    def test_passed(cls, step):
        """Check if a test has passed, return None if the test is still running"""
        status = cls._status_from_dirac(step)
        return STATUS_MAP.get(status)

    @classmethod
    def test_started(cls, step):
        status = cls._status_from_dirac(step)
        return STATUS_MAP.get(status, False) is None

    @classmethod
    def finalise_test(cls, step):
        """"""
        pass

    @classmethod
    def restart_test(cls, step):
        """"""
        pass
