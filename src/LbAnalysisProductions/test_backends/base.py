###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = [
    "BaseTest",
]

from random import shuffle

from LbAnalysisProductions.exceptions import InvalidInputDatasetException
from LbAnalysisProductions.logging import log


class BaseTest:
    @property
    def execution_type(self):
        raise NotImplementedError()

    def __init__(self, step):
        self._step = step
        self._errored = False

    @property
    def step(self):
        return self._step

    @property
    def lfns(self):
        try:
            return self._lfns
        except AttributeError:
            lfns = sorted(self.step.input.lfns, key=lambda x: x.size)
            # Remove the smallest 50% of files to avoid unusually small files
            lfns = lfns[int(len(lfns) / 2) :]
            # Shuffle the LFNs so we can a "random" test
            shuffle(lfns)
            self._lfns = []
            log.debug(
                "Looking for %s input LFNs out of the %s taken from %r",
                self.step.input.n_test_lfns,
                len(lfns),
                self.step.input,
            )
            n_lfns_with_replicas = 0
            for lfn in lfns:
                # TODO: This is really slow...
                if lfn.replicas:
                    n_lfns_with_replicas += 1
                    self._lfns.append(lfn)
                if len(self._lfns) >= int(self.step.input.n_test_lfns):
                    break
            else:
                message = f"Failed to find {self.step.input.n_test_lfns} "
                message += f"available input LFNs for {self.step.name!r}."
                if len(lfns) > 0 and n_lfns_with_replicas == 0:
                    message += "\n\nThere appear to be no replicas available."
                    message += "\nPlease contact lhcb-datamanagement@cern.ch for help."
                raise InvalidInputDatasetException(message)
            return self._lfns

    @property
    def execution_id(self):
        raise NotImplementedError()

    @classmethod
    def test_passed(cls, step):
        """Check if a test has passed, return None if the test is still in progress"""
        for subclass in cls.__subclasses__():
            if step.execution_type == subclass.execution_type:
                return subclass.test_passed(step)
        raise NotImplementedError(f"Unknown execution type {step.execution_type!r}")

    @classmethod
    def test_started(cls, step):
        """Check if a test has started running"""
        for subclass in cls.__subclasses__():
            if step.execution_type == subclass.execution_type:
                return subclass.test_started(step)
        raise NotImplementedError(f"Unknown execution type {step.execution_type!r}")

    @classmethod
    def finalise_test(cls, step):
        """"""
        for subclass in cls.__subclasses__():
            if step.execution_type == subclass.execution_type:
                step.running = False
                return subclass.finalise_test(step)
        raise NotImplementedError(f"Unknown execution type {step.execution_type!r}")

    @classmethod
    def restart_test(cls, step):
        """"""
        for subclass in cls.__subclasses__():
            if step.execution_type == subclass.execution_type:
                subclass.restart_test(step)
                step.running = False
                subclass.finalise_test(step)
                log.info("Re-submitting test for %s %s", step.production.name, step)
                step.launch_test(subclass)
                return
        raise NotImplementedError(f"Unknown execution type {step.execution_type!r}")
