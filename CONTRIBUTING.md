# Development guide

## Starting a local instance in docker

1. Prepare the environment
```bash
  cp /tmp/x509up_u$(id -u) /tmp/lbap-dev/proxy
  export LBAP_IMAGE_TAG=latest
  export AWS_S3_BUCKET=lbap-$(date "+%Y-%m-%d-%H-%M")-dev
  export AWS_ACCESS_KEY_ID=xxxxxx
  export AWS_SECRET_ACCESS_KEY=yyyyyy
  export COVERAGE_FILE=/tmp/lbap-dev/coverage
```
2. Create a cluster for testing jobs on
```bash
openstack coe cluster template list
openstack coe cluster list
openstack keypair list
openstack coe cluster create lbap-testing --keypair cburr-mbp-2015-cern --cluster-template kubernetes --node-count 2
```
3. Wait for the cluster to be created: `openstack coe cluster list`
```bash
openstack coe cluster config lbap-testing --dir /tmp/lbap-dev/
mv /tmp/lbap-dev/config /tmp/lbap-dev/k8_config
export /tmp/lbap-dev/k8_config
```
4. `docker stack deploy -c docker-compose.yml lbap`


## Interactive development

```bash
docker run -e COVERAGE_PROCESS_START=/source/.coveragerc -e COVERAGE_FILE=/tmp/lbap-dev/cov1 -v /tmp/lbap-dev:/tmp/lbap-dev -v $PWD:/source:ro --rm -it 449c32eb2374 bash -c 'echo Done'
```

## Commands for reference

### Docker swarm

```bash
docker swarm init
env LBAP_IMAGE_TAG=latest docker stack deploy -c docker-compose.yml lbap
docker service ls
docker service ps lbap_celery-worker
docker service logs lbap_celery-worker
docker stack rm lbap
```

### Restart the cluster

```bash
docker service scale lbap_celery-beat=0 lbap_celery-worker=0 lbap_webapp=0
docker service scale lbap_celery-beat=1 lbap_celery-worker=1 lbap_webapp=1
```

### Get coverage

```bash
coverage combine
coverage report
```

<!-- ## Fully local

Kind could potentially be used to run a cluster within docker

1. `docker run --rm -it -u root -v /var/run/docker.sock:/var/run/docker.sock -v $PWD:/repo -v /tmp/lbap-dev:/tmp/lbap-dev gitlab-registry.cern.ch/cburr/lbanalysisproductions/kubernetes:latest bash`
2. Set up kubernetes and docker:
```bash
yum install -y docker
curl -o go.tar.gz https://dl.google.com/go/go1.12.9.linux-amd64.tar.gz
tar xf go.tar.gz
export PATH=$PWD/go/bin:$PATH
export PATH=$(go env GOPATH)/bin:$PATH
GO111MODULE="on" go get sigs.k8s.io/kind@v0.5.1
kind create cluster --wait 30s --name kind --config /shared/docker/integration/kind-config.yaml
kind get kubeconfig --name kind | sed 's/127.0.0.1/host.docker.internal/g' > /tmp/lbap-dev/k8_config
export KUBECONFIG=/tmp/lbap-dev/k8_config
kubectl get pods
```
3. Set up the CVMFS driver
 -->
