###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
from os.path import dirname, isdir, join
from subprocess import check_call
from tempfile import TemporaryDirectory

FILES_1 = ["topfile.txt", "somedir/file1.txt", "somedir/file2.txt"]
FILES_2 = ["topfile2.txt", "somedir/file3.txt", "somedir2/file4.txt"]
FILES_3 = [
    "topfile3.txt",
    "somedir2/file5.txt",
    "deep/somedir2/file6.txt",
    "somedir2/deep/file7.txt",
    "a/very/deep/somedir/file8.txt",
]


class MockRepo:
    def __init__(self, repo_dir):
        self.repo_dir = repo_dir

    def checkout(self, branch, is_new):
        is_new = "-b" if is_new else ""
        check_call(["git", "checkout", is_new, branch], cwd=self.repo_dir)

    # def _decorator(self, message):
    #     if isinstance(paths, str):
    #         os.makedirs(dirname(join(self.repo_dir, new_paths)), exist_ok=True)
    #         check_call(['git', 'mv', paths, new_paths], cwd=self.repo_dir)
    #     else:
    #         for path, new_path in zip(paths, new_paths):
    #             self.move(path, path, new_path, commit=False)
    #     if commit:
    #         check_call(['git', 'commit', '-m', 'Move %s to %s' %
    #                    (paths, new_paths)], cwd=self.repo_dir)

    def move(self, paths, new_paths, commit=True):
        if isinstance(paths, str):
            if not isdir(dirname(join(self.repo_dir, new_paths))):
                os.makedirs(dirname(join(self.repo_dir, new_paths)))
            check_call(["git", "mv", paths, new_paths], cwd=self.repo_dir)
        else:
            for path, new_path in zip(paths, new_paths):
                self.move(path, path, new_path, commit=False)
        if commit:
            check_call(
                ["git", "commit", "-m", f"Move {paths} to {new_paths}"],
                cwd=self.repo_dir,
            )

    def rm(self, paths, new_paths, commit=True):
        if isinstance(paths, str):
            if not isdir(dirname(join(self.repo_dir, new_paths))):
                os.makedirs(dirname(join(self.repo_dir, new_paths)))
            check_call(["git", "rm", paths, new_paths], cwd=self.repo_dir)
        else:
            for path, new_path in zip(paths, new_paths):
                self.rm(path, path, new_path, commit=False)
        if commit:
            check_call(["git", "commit", "-m", "Delete %s" % paths], cwd=self.repo_dir)

    def write(self, paths, content="Testing", add=True, commit=True):
        if isinstance(paths, str):
            full_path = join(self.repo_dir, paths)
            if not isdir(dirname(full_path)):
                os.makedirs(dirname(full_path))
            with open(full_path, "a") as fp:
                fp.write(content)
            if add or commit:
                check_call(["git", "add", full_path], cwd=self.repo_dir)
        else:
            for path in paths:
                self.write(path, content, add=add, commit=False)
        if commit:
            check_call(["git", "commit", "-m", "Create %s" % paths], cwd=self.repo_dir)


def _check(repo_dir, expected, *args, **kwargs):
    import LbAnalysisProductions.git

    output = LbAnalysisProductions.git.changes(*args, **kwargs)
    assert set(output) == expected


def with_repo(func):
    def wrapped(*args, **kwargs):
        with TemporaryDirectory() as tmp_dir:
            upstream = MockRepo(join(tmp_dir, "upstream"))
            os.makedirs(upstream.repo_dir)
            check_call("git init -b master".split(), cwd=upstream.repo_dir)
            check_call(
                'git config user.email "ci@example.com"'.split(), cwd=upstream.repo_dir
            )
            check_call(
                'git config user.name "Continuous Integration"'.split(),
                cwd=upstream.repo_dir,
            )

            upstream.write("somedir/file1.txt")
            upstream.write(FILES_1)

            local = MockRepo(join(tmp_dir, "local"))
            check_call(["git", "clone", upstream.repo_dir, local.repo_dir])
            check_call(
                'git config user.email "ci@example.com"'.split(), cwd=local.repo_dir
            )
            check_call(
                'git config user.name "Continuous Integration"'.split(),
                cwd=local.repo_dir,
            )
            return func(upstream, local, *args, **kwargs)

    return wrapped


###############################################################################
# Creating
###############################################################################


@with_repo
def test_new_find_everything_branch(upstream, local):
    local.checkout("my-branch", is_new=True)

    expected = {"topfile.txt", "somedir/file1.txt", "somedir/file2.txt"}
    _check(local.repo_dir, expected, local.repo_dir)
    _check(local.repo_dir, expected, local.repo_dir, "")
    _check(local.repo_dir, expected, local.repo_dir, ".")

    expected = {"file1.txt", "file2.txt"}
    _check(local.repo_dir, expected, local.repo_dir, "somedir")


@with_repo
def test_new_find_everything_mr(upstream, local):
    local.checkout("my-branch", is_new=True)
    local.write(FILES_2)

    expected = {"topfile2.txt", "somedir/file3.txt", "somedir2/file4.txt"}
    _check(local.repo_dir, expected, local.repo_dir, is_merge_request=True)
    _check(local.repo_dir, expected, local.repo_dir, "", is_merge_request=True)
    _check(local.repo_dir, expected, local.repo_dir, ".", is_merge_request=True)

    expected = {"file3.txt"}
    _check(local.repo_dir, expected, local.repo_dir, "somedir", is_merge_request=True)

    expected = {"file4.txt"}
    _check(local.repo_dir, expected, local.repo_dir, "somedir2", is_merge_request=True)


@with_repo
def test_new_deep_branch(upstream, local):
    local.checkout("my-branch", is_new=True)
    local.write(FILES_3)

    expected = {"topfile3.txt", "somedir2", "deep", "a"}
    _check(local.repo_dir, expected, local.repo_dir, depth=1)
    _check(local.repo_dir, expected, local.repo_dir, "", depth=1)
    _check(local.repo_dir, expected, local.repo_dir, ".", depth=1)

    expected = set()
    _check(local.repo_dir, expected, local.repo_dir, "somedir")


@with_repo
def test_new_deep_mr(upstream, local):
    local.checkout("my-branch", is_new=True)
    local.write(FILES_3)

    expected = {
        "topfile3.txt",
        "somedir2/file5.txt",
        "deep/somedir2/file6.txt",
        "somedir2/deep/file7.txt",
        "a/very/deep/somedir/file8.txt",
    }
    _check(local.repo_dir, expected, local.repo_dir, is_merge_request=True)
    _check(local.repo_dir, expected, local.repo_dir, "", is_merge_request=True)
    _check(local.repo_dir, expected, local.repo_dir, ".", is_merge_request=True)

    expected = set()
    _check(local.repo_dir, expected, local.repo_dir, "somedir", is_merge_request=True)

    expected = {"file5.txt", "deep/file7.txt"}
    _check(local.repo_dir, expected, local.repo_dir, "somedir2", is_merge_request=True)


###############################################################################
# Renaming
###############################################################################


@with_repo
def test_rename(upstream, local):
    local.checkout("my-branch", is_new=True)
    local.move("somedir/file1.txt", "somedir/file1a.txt")

    expected = {"somedir/file1a.txt"}
    _check(local.repo_dir, expected, local.repo_dir, is_merge_request=False)
    _check(local.repo_dir, expected, local.repo_dir, "", is_merge_request=False)
    _check(local.repo_dir, expected, local.repo_dir, ".", is_merge_request=False)

    expected = {"file1a.txt"}
    _check(local.repo_dir, expected, local.repo_dir, "somedir", is_merge_request=False)


@with_repo
def test_rename_out(upstream, local):
    local.checkout("my-branch", is_new=True)
    local.move("somedir/file1.txt", "file1.txt")

    expected = {"file1.txt"}
    _check(local.repo_dir, expected, local.repo_dir, is_merge_request=False)
    _check(local.repo_dir, expected, local.repo_dir, "", is_merge_request=False)
    _check(local.repo_dir, expected, local.repo_dir, ".", is_merge_request=False)

    expected = set()
    _check(local.repo_dir, expected, local.repo_dir, "somedir", is_merge_request=False)


@with_repo
def test_rename_in(upstream, local):
    local.checkout("my-branch", is_new=True)
    local.move("topfile.txt", "somedir/topfile.txt")

    expected = {"somedir/topfile.txt"}
    _check(local.repo_dir, expected, local.repo_dir, is_merge_request=False)
    _check(local.repo_dir, expected, local.repo_dir, "", is_merge_request=False)
    _check(local.repo_dir, expected, local.repo_dir, ".", is_merge_request=False)

    expected = {"topfile.txt"}
    _check(local.repo_dir, expected, local.repo_dir, "somedir", is_merge_request=False)


@with_repo
def test_rename_mr(upstream, local):
    local.checkout("my-branch", is_new=True)
    local.move("somedir/file1.txt", "somedir/file1a.txt")

    expected = {"somedir/file1a.txt"}
    _check(local.repo_dir, expected, local.repo_dir, is_merge_request=True)
    _check(local.repo_dir, expected, local.repo_dir, "", is_merge_request=True)
    _check(local.repo_dir, expected, local.repo_dir, ".", is_merge_request=True)

    expected = {"file1a.txt"}
    _check(local.repo_dir, expected, local.repo_dir, "somedir", is_merge_request=True)


@with_repo
def test_rename_out_mr(upstream, local):
    local.checkout("my-branch", is_new=True)
    local.move("somedir/file1.txt", "file1.txt")

    expected = {"file1.txt"}
    _check(local.repo_dir, expected, local.repo_dir, is_merge_request=True)
    _check(local.repo_dir, expected, local.repo_dir, "", is_merge_request=True)
    _check(local.repo_dir, expected, local.repo_dir, ".", is_merge_request=True)

    expected = set()
    _check(local.repo_dir, expected, local.repo_dir, "somedir", is_merge_request=True)


@with_repo
def test_rename_in_mr(upstream, local):
    local.checkout("my-branch", is_new=True)
    local.move("topfile.txt", "somedir/topfile.txt")

    expected = {"somedir/topfile.txt"}
    _check(local.repo_dir, expected, local.repo_dir, is_merge_request=True)
    _check(local.repo_dir, expected, local.repo_dir, "", is_merge_request=True)
    _check(local.repo_dir, expected, local.repo_dir, ".", is_merge_request=True)

    expected = {"topfile.txt"}
    _check(local.repo_dir, expected, local.repo_dir, "somedir", is_merge_request=True)


###############################################################################
# Deletion
###############################################################################

# Advance master
# Delete outside
# Delete inside
# Conflict?
