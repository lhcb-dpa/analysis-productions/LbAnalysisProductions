###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest

from LbAnalysisProductions.job_configuration import from_bk_query_job, parse_input_type


@pytest.fixture(scope="function")
def fake_job():
    class job:
        input_data = {}
        turbo = None
        simulation = None
        data_type = None
        input_type = None
        dddb_tag = None
        conddb_tag = None

    return job


@pytest.mark.parametrize("_turbo", [False, None])
def test_parse_input_type(_turbo):
    assert parse_input_type("DST", _turbo) == "DST"
    assert parse_input_type("LDST", _turbo) == "LDST"
    assert parse_input_type("MDST", _turbo) == "MDST"
    assert parse_input_type("XDST", _turbo) == "XDST"

    assert (
        parse_input_type(
            "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/BHADRONCOMPLETEEVENT.DST",
            _turbo,
        )
        == "DST"
    )
    assert (
        parse_input_type(
            "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/LEPTONIC.MDST",
            _turbo,
        )
        == "MDST"
    )
    assert (
        parse_input_type(
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09g/Trig0x62661709/Reco17/11144001/LDST",
            _turbo,
        )
        == "LDST"
    )
    assert (
        parse_input_type(
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST",
            _turbo,
        )
        == "DST"
    )
    assert (
        parse_input_type(
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09e/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/12143001/ALLSTREAMS.LDST",
            _turbo,
        )
        == "LDST"
    )


def test_parse_input_type_turbo(fake_job):
    _turbo = True

    assert parse_input_type("DST", _turbo) == "MDST"
    assert parse_input_type("LDST", _turbo) == "MDST"
    assert parse_input_type("MDST", _turbo) == "MDST"
    assert parse_input_type("XDST", _turbo) == "MDST"

    assert (
        parse_input_type(
            "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/BHADRONCOMPLETEEVENT.DST",
            _turbo,
        )
        == "MDST"
    )
    assert (
        parse_input_type(
            "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/LEPTONIC.MDST",
            _turbo,
        )
        == "MDST"
    )
    assert (
        parse_input_type(
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09g/Trig0x62661709/Reco17/11144001/LDST",
            _turbo,
        )
        == "MDST"
    )
    assert (
        parse_input_type(
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST",
            _turbo,
        )
        == "MDST"
    )
    assert (
        parse_input_type(
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09e/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/12143001/ALLSTREAMS.LDST",
            _turbo,
        )
        == "MDST"
    )


@pytest.mark.parametrize("_turbo", [True, False, None])
def test_auto_configuration_data(_turbo, fake_job):
    raise pytest.skip()
    fake_job.input_data["bk_query"] = (
        "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/BHADRONCOMPLETEEVENT.DST"
    )
    fake_job.turbo = _turbo

    from_bk_query_job(fake_job)
    assert fake_job.turbo is _turbo
    assert fake_job.simulation is False
    assert fake_job.data_type == "2015"
    assert fake_job.input_type == ("MDST" if _turbo else "DST")
    assert fake_job.dddb_tag is None
    assert fake_job.conddb_tag is None


@pytest.mark.parametrize("_turbo", [True, False, None])
def test_auto_configuration_mc(_turbo, fake_job, grid_proxy):
    raise pytest.skip()
    fake_job.input_data["bk_query"] = (
        "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
    )
    fake_job.turbo = _turbo

    from_bk_query_job(fake_job)
    assert fake_job.turbo is _turbo
    assert fake_job.simulation is True
    assert fake_job.data_type == "2017"
    assert fake_job.input_type == ("MDST" if _turbo else "DST")
    assert fake_job.dddb_tag == "dddb-20170721-3"
    assert fake_job.conddb_tag == "sim-20180411-vc-mu100"


@pytest.mark.parametrize("_turbo", [True, False, None])
def test_auto_configuration_mc_upgrade(_turbo, fake_job, grid_proxy):
    fake_job.input_data["bk_query"] = (
        "/MC/Upgrade/Beam7TeV-UpgradeML2.1-MagDown-Lumi10-25ns/Upgrade-Sim02/Rec03-WithTruth/13104011/DST"
    )
    fake_job.turbo = _turbo

    from_bk_query_job(fake_job)
    assert fake_job.turbo is _turbo
    assert fake_job.simulation is True
    assert fake_job.data_type == "Upgrade"
    assert fake_job.input_type == ("MDST" if _turbo else "DST")
    assert fake_job.dddb_tag == "mul-20100617"
    assert fake_job.conddb_tag == "sim-20100510-vc-md100"


@pytest.mark.parametrize("_turbo", [True, False, None])
def test_auto_configuration_data_override(_turbo, fake_job):
    raise pytest.skip()
    fake_job.input_data["bk_query"] = (
        "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/BHADRONCOMPLETEEVENT.DST"
    )
    fake_job.turbo = _turbo
    fake_job.data_type = "2016"
    fake_job.input_type = "MY_DST"
    fake_job.dddb_tag = "my-dddb-tag"
    fake_job.conddb_tag = "my-conddb-tag"

    from_bk_query_job(fake_job)
    assert fake_job.turbo is _turbo
    assert fake_job.simulation is False
    assert fake_job.data_type == "2016"
    assert fake_job.input_type == "MY_DST"
    assert fake_job.dddb_tag == "my-dddb-tag"
    assert fake_job.conddb_tag == "my-conddb-tag"


@pytest.mark.parametrize("_turbo", [True, False, None])
def test_auto_configuration_mc_override(_turbo, fake_job):
    raise pytest.skip()
    fake_job.input_data["bk_query"] = (
        "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
    )
    fake_job.turbo = _turbo
    fake_job.data_type = "2016"
    fake_job.input_type = "MY_DST"
    fake_job.dddb_tag = "my-dddb-tag"
    fake_job.conddb_tag = "my-conddb-tag"

    from_bk_query_job(fake_job)
    assert fake_job.turbo is _turbo
    assert fake_job.simulation is True
    assert fake_job.data_type == "2016"
    assert fake_job.input_type == "MY_DST"
    assert fake_job.dddb_tag == "my-dddb-tag"
    assert fake_job.conddb_tag == "my-conddb-tag"


def test_auto_configuration_bad_input_type(fake_job):
    fake_job.input_data["bk_query"] = (
        "/LHCb/junkjunkjunk/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/BHADRONCOMPLETEEVENT.DST"
    )
    with pytest.raises(
        ValueError, match=r"Failed to parse config version \(junkjunkjunk\)"
    ):
        from_bk_query_job(fake_job)

    fake_job.input_data["bk_query"] = (
        "/LHCb/Collision00/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/BHADRONCOMPLETEEVENT.DST"
    )
    with pytest.raises(
        ValueError, match=r"Failed to parse config version \(Collision00\)"
    ):
        from_bk_query_job(fake_job)

    fake_job.input_data["bk_query"] = (
        "/LHCb/Collision99/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/BHADRONCOMPLETEEVENT.DST"
    )
    with pytest.raises(
        ValueError, match=r"Failed to parse config version \(Collision99\)"
    ):
        from_bk_query_job(fake_job)

    fake_job.input_data["bk_query"] = (
        "/MC/2000/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
    )
    with pytest.raises(ValueError, match=r"Failed to parse config version \(2000\)"):
        from_bk_query_job(fake_job)

    fake_job.input_data["bk_query"] = (
        "/MC/2099/Beam6500GeV-2017-MagUp-Nu1.6-25ns-BcVegPyPythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/14135000/ALLSTREAMS.DST"
    )
    with pytest.raises(ValueError, match=r"Failed to parse config version \(2099\)"):
        from_bk_query_job(fake_job)
