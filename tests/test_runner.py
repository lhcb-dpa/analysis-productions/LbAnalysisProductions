###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def test_pipeline_registered(db_session, gitlab_job, monkeypatch):
    from LbAnalysisProductions.gitlab_runner import ensure_pipeline_registered
    from LbAnalysisProductions.models import Pipeline
    from LbAnalysisProductions.settings import settings

    monkeypatch.setattr(settings, "db_url", db_session.connection().engine.url)
    assert len(db_session.query(Pipeline).all()) == 0
    ensure_pipeline_registered(gitlab_job)
    assert len(db_session.query(Pipeline).all()) == 1
    ensure_pipeline_registered(gitlab_job)
    assert len(db_session.query(Pipeline).all()) == 1
