###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import time
from datetime import datetime, timedelta

import bs4
import pytest
import requests


@pytest.mark.integration
def test_list_pipelines(integration):
    response = requests.get("http://localhost:4000")
    response.raise_for_status()


# @pytest.mark.integration
# def test_pipeline_master(integration, gitlab_project):
#     prod_name = 'B2CC/BsToJpsiPhi'
#     job_names = {
#         '2015_MagDown_BHADRONCOMPLETEEVENT': 'success',
#         'MC_2017_MagUp_ALLSTREAMS_Bd2JpsiKstar_no_str_g_strip': 'success',
#         'MC_2017_MagUp_ALLSTREAMS_Bd2JpsiKstar_no_str_g_tuple': 'success',
#     }
#     _run_pipeline(gitlab_project, 'master', prod_name, job_names)


@pytest.mark.integration
def test_pipeline_bs2jspiphi_bad(integration, gitlab_project):
    _run_pipeline(gitlab_project, "bs2jspiphi-bad", prod_name=None, job_names={})


def _run_pipeline(gitlab_project, ref, prod_name, job_names):
    pipeline = gitlab_project.pipelines.create(dict(ref=ref))
    pipeline_id = pipeline.get_id()
    print("Pipeline ID is", pipeline_id)

    start = datetime.now()
    while datetime.now() - start < timedelta(seconds=300):
        pipeline.refresh()
        jobs = {j.attributes["name"]: j for j in pipeline.jobs.list()}
        job = jobs["test"]
        if job.attributes["status"] != "pending":
            break
        time.sleep(1)
    else:
        raise Exception("Pipeline %s did not start" % pipeline_id)

    time.sleep(60)

    # Pipeline summary should now be available
    _check_pipeline_summary(pipeline, pipeline_id, prod_name, job_names)

    if prod_name is None:
        return

    # Check the existence of the YAML
    response = requests.get(
        f"http://localhost:4000/dynamic/{pipeline_id}/{prod_name}/raw.yaml"
    )
    response.raise_for_status()
    response = requests.get(
        f"http://localhost:4000/dynamic/{pipeline_id}/{prod_name}/rendered.yaml"
    )
    response.raise_for_status()
    content = response.content.decode()
    for job_name in job_names:
        assert job_name in content

    # Wait for the pipeline to complete
    while job.attributes["status"] == "running":
        pipeline.refresh()
        jobs = {j.attributes["name"]: j for j in pipeline.jobs.list()}
        job = jobs["test"]
        time.sleep(1)

    # Check the status of the tests
    if job_names and all(x == "success" for x in job_names.values()):
        assert job.attributes["status"] == "success"
    else:
        assert job.attributes["status"] == "failed"
    _check_pipeline_summary(
        pipeline, pipeline_id, prod_name, job_names, check_status=True
    )


def _check_pipeline_summary(
    pipeline, pipeline_id, prod_name, job_names, check_status=False
):
    response = requests.get("http://localhost:4000/%s/" % pipeline_id)
    response.raise_for_status()
    content = response.content.decode()
    html = bs4.BeautifulSoup(content)
    if prod_name is None:
        return
    assert prod_name in content
    for job_name in job_names:
        assert job_name in content
    # Pipeline details
    assert pipeline.attributes["sha"] in content

    table = html.find("table", {"id": "results-summary-table"})
    rows = [r for r in table.find_all("tr") if not r.find_all("th")]
    # There should be three rows left
    assert len(rows) == len(job_names)
    # Each job should have one row
    assert {r.find("td").text for r in rows} == set(job_names)
    if check_status:
        # Check the job statuses in the table are shown and correct
        for row in rows:
            assert "job-status-" + job_names[row.find("td").text] in row.attrs["class"]

    # Check the individual tests
    for job_name, expected_status in job_names.items():
        _check_job_summary(
            pipeline,
            pipeline_id,
            prod_name,
            job_name,
            expected_status,
            check_status=check_status,
        )


def _check_job_summary(
    pipeline, pipeline_id, prod_name, job_name, expected_status, check_status=False
):
    response = requests.get(
        f"http://localhost:4000/{pipeline_id}/{prod_name}/{job_name}/"
    )
    response.raise_for_status()
    content = response.content.decode()
    html = bs4.BeautifulSoup(content)
    assert prod_name in content
    assert job_name in content
    if not check_status:
        return

    response = requests.get(
        "http://localhost:4000/dynamic/%s/%s/%s/0/reproduce_locally.sh"
        % (pipeline_id, prod_name, job_name)
    )
    response.raise_for_status()
    assert pipeline.attributes["sha"] in response.content.decode()

    response = requests.get(
        "http://localhost:4000/dynamic/%s/%s/%s/0/Job.log"
        % (pipeline_id, prod_name, job_name)
    )
    response.raise_for_status()
    assert "Application Manager Terminated successfully" in response.content.decode()

    response = requests.get(
        "http://localhost:4000/dynamic/%s/%s/%s/0/DIRAC.log"
        % (pipeline_id, prod_name, job_name)
    )
    response.raise_for_status()
    assert "lb-run" in response.content.decode()

    if expected_status == "success":
        table = html.find("table", {"id": "output-data-table"})
        output_url = table.find_all("td")[0].find("a").attrs["href"]
        response = requests.get("http://localhost:4000%s" % output_url)
        response.raise_for_status()
        # Output file should be at least a megabyte
        assert len(response.content) > 1024**2
