###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys

sys.path.append(os.path.join(os.environ["ANALYSIS_PRODUCTIONS_BASE"], "BsToJpsiPhi"))

from Configurables import DaVinci

from helpers.stripping import stripping
from generate_configuration import bookkeeping_data

stripping_line = "BetaSBu2JpsiKDetachedLine"
data_type = DaVinci().DataType
stripping_version = bookkeeping_data[data_type].strip

seq = stripping(stripping_version, stripping_line)
DaVinci().UserAlgorithms = [seq]
