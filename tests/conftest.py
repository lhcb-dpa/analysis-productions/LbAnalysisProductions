###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import re
import subprocess
import time
from os.path import dirname, join
from unittest import mock

import pytest
import requests
import sqlalchemy
from gitlab_runner_api import Job as GitLabJob
from gitlab_runner_api import Runner as GitLabRunner
from sqlalchemy.orm import scoped_session, sessionmaker

from LbAnalysisProductions.models import Base, Pipeline, Production


def pytest_addoption(parser):
    parser.addoption(
        "--run-integration",
        action="store_true",
        default=False,
        help="run integration tests",
    )


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "integration: mark test as integration and not ran by default"
    )


def pytest_collection_modifyitems(config, items):
    if config.getoption("--run-integration"):
        return
    skip_slow = pytest.mark.skip(reason="need --run-integration option to run")
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_slow)


@pytest.fixture(scope="function")
def MC_JOB():
    return {
        "wg": "B2CC",
        "inform": "someone@example.com",
        "application": "DaVinci/v42r7p2",
        "input": {
            "bk_query": "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09g/Trig0x62661709/Reco17/11144001/LDST",
        },
        "output": "JOB1_OUTPUT.DST",
        "options": ["options_1.py"],
    }


@pytest.fixture(scope="function")
def DATA_JOB():
    return {
        "wg": "Charm",
        "inform": "someone@example.com",
        "application": "Castelao/v2r1",
        "input": {
            "bk_query": "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34r0p1/90000000/CHARM.MDST",
        },
        "output": "JOB2_OUTPUT.ROOT",
        "options": ["options_2.py"],
    }


@pytest.fixture(scope="function")
def db_session(tmpdir, patch_RunAsDIRACUser):
    engine = sqlalchemy.create_engine(f"sqlite:///{tmpdir}/test.db", echo=False)
    Session = scoped_session(sessionmaker(bind=engine))

    Base.metadata.create_all(engine)

    yield Session

    Session.close()
    Base.metadata.drop_all(bind=engine)


@pytest.fixture(scope="function")
def repo_dir(tmp_path):
    tmp_path = str(tmp_path)
    prod_name = "prod_name"
    os.makedirs(join(tmp_path, prod_name, "folder_1", "folder_2"))
    with open(join(tmp_path, prod_name, "options_1.py"), "w") as fp:
        fp.write('print("Running options_1.py")')
    with open(join(tmp_path, prod_name, "options_2.py"), "w") as fp:
        fp.write('print("Running options_2.py")')
    with open(join(tmp_path, prod_name, "folder_1", "options_3.py"), "w") as fp:
        fp.write('print("Running options_3.py")')
    with open(
        join(tmp_path, prod_name, "folder_1", "folder_2", "options_4.py"), "w"
    ) as fp:
        fp.write('print("Running options_4.py")')
    yield tmp_path


@pytest.fixture(scope="function")
def grid_proxy():
    if not os.path.isfile(f"/tmp/x509up_u{os.getuid()}"):
        pytest.skip(
            "Job configuration can only be tested when a grid proxy is available"
        )


@pytest.fixture(scope="function")
def gitlab_job(patch_RunAsDIRACUser):
    with mock.patch.object(GitLabRunner, "check_auth", return_value=True):
        gitlab_job = GitLabJob.load(join(dirname(__file__), "data", "gitlab-job.json"))
        yield gitlab_job


@pytest.fixture(scope="function")
def filled_db(db_session, gitlab_job):
    pipeline = Pipeline.create(gitlab_job)
    production = Production.create(
        pipeline, join(dirname(__file__), "data", "repo_dir"), "BsToJpsiPhi"
    )
    db_session.add(production)
    db_session.commit()

    yield db_session


@pytest.fixture(scope="session")
def integration():
    # Initially all services should have no replicas so we can capture coverage data
    output = subprocess.check_output(
        ["docker", "service", "ls", "--format", "{{ .Name }} {{ .Replicas }}"]
    ).decode()
    running_services = dict(re.findall(r"([^\s]+) (\d+)/\d+", output))
    if not all(
        s in running_services
        for s in {"lbap_celery-beat", "lbap_celery-worker", "lbap_webapp"}
    ):
        print(running_services)
        raise pytest.skip("Local instance not running, see CONTRIBUTING.md for details")
    if any(
        running_services[s] != "0"
        for s in {"lbap_celery-beat", "lbap_celery-worker", "lbap_webapp"}
    ):
        print(running_services)
        raise pytest.fail("lbap services should be scaled to 0 replicas")

    # Scale up the services in docker swarm
    subprocess.check_call(
        [
            "docker",
            "service",
            "scale",
            "lbap_celery-beat=1",
            "lbap_celery-worker=1",
            "lbap_webapp=1",
        ]
    )

    for i in range(300):
        time.sleep(1)
        try:
            response = requests.get("http://localhost:4000")
        except Exception:
            pass
        else:
            if response.status_code == 200:
                break
    else:
        raise Exception("Failed to scale up services quickly enough")

    yield None

    # Scale down services so the coverage data is stored
    subprocess.check_call(
        [
            "docker",
            "service",
            "scale",
            "lbap_celery-beat=0",
            "lbap_celery-worker=0",
            "lbap_webapp=0",
        ]
    )
    # Sleep a little longer to ensure everything is complete
    time.sleep(30)


@pytest.fixture(scope="session")
def gitlab_project():
    if "GITLAB_API_TOKEN" not in os.environ:
        raise pytest.skip("GITLAB_API_TOKEN not found in environment")
    import gitlab

    gl = gitlab.Gitlab(
        "https://gitlab.cern.ch", private_token=os.environ["GITLAB_API_TOKEN"]
    )
    gl.auth()

    yield gl.projects.get("cburr/analysisproductions-dev")


@pytest.fixture
def patch_RunAsDIRACUser(monkeypatch):
    from LbAnalysisProductions.utils import RunAsDIRACUser

    monkeypatch.setattr(RunAsDIRACUser, "__init__", lambda self, pipeline: None)
    monkeypatch.setattr(RunAsDIRACUser, "__enter__", lambda self: None)
    monkeypatch.setattr(
        RunAsDIRACUser, "__exit__", lambda self, exc_type, exc_value, traceback: None
    )

    yield
