###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import shutil
from os.path import join
from unittest.mock import MagicMock

import pytest
import yaml

from LbAnalysisProductions.exceptions import LbAPJobFailed
from LbAnalysisProductions.models import Production


@pytest.mark.parametrize("name", ["prodname"])
def test_good_prod_name(name, db_session, repo_dir, MC_JOB):
    shutil.move(join(repo_dir, "prod_name"), join(repo_dir, name))
    with open(join(repo_dir, name, "info.yaml"), "w") as fp:
        yaml.safe_dump({"job_name": MC_JOB}, fp)
    p = Production.create(MagicMock(), repo_dir, name)
    assert p.name == name


@pytest.mark.parametrize("name", ["@@@@"])
def test_bad_prod_name(name, db_session, repo_dir, MC_JOB):
    shutil.move(join(repo_dir, "prod_name"), join(repo_dir, name))
    with open(join(repo_dir, name, "info.yaml"), "w") as fp:
        yaml.safe_dump({"job_name": MC_JOB}, fp)
    with pytest.raises(ValueError):
        Production.create(MagicMock(), repo_dir, name)


def test_prod_no_jobs(db_session, repo_dir, MC_JOB):
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump({}, fp)
    with pytest.raises(LbAPJobFailed) as e:
        Production.create(MagicMock(), repo_dir, "prod_name")


@pytest.mark.parametrize("body", ["", [], ["hello", {}]])
def test_bad_prod_content(body, db_session, repo_dir, MC_JOB):
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(body, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


def test_bad_yaml(db_session, repo_dir, MC_JOB):
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        fp.write(":")
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


def test_good_jinja2(db_session, repo_dir, MC_JOB):
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        fp.write('{% set my_name = "some_job_name" %}\n')
        yaml.safe_dump({"{{ my_name }}": MC_JOB}, fp)
    p = Production.create(MagicMock(), repo_dir, "prod_name")
    assert p.jobs[0].name == "some_job_name"


def test_bad_jinja2(db_session, repo_dir, MC_JOB):
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump("{% {{ }} %}", fp)
    with pytest.raises(ValueError, match="with jinja2 on line"):
        Production.create(MagicMock(), repo_dir, "prod_name")


@pytest.mark.parametrize("name", ["job_name", "123job_name", "job-name"])
def test_good_job_name(name, db_session, repo_dir, MC_JOB):
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump({name: MC_JOB}, fp)
    p = Production.create(MagicMock(), repo_dir, "prod_name")
    assert p.jobs[0].name == name


@pytest.mark.parametrize(
    "name", ["../job/name", ".job", "job.name", "job/../name", "Charm/Rare/analysis"]
)
def test_bad_job_name(name, db_session, repo_dir, MC_JOB):
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump({name: MC_JOB}, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


def test_duplicated_job_name(db_session, repo_dir, MC_JOB, DATA_JOB):
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump({"job_name": MC_JOB, "JOB_NAME": DATA_JOB}, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


@pytest.mark.parametrize(
    "app_name,app_version", [("DaVinci", "v44r5"), ("My/App", "v10r12")]
)
def test_good_application(app_name, app_version, db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    prod_info["job_name"]["application"] = "/".join([app_name, app_version])
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    p = Production.create(MagicMock(), repo_dir, "prod_name")
    assert p["job_name"].application_name == app_name
    assert p["job_name"].application_version == app_version


@pytest.mark.parametrize("application", ["DaVinci", [], {}])
def test_bad_application(application, db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    prod_info["job_name"]["application"] = application
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


def test_no_application(db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    del prod_info["job_name"]["application"]
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


@pytest.mark.parametrize("input", ["", [], {}, {"unknown_type": "value"}])
def test_bad_input(input, db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    prod_info["job_name"]["input"] = input
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    with pytest.raises(Exception):
        Production.create(MagicMock(), repo_dir, "prod_name")


def test_no_input(db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    del prod_info["job_name"]["output"]
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


@pytest.mark.parametrize("input", [""])
def test_good_input_bk_query(input, db_session, repo_dir, MC_JOB):
    raise pytest.xfail()


@pytest.mark.parametrize("input", [""])
def test_bad_input_bk_query(input, db_session, repo_dir, MC_JOB):
    raise pytest.xfail()


@pytest.mark.parametrize("input", [""])
def test_good_input_job_name(input, db_session, repo_dir, MC_JOB):
    raise pytest.xfail()


@pytest.mark.parametrize("input", [""])
def test_bad_input_job_name(input, db_session, repo_dir, MC_JOB):
    raise pytest.xfail()


@pytest.mark.parametrize("input", [""])
def test_good_input_prod_id(input, db_session, repo_dir, MC_JOB):
    raise pytest.xfail()


@pytest.mark.parametrize("input", [""])
def test_bad_input_prod_id(input, db_session, repo_dir, MC_JOB):
    raise pytest.xfail()


@pytest.mark.parametrize(
    "output",
    [
        "ANALYSIS.LDST",
        "STREAM.ANALYSIS.LDST",
        ["ANALYSIS.ROOT"],
        ["HIST.ROOT", "TUPLE.ROOT"],
    ],
)
def test_good_output(output, db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    prod_info["job_name"]["output"] = output
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    p = Production.create(MagicMock(), repo_dir, "prod_name")
    assert p["job_name"].output == output or (
        len(p["job_name"].output) == 1 and p["job_name"].output[0] == output
    )


@pytest.mark.parametrize("output", ["", [""], [], {}])
def test_bad_output(output, db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    prod_info["job_name"]["output"] = output
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


def test_no_output(db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    del prod_info["job_name"]["output"]
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


@pytest.mark.parametrize(
    "options",
    [
        "options_1.py",
        "folder_1/options_3.py",
        "folder_1/folder_2/options_4.py",
        "folder_1/../options_2.py",
        ["options_1.py"],
        ["options_1.py", "options_2.py"],
        ["options_1.py", "$APPCONFIGOPTS/DaVinci/DataType-2016.py", "options_2.py"],
        [
            "folder_1/../options_1.py",
            "folder_1/options_3.py",
            "folder_1/folder_2/options_4.py",
        ],
    ],
)
def test_good_options(options, db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    prod_info["job_name"]["options"] = options
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    p = Production.create(MagicMock(), repo_dir, "prod_name")
    expected_options = [
        (
            o
            if o.startswith("$")
            else "$ANALYSIS_PRODUCTIONS_BASE/prod_name/" + os.path.normpath(o)
        )
        for o in ([options] if isinstance(options, str) else options)
    ]
    assert p["job_name"].options == {"files": expected_options} or (
        len(p["job_name"].options) == 1
        and p["job_name"].options[0] == {"files": expected_options}
    )


@pytest.mark.parametrize(
    "options",
    [
        "",
        [""],
        [],
        {},
        "../escaping_options.py",
        "something/../../escaping_options.py",
        ["../escaping_options.py"],
        ["something/../../escaping_options.py"],
    ],
)
def test_bad_options_value(options, db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    prod_info["job_name"]["options"] = options
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    with pytest.raises((LbAPJobFailed, LbAPJobFailed, ValueError)):
        Production.create(MagicMock(), repo_dir, "prod_name")


@pytest.mark.parametrize(
    "options",
    [
        "missing_options.py",
        "folder_1/options_1.py",
        ["missing_options.py"],
        ["folder_1/../options_3.py"],
        ["options_1.py", "missing_options.py"],
        ["options_1.py", "folder_1/../options_3.py"],
        ["folder_1/folder_2/options_4.py", "missing_options.py"],
        ["folder_1/folder_2/options_4.py", "folder_1/../options_3.py"],
        ["folder_1/folder_2/options_4.py", "folder_1/../options_3.py"],
    ],
)
def test_bad_options_notfound(options, db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    prod_info["job_name"]["options"] = options
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


def test_no_options(db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    del prod_info["job_name"]["options"]
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


@pytest.mark.parametrize(
    "inform",
    [
        "username",
        "my-email+something@example.com",
        ["username"],
        ["my-email+something@example.com"],
        ["username", "my-email+something@example.com"],
    ],
)
def test_good_inform(inform, db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    if inform:
        prod_info["job_name"]["inform"] = inform
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    p = Production.create(MagicMock(), repo_dir, "prod_name")
    assert p["job_name"].inform == inform or (
        len(p["job_name"].inform) == 1 and p["job_name"].inform[0] == inform
    )


@pytest.mark.parametrize(
    "inform",
    [
        {},
        "???",
        "@@@",
        "somebody@cern",
        " username",
        "username ",
        "user name",
    ],
)
def test_bad_inform(inform, db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    prod_info["job_name"]["inform"] = inform
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    with pytest.raises(LbAPJobFailed):
        Production.create(MagicMock(), repo_dir, "prod_name")


def test_no_inform(db_session, repo_dir, MC_JOB):
    prod_info = {"job_name": MC_JOB}
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    p = Production.create(MagicMock(), repo_dir, "prod_name")
    assert p["job_name"].inform == ["someone@example.com"]


def test_simple(db_session, repo_dir, MC_JOB, DATA_JOB):
    prod_info = {
        "job_1": MC_JOB,
        "job_2": DATA_JOB,
    }
    with open(join(repo_dir, "prod_name", "info.yaml"), "w") as fp:
        yaml.safe_dump(prod_info, fp)
    production = Production.create(MagicMock(), repo_dir, "prod_name")
    production.pipeline = None
    db_session.add(production)
    db_session.commit()

    assert production.name == "prod_name"
    assert len(production.jobs) == 2
    assert len(production.steps) == 2

    job_1 = production["job_1"]
    assert job_1.name == "job_1"
    assert job_1.application_name == "DaVinci"
    assert job_1.application_version == "v42r7p2"
    assert job_1.parent is None
    assert job_1.children == []
    assert (
        job_1.input.bk_query
        == "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09g/Trig0x62661709/Reco17/11144001/LDST"
    )
    assert job_1.output == ["JOB1_OUTPUT.DST"]
    assert job_1.options == {
        "files": ["$ANALYSIS_PRODUCTIONS_BASE/prod_name/options_1.py"]
    }

    job_2 = production["job_2"]
    assert job_2.name == "job_2"
    assert job_2.application_name == "Castelao"
    assert job_2.application_version == "v2r1"
    assert job_2.parent is None
    assert job_2.children == []
    assert (
        job_2.input.bk_query
        == "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34r0p1/90000000/CHARM.MDST"
    )
    assert job_2.output == ["JOB2_OUTPUT.ROOT"]
    assert job_2.options == {
        "files": ["$ANALYSIS_PRODUCTIONS_BASE/prod_name/options_2.py"]
    }
