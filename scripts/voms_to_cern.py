#!/usr/bin/env python3
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import pprint
from os.path import isfile
from subprocess import check_output

import requests

PROXY_FN = "voms-admin.proxy"
assert isfile(PROXY_FN)

dirac_users = check_output(["lb-dirac", "dirac-admin-list-users"], text=True)
dirac_users = [
    u.strip() for u in dirac_users.split("\n") if u and u != "All users registered:"
]
print(f"Found {len(dirac_users)} users in DIRAC")

users = []
with requests.Session() as s:
    s.verify = "/cvmfs/grid.cern.ch/etc/grid-security/certificates/"
    s.cert = PROXY_FN
    for i in range(100):
        print(f"Getting users from VOMS starting from {len(users)}")
        r = s.get(
            "https://lcg-voms2.cern.ch:8443/voms/lhcb/apiv2/users",
            headers={"X-VOMS-CSRF-GUARD": "y"},
            params={"startIndex": len(users), "pageSize": 100},
        )
        r.raise_for_status()
        data = r.json()
        users.extend(data["result"])
        if len(users) >= data["count"]:
            break
    else:
        raise NotImplementedError()

cern_user_to_dirac_nick = {}
for user in users:
    attributes = {x["name"]: x["value"] for x in user["attributes"]}
    if user["cernHrId"] in [
        759675,
        557847,
        428662,
        797630,
        373326,
        370370,
        397129,
        842802,
        842800,
        817260,
        819957,
        847307,
        396857,
        820097,
    ]:
        print(
            "ERROR parsing:",
            user.get("name"),
            user.get("surname"),
            (
                user.get("certificates")[0].get("subjectString")
                if user.get("certificates")
                else None
            ),
        )
        continue
    if "nickname" not in attributes:
        raise Exception("ERROR: No nickname found for {user['name']} {user['surname']}")
    if attributes["nickname"] not in dirac_users:
        raise Exception(
            f"{user['cernHrId']!r}: {user['name'].upper()} {user['surname'].upper()} has the nickname {attributes['nickname']!r} but is known in DIRAC as 'xxxxx'"
        )
    accounts = check_output(
        [
            "phonebook",
            "--ccid",
            str(user["cernHrId"]),
            "--terse",
            "login",
            "--terse",
            "type",
        ],
        text=True,
    )
    for line in filter(None, accounts.split("\n")):
        cern_user, account_type, _ = line.split(";")
        if account_type in ["Primary", "Secondary"]:
            cern_user_to_dirac_nick[cern_user] = attributes["nickname"]

print("CERN accounts that are mappbut but not equal to DIRAC nickname:")
pprint.pprint({k: v for k, v in cern_user_to_dirac_nick.items() if k != v})

with open("cern_user_to_dirac_nick.json", "w") as fp:
    json.dump(cern_user_to_dirac_nick, fp)

print("Written results to cern_user_to_dirac_nick.json")
